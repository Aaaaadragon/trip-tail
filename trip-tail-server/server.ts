import cors from "cors";
import express from "express";
import { env } from "./env";
import {print} from 'listening-on'
import { UserController } from "./controllers/user-controller";
import {mapLabelService, mountainInfoService, trailRecordService, userProfileService, userService} from "./services/services";
import { MountainInfoController } from "./controllers/mountain_info-controller";
import { TrailRecordController } from "./controllers/trail_record-controller";
import { MapLabelController } from "./controllers/map_label-controller";
import { upload } from "./upload";
import { UserProfileController } from "./controllers/user_profile-controller";
import fakeData from './seeds/jsonFiles/fake_user_data.json'
import fakeUserRank from './seeds/jsonFiles/app_user.json'
import mountainRankAnalysis from './seeds/jsonFiles/mountain_analysis.json'
let app = express()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/photo',express.static(__dirname+'/upload'));


let userController = new UserController(userService)
let mountainInfoController = new MountainInfoController(mountainInfoService)
let trailRecordController = new TrailRecordController(trailRecordService)
let mapLabelController = new MapLabelController(upload,mapLabelService)
let userProfileController = new UserProfileController(upload,userProfileService)

app.use(userController.router)
app.use(mountainInfoController.router)
app.use(trailRecordController.router)
app.use(mapLabelController.router)
app.use(userProfileController.router)


app.get('/fakeData', (req,res)=>{
    res.status(200).json(fakeData)
})

app.get('/fakeUserRank',(req,res)=>{
    res.status(200).json(fakeUserRank)
})

app.get('/mountainRankAnalysis',(req,res)=>{
    res.status(200).json(mountainRankAnalysis)
})

app.use((req, res) => {
	res.status(404).json({message:'content not found'})
})


const PORT = env.port

app.listen(PORT, ()=>{
    print(PORT)
})