import {env} from './env'

export let jwtConfig = {
    jwtSecret:env.jwtSecret,
    jwtSession:{
        session:false
    }
}