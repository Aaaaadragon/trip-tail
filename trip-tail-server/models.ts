export type JWTpayload = {
  id: number;
  exp: number;
  username?: string;
  email?: string;
};

export type RegisterUserWithPasswordDTO = {
  username: string;
  password: string;
};

export type LoginUserWithPasswordDTO = {
  username: string;
  password: string;
};

export type MountainInfoDTO = {
  district_id: number;
  name: string;
  distance: number;
  difficulty: number;
  scenery_rating: number;
  description: string;
  image_filename?: string;
  gps_latitude?: number;
  gps_longitude?: number;
  gps_filename?: string;
  transportation?: string;
};

export type DistrictDTO = {
  id: number;
  name: string;
};

export type TrailRecordDTO = {
  user_id: number;
  trail_name: string;
  gps_record: string; //JSON.Stringify(GPSRecord)
  duration: number;
  distance: number;
  mountain_id?: number;
  is_private?: boolean;
  created_at?: any;
  updated_at?: any;
};

export type TrailRecordPostDTO = {
  user_id: number;
  trail_record_id: number;
  title: string;
  gps_latitude: number;
  gps_longitude: number;
  image_filename?: string;
  description: string;
};

export type LatLngExpression = [number, number];
export type GPSRecord = { GPSRecord: LatLngExpression[] };

export type MapLabelDTO = {
  id?: number;
  user_id?: number;
  label_type_id: number;
  title: string;
  image_filename: string;
  gps_latitude: number;
  gps_longitude: number;
  like_members?: number[];
  dislike_members?: number[];
};

export type UserProfileDTO = {
  user_id: number;
  nickname?: string;
  gender?: GenderOptions;
  district_id?: number;
  icon_photo?: string;
  hike_frequency?: number; //How many times per month
  is_private?: boolean;
};

export type GenderOptions = "male" | "female" | "default";
