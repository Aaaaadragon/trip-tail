import { google } from "googleapis";
import nodemailer from "nodemailer";
const user = process.env.MAIL_FROM
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;
const redirectURI = process.env.REDIRECT_URI;
const refresh_token = process.env.REFRESH_TOKEN;
const oAuth2Client = new google.auth.OAuth2(
  clientId,
  clientSecret,
  redirectURI
);


oAuth2Client.setCredentials({ refresh_token });

export async function getTransport() {
  
    const accessToken = await oAuth2Client.getAccessToken();
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user,
        clientId,
        clientSecret,
        refreshToken: refresh_token,
        accessToken: accessToken as any,
      },
    });
    
    return transport
}
