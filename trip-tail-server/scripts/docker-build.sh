set -e

if [$# == 1]; then
  tag=$1
else
  read -p "tag: " tag
fi

docker build --platform=linux/amd64 --no-cache . -t docker-trip-tail-server:$tag