set -e

./scripts/docker-build.sh dev

docker stop docker-trip-tail-server || true
docker rm docker-trip-tail-server || true

docker run --name docker-trip-tail-server -p 8100:8100  docker-trip-tail-server:dev
