import fetch from "node-fetch";

export async function searchCoorByAddress(trail: string) {
  let hkStandardCoorResult = await fetch(
    `https://geodata.gov.hk/gs/api/v1.0.0/locationSearch?q=${trail}`
  );
  hkStandardCoorResult = await hkStandardCoorResult.json();
  // console.log(hkStandardCoorResult[0])
  let nameZH = hkStandardCoorResult[0].nameZH;
  if (nameZH !== trail) {
    noGPSTrail(trail);
    return [0, 0];
  }
  let x = hkStandardCoorResult[0].x;
  let y = hkStandardCoorResult[0].y;
  let GPSCoordination: any = await fetch(
    `http://www.geodetic.gov.hk/transform/v2/?inSys=hkgrid&e=${x}&n=${y}`
  );

  GPSCoordination = await GPSCoordination.json();
  let wgsCoordination = [GPSCoordination.wgsLat, GPSCoordination.wgsLong];

  return wgsCoordination;
}

//  Cannot find GPS coordinate
let array: string[] = [];
function noGPSTrail(trail: string) {
  array.push(trail);
}
