// import { Knex } from "knex";
import { chromium } from "playwright";
import { MountainInfoDTO } from "../models";
import { searchCoorByAddress } from "./addressToCoordinate";
import fs from "fs";

// let sleep = async function (time: number) {
//   await timeout(time);
// };
// async function timeout(ms: number) {
//   return new Promise((resolve) => setTimeout(resolve, ms));
// }

export async function search() {
  const browser = await chromium.launch({ headless: false });
  let context = await browser.newContext({
    userAgent:
      "Mozilla / 5.0(Windows NT 10.0; Win64; x64; rv: 96.0) Gecko/20100101 Firefox/96.0",
  });
  let page = await context.newPage();

  //  visit oasisTrek.com
  let url = "https://www.oasistrek.com/";
  await page.goto(url);

  //  Query trails and 4 trails)
  let trailList = await page.$$("a.dropdown-item");
  let districtInfo = {};
  for (let item of trailList) {
    let trailsName = await item.textContent();
    let href = await item.getAttribute("href");
    districtInfo[trailsName as string] = url + href;
  }

  let trailDetail: MountainInfoDTO[] = [];

  for (let trailDistrict in districtInfo) {
    // await sleep(3000);
    let district_id;
    switch (trailDistrict) {
      case "西貢":
      case "新界中部":
      case "新界東北部":
      case "新界西北部":
      case "大嶼山":
        district_id = 1;
        break;
      case "九龍":
        district_id = 2;
        break;
      case "香港島":
        district_id = 3;
        break;
      case "離島":
        district_id = 4;
        break;
      // case "麥理浩徑":
      //   district_id = "5";
      // break;
      default:
        continue;
    }
    console.log("District id: " + district_id);
    await page.goto(districtInfo[trailDistrict]);

    await page.waitForSelector(".row");

    let links = await page.$$(".col-md-2 a");
    let info = {};
    for (let link of links) {
      let linkName = await link.textContent();
      let lastCha = linkName?.charAt(linkName.length - 1);
      if (lastCha == "2" || lastCha == "3" || lastCha == "4") {
        continue;
      }
      let href = await link.getAttribute("href");
      info[linkName as string] = url + href;
    }
    for (let trail in info) {
      console.log("Now : " + trail);
      try {
        await page.goto(info[trail]);
        // console.log("sleep 1 seconds");
        // await sleep(200);

        //  Image
        let img_src = await (
          await page.$("img.img-fluid")
        )?.getAttribute("src");
        let route = (await page.textContent("table"))?.replace(/\s/g, "");

        //  Transportation
        let transportationGroup = await page.$$(".transport td");
        let transportDetail: string | string[] = [];
        for (let transportation of transportationGroup) {
          let transport = await transportation.textContent();
          transportDetail.push(transport!);
        }
        transportDetail = transportDetail.join(",").replace(/\s/g, "");

        //  Description
        let descriptionGroup = await page.$$("p");
        let descriptionArray: string | null | undefined | string[] = [];
        for (let p of descriptionGroup) {
          let description = await p.textContent();
          descriptionArray.push(description!);
        }
        console.log("Length of description: " + descriptionArray.join().length);

        //  Get 10 starts
        let starsArray: number[] = [];
        let stars = await page.$$(".content .star");
        for (let s of stars) {
          let star = await s.getAttribute("src");
          if (star === "svg/star.svg") {
            star = "1";
          }
          if (star === "svg/star_half.svg") {
            star = "0.5";
          }
          if (star === "svg/star_empty.svg") {
            star = "0";
          }
          starsArray.push(parseFloat(star!));
        }

        //  First 5 starts for Difficulty
        let difficulty = 0;
        let starOfDifficulty = starsArray.slice(0, 5);
        for (let i = 0; i < starOfDifficulty.length; i++) {
          difficulty += starOfDifficulty[i];
        }

        //  Last 5 starts for Scenery
        let scenery = 0;
        let starOfScenery = starsArray.slice(-5);
        for (let i = 0; i < starOfScenery.length; i++) {
          scenery += starOfScenery[i];
        }
        console.log("Difficulty: " + difficulty + " Scenery: " + scenery);

        //  Distance
        let distance: number | number[] = [];
        let contents = await page.$$("div.content");
        for (let content of contents) {
          let text = (await content.textContent())?.substring(0, 2);
          if (text !== "長度") {
            continue;
          }
          let detail = (await content.textContent())?.substring(2, 6);
          distance.push(parseFloat(detail as string));
        }

        //  Latitude and Longitude
        let lat_Long = await searchCoorByAddress(trail);
        if (lat_Long.length == 0) {
        }
        let gps_latitude = lat_Long[0];
        let gps_longitude = lat_Long[1];
        //  Latitude = N
        //  Longitude = E

        if (
          !trail ||
          !img_src ||
          !route ||
          !transportDetail ||
          !difficulty ||
          !scenery ||
          !distance
        ) {
          continue;
        }

        trailDetail.push({
          district_id: district_id,
          name: trail!,
          distance: distance[0],
          // route_description: route!,
          difficulty: difficulty,
          scenery_rating: scenery,
          description: descriptionArray.join(" "),
          image_filename: img_src!,
          gps_latitude: gps_latitude!,
          gps_longitude: gps_longitude!,
          transportation: transportDetail,
        });
      } catch (error) {
        console.error(error);
        continue;
        // browser.close();
      }
    }
    // console.log(trailDetail);
    console.log("Scrapping finished");
    fs.writeFileSync(
      "scraper/mountain_info_data.json",
      JSON.stringify(trailDetail),
      { encoding: "utf-8" }
    );
    browser.close(); // close after date being saved
  }

  return trailDetail;
}

search();
