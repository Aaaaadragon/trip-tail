const casual = require('casual')
const fs = require('fs')
function genFakeData(number) {
//   let data = { userData: [], userProfile: [], trailRecord: [] };
    let data = []
  let mountainNames = getMountainNames()
  for (let i = 0; i <= number; i++) {
    const userData = { username: casual.full_name };
    // data.userData.push(userData);

    const userProfile = {
      gender: casual.random_element(['female','male']),
      hike_frequency: casual.integer((from = 0), (to = 23)),
    };
    // data.userProfile.push(userProfile);
    const duration= casual.double(from=300000,to=32400000)
    const trailRecord = {
      user_id: casual.integer((from = 0), (to = number)),
      trail_name: casual.random_element(mountainNames),
      gps_record:[[casual.random*100,casual.random*100]],
      duration:casual.integer(from=300000,to=144000000),
      distance:duration*0.00000111*(casual.random_element([0.8,0.831,0.86,0.88,0.91,0.923,0.947,0.97,1.02,1.01,1.0421,1.0451,1.05653,1.0645,1.08,1.1,1.12,1.13,1.16,1.17,1.19,1.21]))
    };

    combineData = {
        ...userData,...userProfile,...trailRecord
    }
    
    // data.trailRecord.push(trailRecord)
    data.push(combineData)
  }
  fs.writeFileSync(
    "seeds/fake_user_data.json",
    JSON.stringify(data),
    { encoding: "utf-8" }
  );
}

function getMountainNames() {
  const JSONDataInString = fs.readFileSync("scraper/mountain_info_data.json", {
    encoding: "utf-8",
  });
  const MountainInfoData = JSON.parse(JSONDataInString);
  let mountainNames = [];
  for (let mountainInfo of MountainInfoData) {
    try {
      mountainNames.push(mountainInfo["name"]);
      console.log(mountainInfo["name"], "info being inserted...");
    } catch (error) {
      console.log(
        "ERROR:",
        error,
        "refer to mountain name:",
        mountainInfo["name"]
      );
    }
  }
  return mountainNames;
}

genFakeData(3000)