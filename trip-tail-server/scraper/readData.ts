import fs from 'fs'
import { Knex } from 'knex'
import { knex } from "../db";
async function main(knex:Knex){

    const JSONDataInString = fs.readFileSync('scraper/mountain_info_data.json',{encoding:'utf-8'})
    console.log('Total mountain_info amount:',JSON.parse(JSONDataInString).length)
    const MountainInfoData = JSON.parse(JSONDataInString)
    // console.log(MountainInfoData.slice(-1)[0])

    for(let mountainInfo of MountainInfoData){
        try {
            console.log(mountainInfo['name'],'being inserted...')

            await knex('mountain_info').insert(mountainInfo)
        } catch (error) {
            console.log('ERROR:',error, 'refer to mountain name:', mountainInfo['name'])
        }
    }
    console.log('all data has been inserted successfully')
    
}

main(knex)