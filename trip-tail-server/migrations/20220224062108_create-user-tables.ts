import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!(await knex.schema.hasTable('district'))){
        await knex.schema.createTable('district',table=>{
            table.increments('id')
            table.string('name')
        })
    }
    if(!(await knex.schema.hasTable('user'))) {
        await knex.schema.createTable('user',table=>{
            table.increments('id').unique()
            table.string('username',20).unique().notNullable()
            table.string('password',60).unique().nullable()
            table.string('email',120).unique().nullable()
            table.string('email_code',6).nullable()
            table.integer('email_code_created_at').nullable()
            table.boolean('passed_oauth').defaultTo(false)
            table.timestamps(false,true)
        })
    }
    if(!(await knex.schema.hasTable('user_profile'))){
        await knex.schema.createTable('user_profile',table=>{
            table.integer('user_id').references('id').inTable('user').unique()
            table.string('nickname',10).nullable()
            table.string('gender',7).nullable()
            table.integer('district_id').references('id').inTable('district').nullable()
            table.string('icon_photo').nullable()
            table.integer('hike_frequency').nullable()
            table.boolean('is_private').defaultTo(false)
        })
    }

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('user_profile')
    await knex.schema.dropTableIfExists('user')
    await knex.schema.dropTableIfExists('district')

}

