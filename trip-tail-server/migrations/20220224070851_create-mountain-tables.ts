import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

    if(!(await knex.schema.hasTable('mountain_info'))){
        await knex.schema.createTable('mountain_info', table=>{
            table.increments('id')
            table.integer('district_id').references('id').inTable('district').notNullable()
            table.string('name',30).notNullable()
            table.string('image_filename',100)
            table.float('difficulty').notNullable()
            table.float('scenery_rating').notNullable()
            table.float('distance').notNullable()
            table.float('gps_latitude')
            table.float('gps_longitude')
            table.string('gps_filename',100)
            table.string('route_description')
            table.string('description',3000).notNullable()
            table.string('transportation',500)
        })
    }


    if(!(await knex.schema.hasTable('trail_record'))){
        await knex.schema.createTable('trail_record',table=>{
            table.increments('id')
            table.integer('user_id').references('id').inTable('user')
            table.string('trail_name',10).notNullable()
            table.integer('mountain_id').references('id').inTable('mountain_info').nullable()
            table.json('gps_record').notNullable()
            table.integer('duration').notNullable()
            table.float('distance')
            table.boolean('is_private').defaultTo(true)
            table.timestamps(false,true)
        })
    }
    

    if(!(await knex.schema.hasTable('trail_record_post'))){
        await knex.schema.createTable('trail_record_post',table=>{
            table.increments('id')
            table.integer('user_id').references('id').inTable('user').notNullable()
            table.integer('trail_record_id').references('id').inTable('trail_record').notNullable()
            table.string('title',20).notNullable()
            table.float('gps_latitude').notNullable()
            table.float('gps_longitude').notNullable()
            table.string('image_filename')
            table.string('description',1000)
            table.timestamps(false,true)
        })
    }

    if(!(await knex.schema.hasTable('label_type'))){
        await knex.schema.createTable('label_type',table=>{
            table.increments('id')
            table.string('title',10)
        })
    }

    if(!(await knex.schema.hasTable('map_label'))){
        await knex.schema.createTable('map_label',table=>{
            table.increments('id')
            table.integer('user_id').references('id').inTable('user').notNullable()
            table.integer('label_type_id').references('id').inTable('label_type').notNullable()
            table.string('title',10).notNullable()
            table.string('image_filename',100)
            table.float('gps_latitude').notNullable()
            table.float('gps_longitude').notNullable()
            table.specificType('like_members','integer ARRAY')
            table.specificType('dislike_members','integer ARRAY')
            table.boolean('is_private').notNullable().defaultTo(false)
            table.timestamps(false,true)
        })
    }
    if(!(await knex.schema.hasTable('trip_group'))){
        await knex.schema.createTable('trip_group',table=>{
            table.increments('id')
            table.boolean('is_private').defaultTo(false)
            table.specificType('group_member','integer ARRAY').notNullable()
            table.integer('mountain_id').references('id').inTable('mountain_info').notNullable()
            table.string('description',1000)
            table.string('venue',25).notNullable()
            table.datetime('start_at').notNullable()
            table.datetime('end_at').notNullable()
            table.integer('member_limit').notNullable()
            table.string('gender_limit')
            
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('mountain_detail')
    await knex.schema.dropTableIfExists('trail_record_post')
    await knex.schema.dropTableIfExists('trail_record')
    await knex.schema.dropTableIfExists('map_label')
    await knex.schema.dropTableIfExists('label_type')
    await knex.schema.dropTableIfExists('trip_group')
    await knex.schema.dropTableIfExists('mountain_info')
}

