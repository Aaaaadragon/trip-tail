import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!(await knex.schema.hasTable('friend_status'))) {
        await knex.schema.createTable('friend_status', table=>{
            table.increments('id')
            table.string('status').notNullable()
        })
    }
    if(!(await knex.schema.hasTable('friendship'))) {
        await knex.schema.createTable('friendship',table=>{
            table.increments('id')
            table.integer('sender_id').references('id').inTable('user').notNullable()       
            table.integer('receiver_id').references('id').inTable('user').notNullable()       
            table.integer('status_id').references('id').inTable('friend_status').notNullable()
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('friendship')
    await knex.schema.dropTableIfExists('friend_status')
}

