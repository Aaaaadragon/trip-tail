import { Knex } from "knex";
import { JWTpayload, LoginUserWithPasswordDTO, RegisterUserWithPasswordDTO } from "../models";
import jwt from "jwt-simple";
import { jwtConfig } from "../jwt";
import { HttpError } from "../http-error";
import { comparePassword, hashPassword } from "../hash";
const SECOND = 1000;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;
const DAY = 24 * HOUR;



export class UserService {
  constructor(private knex: Knex) {}

  table() {
    return this.knex("user");
  }

  createToken(user: Omit<JWTpayload, "exp">) {
    let payload: JWTpayload = {
      id: user.id,
      username: user.username,
      email: user.email,
      exp: Date.now() + 90 * DAY,
    };
    let token: string = jwt.encode(payload, jwtConfig.jwtSecret);
    return token;
  }

  decodeToken(token: string) {
    let payload: JWTpayload;
    try {
      payload = jwt.decode(token, jwtConfig.jwtSecret);
    } catch (error) {
      throw new HttpError("Invalid JWT token: " + error, 404);
    }
    if (payload.exp <= Date.now()) {
      throw new HttpError("Expired JWT token, expired at" + payload.exp, 401);
    }
    return payload;
  }

  async createRegisterUserEmailCode(username:string,email:string,email_code:number){
    try {
      
      let row = await this.table().insert({username,email,email_code}).returning('id')
      setTimeout(async()=>{
        let result = await this.table().where({email,passed_oauth:false},).del()
        console.log(`account(${result[0]}) has been deleted due to oath failed after 30 minutes`)
      },30*MINUTE)
      return row[0] as number
    } catch (error) {
      console.log({error})
      return 0
    }
  }

  async createUserEmailCode(email:string,email_code:number){
    
    let row = await this.table().update({email_code}).where({email}).returning('id')
    setTimeout(async()=>{
      let result = await this.table().where({email,passed_oauth:true})
      if(result[0].email){
        await this.table().update({email_code:'000000'}).returning('id')
        console.log(`account(${result[0]}) has been deleted due to oath failed after 30 minutes`)
      }
    },30*MINUTE)
    return row[0] as number
  }

  async verifyEmailCode(email:string, email_code:number){
    let row = await this.table().select('id','username').where({email,email_code})

    if(row[0]){
      let username = row[0].username
      let id = row[0].id
      await this.table().update({passed_oauth:true}).where({email,email_code})
      let token:string = this.createToken({id,username,email})
      return {status:'success',token}
    }else{
      return {status:'fail'}
    }
  }


  async checkIfUserEmailExist(email:string){
    let row = await this.table().select('id').where({email})
    console.log('email:',{row})
    if(row[0]){
      console.log('email exist')
      return true
    }else{
      console.log('email not exist')
      return false
    }
  }
  async checkIfUsernameExist(username:string){
    let row = await this.table().select('id').where({username})
    console.log('username:',{row})
    if(row[0]){
      console.log('username exist')
      return true
    }else{
      console.log('username not exist')
      return false
    }
  }


  async register(user: RegisterUserWithPasswordDTO) {
    //TODO length, syntax checking
    let rows: any[] = await this.table()
      .insert({
        username: user.username,
        password: await hashPassword(user.password),
      })
      .returning("id");
    await this.knex('user_profile').insert({user_id:rows[0].id})

    let id = rows[0].id as number;
    let token: string = this.createToken({ id, username: user.username });
    return token;
  }

  async loginWithPassword(user: LoginUserWithPasswordDTO) {
    let row = await this.table()
      .select("id", "password", "username")
      .where({ username: user.username }).first()

      if(!row){
          //TODO error message to be correct
          throw new HttpError('用戶名稱無效: '+user.username,401)
      }
      let isMatch = await comparePassword({password:user.password,hashedPassword:row.password})
      if(!isMatch){
          throw new HttpError('帳號或密碼錯誤',401)
      }
      let token:string = this.createToken(row)
      return token
  }

  async deleteAfterDue(){
    
  }

}
