import { Knex } from "knex";
import { UserProfileDTO } from "../models";

export class UserProfileService{
    constructor(private knex:Knex){}
    table(){
        return this.knex("user_profile")
    }

    
    async updateProfile(profile:UserProfileDTO){
        //check if profile already exit
        let record = await this.table().select('*').where("user_id","=",profile.user_id).first()
        console.log(record)
        if(record){
            let row = await this.table().update(profile).where("user_id","=",profile.user_id).returning('*')
            console.log('profiles has been created')
            return row[0]
        }else{
            let row =  await this.table().insert(profile).returning('*')
            console.log('new user profiles has been created')
            return row[0]
        }
    }

    async getUserProfile(user_id:number){
        let row = await this.table().select('*').where({user_id}).first()
        console.log('get user profile')
        return row as UserProfileDTO
    }

    async getOldPhoto(user_id:number){
        let row = await this.table().select('icon_photo').where({user_id}).first()
        if(!row){
            return row = ' '
        }else{
            return row = row.icon_photo
        }
    }
}
