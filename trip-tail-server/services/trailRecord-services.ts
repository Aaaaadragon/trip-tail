import { Knex } from "knex";
import { TrailRecordDTO, TrailRecordPostDTO } from "../models";

export class TrailRecordService {
    constructor(private knex:Knex){
        
    }
    table(){
        return this.knex("trail_record")
    }
    subTable(){
        return this.knex('trail_record_post')
    }
    
    async getAllBriefRecord(user_id:number){
        let trailRecords = await this.table().select('id','trail_name','duration','distance','created_at','gps_record').where({user_id}).orderBy('updated_at',"desc")
        let allTrailRecordPosts = {}



        for(let trailRecord of trailRecords){
            let trail_record_id = trailRecord.id
            let trailRecordPosts = await this.subTable().select('*').where({trail_record_id})
            allTrailRecordPosts[trail_record_id] = trailRecordPosts
        }

        return {trailRecords, allTrailRecordPosts}
    }

    async createTrailRecord(trailRecord:TrailRecordDTO){
        let row = await this.table().insert(trailRecord).returning('id')
        console.log('new trail record has been created. id:',row[0].id)
        return row[0] as number
    }

    async createTrailRecordPost(trailRecordPost:TrailRecordPostDTO){
        let row = await this.subTable().insert(trailRecordPost).returning('id')
        console.log('new trail record has been created. id:',row[0].id)
        return row[0] as number
    }

    async getTrailRecordPost(trail_record_id:number,user_id:number){
        let row = await this.subTable().select('*').where({trail_record_id,user_id})
        return row as TrailRecordPostDTO[]
    }

    async getDetailRecordById(id:number,user_id:number){
        let row = await this.table().select('*').where({id,user_id}).first()
        return row as TrailRecordDTO
    }
    
    async updateNameById(trail_name:string,id:number,user_id:number){
        let row = await this.table().update({trail_name}).where({id,user_id}).returning('id')
        console.log(`trail name has been updated, reference trail_record id:${id}`)
        return row[0] as number
    }
    
    async updateTrailRecordPost(trailRecordPost:TrailRecordPostDTO){
        let row = await this.subTable().update(trailRecordPost).returning('id')
        console.log(`trail trail post has been updated, reference trail_record_post id:${row[0]}`)
        return row[0] as number
    }

    async deleteTrailRecordById(id:number,user_id:number){
        let trail_record_id = id
        let deleted_post_title = await this.subTable().where({trail_record_id,user_id}).del().returning('title')
        console.log(`trail record post title:${[...deleted_post_title]}) has been deleted`)
        let row = await this.table().where({id,user_id}).del().returning('trail_name')
        console.log(`trail record(id:${id}; trail name:${row[0]}) has been deleted`)
        return row[0] as string
    }

    async deleteTrailRecordPostById(id:number,user_id:number){
        let row = await this.subTable().where({id,user_id}).del().returning('title')
        console.log(`trail record post(id:${id}; title:${row[0]}) has been deleted`)
        return row[0] as string
    }
    async deleteAllPostByTrailRecordId(trail_record_id:number,user_id:number){
        let row = await this.subTable().where({trail_record_id,user_id}).del().returning('title')
        console.log(`trail record post title:${[...row]}) has been deleted`)
        
    }

}

