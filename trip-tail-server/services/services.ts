import { knex } from "../db";
import { MapLabelService } from "./mapLabel-services";
import { MountainInfoService } from "./mountainInfo-services";
import { TrailRecordService } from "./trailRecord-services";
import { UserService } from "./user-services";
import { UserProfileService } from "./userProfile-services";

export let userService = new UserService(knex);
export let mountainInfoService = new MountainInfoService(knex);
export let trailRecordService = new TrailRecordService(knex);
export let mapLabelService = new MapLabelService(knex);
export let userProfileService = new UserProfileService(knex);
