import { Knex } from "knex";
import { MapLabelDTO } from "../models";
export class MapLabelService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("map_label");
  }

  async getAllMapLabel() {
    let rows = await this.table()
      .select(
        "map_label.id",
        "map_label.user_id",
        "map_label.label_type_id",
        "map_label.title",
        "map_label.image_filename",
        "map_label.gps_latitude",
        "map_label.gps_longitude",
        "map_label.like_members",
        "map_label.dislike_members",
        "map_label.created_at",
        "user.username",
        "user_profile.nickname",
        "user_profile.gender",
        "user_profile.icon_photo",
      ).where("map_label.is_private","=",false)
      .join("user", "map_label.user_id", "user.id")
      .leftJoin("user_profile", "user.id", "user_profile.user_id").distinct('map_label.id')
    return rows as MapLabelDTO[];
  }

  async getAllMapLabelWithUserId(user_id: number) {
    let rows = await this.table().select("*").where({ user_id }).orderBy('updated_at',"desc");
    return rows as MapLabelDTO[];
  }

  async createMapLabel(mapLabel: MapLabelDTO) {
    let row = await this.table().insert(mapLabel).returning("id");
    console.log("new map label has been created id", row[0].id);
    return row[0];
  }

  async getMapLabelById(id:number){
    let row = await this.table().select("*").where({id}).first()
    return row
  }

  async updateMapLabelById(
    mapLabel: Partial<MapLabelDTO>,
    id: number,
    user_id?: number
  ) {
      let updateDependency = user_id?{user_id,id}:{id}
    let row = await this.table()
      .update(mapLabel)
      .where({...updateDependency})
      .returning("id");
    console.log(`map label has been updated, reference map label id:${id}`);
    return row[0] as number;
  }

  async deleteMapLabelById(id: number, user_id: number) {
    let row = await this.table()
      .where({ id, user_id })
      .del()
      .returning("title");
    console.log(`map label(${id}); map label name:${row[0]}) has been deleted`);
    return row[0] as string;
  }
}
