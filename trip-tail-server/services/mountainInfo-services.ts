import { Knex } from "knex";
import { MountainInfoDTO } from "../models";

export class MountainInfoService {
  constructor(private knex: Knex) {}

  table() {
    return this.knex("mountain_info");
  }

  async getAllMountainInfo() {
    let rows = await this.knex("mountain_info")
      .select(
        "*",
        "mountain_info.id as id",
        "district.id as district_id",
        "mountain_info.name as name",
        "district.name as district_name"
      )
      .join("district", "mountain_info.district_id", "district.id");
    return rows as MountainInfoDTO[];
  }

  async getMountainInfoById(id: number) {
    let row = await this.table().select("*").where({ id }).first();
    return row as MountainInfoDTO;
  }

  async getDistrict() {
    let rows = await this.table()
      .select("district.id as district_id")
      .join("district", "mountain_info.district_id", "district.id");
    return rows as MountainInfoDTO[];
  }

  async createMountainInfo(mountainInfo: MountainInfoDTO) {
    // check if same record already been inserted by name
    let pre_checkResult = await this.table()
      .select("id")
      .where({ name: mountainInfo.name })
      .first();
    if (pre_checkResult) {
      console.log(
        "Mountain_info name:",
        mountainInfo.name,
        "already exist. Failed to create new mountain info."
      );
      return;
    }
    // insert new mountain info to database
    let row: any[] = await this.table().insert(mountainInfo).returning("id");

    console.log(
      `New mountain info(id:${row[0]}) has been created successfully!`
    );

    return row[0];
  }

  async deleteMountainInfoById(id: number) {
    await this.table().where({ id }).del();
    console.log(`Mountain info(id:${id} has been deleted.)`);
    return;
  }

  async updateAllInfoById(newMountainInfo: MountainInfoDTO, id: number) {
    let row = await this.table()
      .update(newMountainInfo)
      .where({ id })
      .returning("name");
    console.log(
      `mountain info(id:${id} name:${row[0]}) all not nullable column has been updated!`
    );
    return row[0];
  }

  async updateDistrictIdById(district_id: number, id: number) {
    let row = await this.table()
      .update({ district_id })
      .where({ id })
      .returning("name");
    console.log(
      `mountain info(id:${id} name:${row[0]}) district_id has been updated to ${district_id}!`
    );
    return;
  }

  async updateDistanceById(distance: number, id: number) {
    let row = await this.table()
      .update({ distance })
      .where({ id })
      .returning("name");
    console.log(
      `mountain info(id:${id} name:${row[0]}) ${
        Object.keys({ distance })[0]
      } has been updated to ${distance}!`
    );
    return;
  }
  async updateDifficultyById(difficulty: number, id: number) {
    let row = await this.table()
      .update({ difficulty })
      .where({ id })
      .returning("name");
    console.log(
      `mountain info(id:${id} name:${row[0]}) ${
        Object.keys({ difficulty })[0]
      } has been updated to ${difficulty}!`
    );
    return;
  }

  async updateSpecColumnById(data: Partial<MountainInfoDTO>, id: number) {
    let row = await this.table().update(data).where({ id }).returning("name");
    console.log(
      `mountain info(id:${id} name:${row[0]}) ${
        Object.keys(data)[0]
      } has been updated to ${data}!`
    );
    return;
  }
}
