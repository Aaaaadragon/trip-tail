import { compare, hash } from "bcryptjs";

let ROUND = 12

export async function hashPassword(password:string){
    let digest:string = await hash(password,ROUND)
    return digest
}


export async function comparePassword(data:{password:string,hashedPassword:string}){
    let isMatch:boolean = await compare(data.password,data.hashedPassword)
    return isMatch
}