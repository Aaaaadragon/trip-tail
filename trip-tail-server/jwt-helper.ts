import { Bearer } from "permit";
import { HttpError } from "./http-error";
import { JWTpayload } from "./models";
import express from 'express'
import { userService } from "./services/services";

const permit = new Bearer({
    query: 'access_token',
  })
  

export function requireJWTPayload(req:express.Request):JWTpayload{
    let token:string
    try {
        token = permit.check(req)
    } catch (error) {
        throw new HttpError('Invalid Bearer Token', 401)
    }
    let payload = userService.decodeToken(token)
    return payload
}