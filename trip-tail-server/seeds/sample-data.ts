import { Knex } from "knex";
import fs from "fs";

export async function seed(knex: Knex) {
  async function seedFriendStatus(row: { id?: number; status: string }) {
    try {
      if (
        !(await knex("friend_status").select("status").where({ id: 2 }).first())
      ) {
        throw new Error("friend_status table seeds creation failed");
      }
    } catch (error) {
      await knex("friend_status").insert(row);
    }
  }

  async function seedDistrict(row: { id?: number; name: string }) {
    try {
      if (!(await knex("district").select("name").where({ id: 8 }).first())) {
        throw new Error("district table seeds creation failed");
      }
    } catch (error) {
      await knex("district").insert(row);
    }
  }
  async function seedLabelType(row: { id?: number; title: string }) {
    try {
      if (
        !(await knex("label_type").select("title").where({ id: 8 }).first())
      ) {
        throw new Error("label_type table seeds creation failed");
      }
    } catch (error) {
      await knex("label_type").insert(row);
    }
  }

  async function seedMountainInfo() {
    const JSONDataInString = fs.readFileSync(__dirname+'/'+
      "jsonFiles/mountain_info_data.json",
      { encoding: "utf-8" }
    );
    const MountainInfoData = JSON.parse(JSONDataInString);

    for (let mountainInfo of MountainInfoData) {
      try {
        console.log(mountainInfo["name"], "info being inserted...");
        await knex("mountain_info").insert(mountainInfo);
        mountainInfo = {
          ...mountainInfo,
          description: mountainInfo.description.trim(),
        };
      } catch (error) {
        console.log(
          "ERROR:",
          error,
          "refer to mountain name:",
          mountainInfo["name"]
        );
      }
    }
    console.log("all seed data has been inserted successfully");
  }

  async function seedUserData() {
    const userDataInString = fs.readFileSync(__dirname+'/'+
      "jsonFiles/database/_user__202203162206.json",
      { encoding: "utf-8" }
    );
    const userData = JSON.parse(userDataInString).user;

    const trailRecordDataInString = fs.readFileSync(__dirname+'/'+
      "jsonFiles/database/trail_record_202203162206.json",
      { encoding: "utf-8" }
    );
    const trailRecordData = JSON.parse(trailRecordDataInString).trail_record;

    const trailPostDataInString = fs.readFileSync(__dirname+'/'+
      "/jsonFiles/database/trail_record_post_202203162206.json",
      { encoding: "utf-8" }
    );
    const trailPostData = JSON.parse(trailPostDataInString).trail_record_post;

    try {
      await knex("user").insert(userData);
      await knex("trail_record").insert(trailRecordData);
      await knex("trail_record_post").insert(trailPostData);
    } catch (error) {
      console.log("ERROR:", error);
    }
  }

  await seedFriendStatus({ id: 1, status: "pending" });
  await seedFriendStatus({ id: 2, status: "friend" });
  await seedDistrict({ id: 0, name: "不透露" });
  await seedDistrict({ id: 1, name: "新界" });
  await seedDistrict({ id: 2, name: "九龍" });
  await seedDistrict({ id: 3, name: "香港島" });
  await seedDistrict({ id: 4, name: "離島" });
  await seedLabelType({ id: 1, title: "警告" });
  await seedLabelType({ id: 2, title: "景點" });
  await seedLabelType({ id: 3, title: "動物" });
  await seedLabelType({ id: 4, title: "商舖" });
  await seedLabelType({ id: 5, title: "飲品供應" });
  await seedLabelType({ id: 6, title: "洗手間" });
  await seedLabelType({ id: 7, title: "其他" });
  await seedMountainInfo();
  await seedUserData();
}

// await seedDistrict({ name: "麥理浩徑" });
// await seedDistrict({ name: "衛奕信徑" });
// await seedDistrict({ name: "港島徑" });
// await seedDistrict({ name: "鳳凰徑" });
