import express from "express";
import { HttpError } from "../http-error";
import { requireJWTPayload } from "../jwt-helper";
import { JWTpayload, TrailRecordDTO, TrailRecordPostDTO } from "../models";
import { TrailRecordService } from "../services/trailRecord-services";
import { upload } from "../upload";
import { wrapControllerMethod } from "./rest-controller";

export class TrailRecordController {
  router = express.Router();

  constructor(private trailRecordService: TrailRecordService) {
    this.router.post(
      "/trail_record/create",
      wrapControllerMethod(this.createTrailRecord)
    );
    this.router.get(
      "/trail_record/getAll",
      wrapControllerMethod(this.getAllRecordByUserId)
    );
    this.router.delete(
      "/trail_record/id/:id/delete",
      wrapControllerMethod(this.deleteTrailRecordById)
    );
    this.router.put(
      "/trail_record/id/:id/updateName",
      wrapControllerMethod(this.updateTrailNameById)
    );

    this.router.post(
      "/trail_record_post/create",
      upload.single("trail_record_photo"),
      wrapControllerMethod(this.createTrailRecordPost)
    );

    this.router.get(
      "/trail_record_post/id/:id/getAll",
      wrapControllerMethod(this.getAllTrailRecordPost)
    );

    this.router.delete(
      "/trail_record_post/id/:id/delete",
      wrapControllerMethod(this.deleteTrailRecordPost)
    );

    this.router.delete(
      "/trail_record_post/id/:id/deleteAllPost",
      wrapControllerMethod(this.deleteAllPostByTrailRecordId)
    );
  }

  createTrailRecord = async (req: express.Request) => {
    //   console.log('try to create trail record')
    let payload: JWTpayload = requireJWTPayload(req);
    let trailRecord = req.body;
    if (!trailRecord.trail_name)
      throw new HttpError("missing trail_name in request body", 400);
    if (!trailRecord.gps_record)
      throw new HttpError("missing gps_record in request body", 400);
    if (!trailRecord.duration)
      throw new HttpError("missing duration in request body", 400);
    if (!trailRecord.distance)
      throw new HttpError("missing distance in request body", 400);
    let trailRecordDTO: TrailRecordDTO = {
      ...trailRecord,
      user_id: payload.id,
      gps_record: JSON.stringify(trailRecord.gps_record),
    };

    let newItemId = await this.trailRecordService.createTrailRecord(
      trailRecordDTO
    );
    return { newItemId };
  };

  getAllRecordByUserId = async (req: express.Request) => {
    let payload: JWTpayload = requireJWTPayload(req);
    let user_id = payload.id;
    let result = await this.trailRecordService.getAllBriefRecord(
      user_id
    );
      const trailRecords = result.trailRecords
      const allTrailRecordPosts = result.allTrailRecordPosts
    return {trailRecords,allTrailRecordPosts};
  };

  deleteTrailRecordById = async (req: express.Request) => {
    let id = +req.params.id;
    if (!id) {
      throw new HttpError("expect numeric req.params.id", 400);
    }
    let payload: JWTpayload = requireJWTPayload(req);
    let user_id = payload.id;
    let deletedItemId = await this.trailRecordService.deleteTrailRecordById(
      id,
      user_id
    );
    return { deletedItemId };
  };

  updateTrailNameById = async (req: express.Request) => {
    console.log("update name");
    let id = +req.params.id;
    if (!id) {
      throw new HttpError("expect numeric req.params.id", 400);
    }
    console.log(req.body);
    let trail_name = req.body.trail_name;
    if (!trail_name) {
      throw new HttpError("missing trail name in in request body", 400);
    }
    let payload: JWTpayload = requireJWTPayload(req);
    let user_id = payload.id;
    let updatedItemId = await this.trailRecordService.updateNameById(
      trail_name,
      id,
      user_id
    );
    return { updatedItemId };
  };

  getAllTrailRecordPost = async (req: express.Request) => {
    let trail_record_id = +req.params.id;
    if (!trail_record_id) {
      throw new HttpError("expect numeric req.params.id", 400);
    }
    let payload: JWTpayload = requireJWTPayload(req);
    let user_id = payload.id;
    let trailRecordPosts = await this.trailRecordService.getTrailRecordPost(
      trail_record_id,
      user_id
    );
    return trailRecordPosts;
  };

  createTrailRecordPost = async (req: express.Request) => {
    let payload = requireJWTPayload(req);
    let user_id = payload.id;
    // let mapLabel = JSON.parse(JSON.stringify(req.body))
    let trail_record_post = req.body;
    let file: any = req.file;
    let image_filename: string;
    if (!trail_record_post.title)
      throw new HttpError("missing title in request body", 400);
    if (!trail_record_post.trail_record_id)
      throw new HttpError("missing  trail_record_id in request body", 400);
    if (!trail_record_post.gps_latitude || !trail_record_post.gps_longitude)
      throw new HttpError("missing gps data in request body", 400);
    if (!file) {
      image_filename = "";
    } else {
      image_filename = file.key || file.filename;
    }
    let trailRecordPostDTO: TrailRecordPostDTO = {
      ...trail_record_post,
      user_id,
      image_filename,
    };
    delete trailRecordPostDTO['trail_record_photo']
    console.log({ trailRecordPostDTO });
    let newItemId =
      this.trailRecordService.createTrailRecordPost(trailRecordPostDTO);
    return newItemId;
  };

  deleteTrailRecordPost = async (req: express.Request) => {
    let id = +req.params.id;
    if (!id) {
      throw new HttpError("expect numeric req.params.id", 400);
    }
    let payload: JWTpayload = requireJWTPayload(req);
    let user_id = payload.id;
    let deletedItemId = await this.trailRecordService.deleteTrailRecordPostById(
      id,
      user_id
    );
    return deletedItemId;
  };

  deleteAllPostByTrailRecordId = async (req: express.Request) => {
    let trail_record_id = +req.params.id;
    if (!trail_record_id) {
      throw new HttpError("expect numeric req.params.id", 400);
    }
    let payload: JWTpayload = requireJWTPayload(req);
    let user_id = payload.id;
    let deletedItemId =
      await this.trailRecordService.deleteAllPostByTrailRecordId(
        trail_record_id,
        user_id
      );
    return deletedItemId;
  };
}
