import express from "express";
import { HttpError } from "../http-error";
import { LoginUserWithPasswordDTO, RegisterUserWithPasswordDTO } from "../models";
import { UserService } from "../services/user-services";
import { wrapControllerMethod } from "./rest-controller";
import {getTransport} from '../email-transport'
export class UserController {
  router = express.Router();

  constructor(private userService: UserService) {
    this.router.post("/user/register", wrapControllerMethod(this.register));
    this.router.post(
      "/user/login/password",
      wrapControllerMethod(this.loginWithPassword)
    );
    this.router.post(
      "/user/register/email",wrapControllerMethod(this.registerWithEmail)
    )
    this.router.post(
      "/user/login/email",wrapControllerMethod(this.loginWithEmail)
    )
    this.router.post(
      "/user/verify/emailCode",wrapControllerMethod(this.verifyUserEmailCode)
    )
  }
  loginWithPassword = async(req:express.Request)=>{
      let {username, password} = req.body
      if (!username) throw new HttpError('missing username in request body', 400)
      if (!password) throw new HttpError('missing password in request body', 400)
      let user: LoginUserWithPasswordDTO = {username,password}
      let token = await this.userService.loginWithPassword(user)
      return {token}
  }
  register = async (req:express.Request)=>{
    let {username, password} = req.body
    
    if (!username) throw new HttpError('missing username in request body', 400)
    if (username.length<3||username.length>25) throw new HttpError('用戶名稱最少3個字元，最多25個字元', 400)
    if (!password) throw new HttpError('missing password in request body', 400)
    if (password.length<8) throw new HttpError('密碼長度最少8個字元', 400)
    if(await this.userService.checkIfUsernameExist(username)){
      throw new HttpError('此用戶名稱已被使用', 400)
    }
    let user: RegisterUserWithPasswordDTO={username,password}
    let token = await this.userService.register(user)
    return {token}
  }
  registerWithEmail = async (req:express.Request)=>{
    let {username,email} = req.body
    if (!username) throw new HttpError('missing username in request body', 400)
    if(await this.userService.checkIfUsernameExist(username)){
      throw new HttpError('此用戶名稱已被使用', 400)

    }
    if (!email) throw new HttpError('missing email in request body', 400)
    if(await this.userService.checkIfUserEmailExist(email)){
      throw new HttpError('此電郵已經被註冊',400)
    } 
    
    let email_code = +(Math.random() * 999999).toFixed(0);
    while (email_code < 100000) {
      email_code = +(Math.random() * 999999).toFixed(0);
    }
    try {
      
      let emailTransport = await getTransport()
      
      let result = await emailTransport.sendMail({
        from: "Trip Tail<teckyallen05@gmail.com>",
        to: email,
        subject: "Trip Tail 註冊驗證郵件",
        html: `<p>這是來自Trip Tail的註冊用戶驗證郵件，用戶 ${username} ，你的驗證碼為：</p>
        <p style="text-align: center;"><span style="color: rgb(97, 189, 109);">========================================</span></p>
        <p style="text-align: center;"><span style="color: rgb(209, 72, 65);"><strong><span style="font-size: 28px;">${email_code}</span></strong></span></p>
        <p style="text-align: center;"><span style="color: rgb(235, 107, 86);">（<strong>驗證碼將在30分鐘後失效，請儘快完成註冊</strong>）</span></p>
        <p style="text-align: center;"><span style="color: rgb(97, 189, 109);">========================================</span></p>
        <p>本郵件由電腦程式自動發出，切勿回覆。</p>`,
      })

      console.log(result)

      let userId = await this.userService.createRegisterUserEmailCode(username,email,email_code)
      return {userId}
      
    } catch (error) {
      throw new HttpError('發送驗證郵件失敗，原因:',error)
    }
    // return {message:"finished register"}
  }

  loginWithEmail = async (req:express.Request)=>{
    let {email} = req.body
    if (!email) throw new HttpError('missing email in request body', 400)
    let isUserExist = await this.userService.checkIfUserEmailExist(email)
    if(!isUserExist) throw new HttpError('電郵地址錯誤或未曾註冊，請重新輸入。',400)
    
    let email_code = +(Math.random() * 999999).toFixed(0);
    while (email_code < 100000) {
      email_code = +(Math.random() * 999999).toFixed(0);
    }
    try {
      
      let emailTransport = await getTransport()
  
      emailTransport.sendMail({
        from: "Trip Tail <auth@triptail.com>",
        to: email,
        subject: "Trip Tail 登入驗證郵件",
        html: `<p>這是來自Trip Tail的用戶驗證郵件，你的登入驗證碼為：</p>
        <p style="text-align: center;"><span style="color: rgb(97, 189, 109);">========================================</span></p>
        <p style="text-align: center;"><span style="color: rgb(209, 72, 65);"><strong><span style="font-size: 28px;">${email_code}</span></strong></span></p>
        <p style="text-align: center;"><span style="color: rgb(235, 107, 86);">（<strong>驗證碼將在30分鐘後失效，請儘快完成註冊</strong>）</span></p>
        <p style="text-align: center;"><span style="color: rgb(97, 189, 109);">========================================</span></p>
        <p>本郵件由電腦程式自動發出，切勿回覆。</p>`,
      })
  
      let userId = await this.userService.createUserEmailCode(email,email_code)
      return {userId}
      
    } catch (error) {
      throw new HttpError('發送驗證郵件失敗，原因:'+error,400)
    }
  }

  verifyUserEmailCode = async (req:express.Request)=>{
    let {email,email_code} = req.body
    if (!email) throw new HttpError('missing email in request body', 400)
    if (!email_code) throw new HttpError('missing email_code in request body', 400)
    let result = await this.userService.verifyEmailCode(email,email_code)
    if(result.status==='success'){
      let token = result.token
      return {token}
    }else{
      throw new HttpError('郵箱或驗證碼錯誤',400)
    }
  }
}
