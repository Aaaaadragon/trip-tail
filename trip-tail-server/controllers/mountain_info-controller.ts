import express from "express";
import { HttpError } from "../http-error";
import { requireJWTPayload } from "../jwt-helper";
import { MountainInfoDTO } from "../models";
import { MountainInfoService } from "../services/mountainInfo-services";
import { wrapControllerMethod } from "./rest-controller";

export class MountainInfoController {
  router = express.Router();

  constructor(private mountainInfoService: MountainInfoService) {
    this.router.post(
      "/mountain_info/create",
      wrapControllerMethod(this.createMountainInfo)
    );
    this.router.get(
      "/mountain_info/getAllInfo",
      wrapControllerMethod(this.getAllMountainInfo)
    );

    this.router.get(
      "/mountain_info/:id/getInfo",
      wrapControllerMethod(this.getMountainInfoById)
    );
  }

  createMountainInfo = async (req: express.Request) => {
    requireJWTPayload(req);
    let mountainInfo: MountainInfoDTO = req.body.mountainInfo;
    if (!mountainInfo.district_id)
      throw new HttpError("missing district_id in request body", 400);
    if (!mountainInfo.name)
      throw new HttpError("missing name in request body", 400);
    if (!mountainInfo.distance)
      throw new HttpError("missing distance in request body", 400);
    if (!mountainInfo.difficulty)
      throw new HttpError("missing difficulty in request body", 400);
    if (!mountainInfo.scenery_rating)
      throw new HttpError("missing scenery_rating in request body", 400);
    if (!mountainInfo.description)
      throw new HttpError("missing description in request body", 400);
    let id = await this.mountainInfoService.createMountainInfo(mountainInfo);
    return { id };
  };

  getAllMountainInfo = async (req: express.Request) => {
    let allMountainInfo: MountainInfoDTO[] =
      await this.mountainInfoService.getAllMountainInfo();
    return allMountainInfo;
  };

  getMountainInfoById = async (req: express.Request) => {
    let id = +req.params.id;
    if (!id) {
      throw new HttpError("expect numeric req.params.id", 400);
    }
    let mountainInfo: MountainInfoDTO =
      await this.mountainInfoService.getMountainInfoById(id);
    return mountainInfo;
  };
}
