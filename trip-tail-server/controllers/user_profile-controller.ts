import express from "express";
import { Multer } from "multer";
import { HttpError } from "../http-error";
import { requireJWTPayload } from "../jwt-helper";
import { JWTpayload, UserProfileDTO } from "../models";
import { UserProfileService } from "../services/userProfile-services";
import { wrapControllerMethod } from "./rest-controller";

export class UserProfileController{
    router = express.Router()

    constructor(private upload:Multer, private userProfileService:UserProfileService){
        this.router.post("/user_profile/update",this.upload.single('icon_photo'),wrapControllerMethod(this.updateUserProfile))
        this.router.get("/user_profile/get",wrapControllerMethod(this.getUserProfile))
    }

    updateUserProfile = async (req:express.Request)=>{
        let payload :JWTpayload = requireJWTPayload(req)
        let user_id = payload.id
        let userProfile = req.body;
        let file:any = req.file
        let icon_photo;
        icon_photo =file?.key || file?.filename
        if(!file){
            let oldPhoto = await this.userProfileService.getOldPhoto(user_id)
            icon_photo = oldPhoto
        }
        if(!userProfile)
            throw new HttpError("missing user profile in request body",400)
        if(userProfile.hike_frequency=='null'||userProfile.hike_frequency=='undefined') userProfile = {...userProfile,hike_frequency:0}
        if(userProfile.gender=='null'||userProfile.gender=='undefined') userProfile = {...userProfile,gender:'default'}
        if(userProfile.district_id=='null'||userProfile.district_id=='undefined') userProfile = {...userProfile,district_id:0}
        if(userProfile.icon_photo=='null'||userProfile.icon_photo=='undefined') userProfile = {...userProfile,icon_photo:''}
        if(userProfile.nickname=='null'||userProfile.nickname=='undefined') userProfile = {...userProfile,nickname:''}
        if(userProfile.is_private=='null'||userProfile.is_private=='undefined') userProfile = {...userProfile,is_private:false}
        let userProfileDTO:UserProfileDTO = {...userProfile,user_id,icon_photo}
        console.log({userProfileDTO})
        let newItem = await this.userProfileService.updateProfile(userProfileDTO)
        return {newItem}
    }
    
    getUserProfile =  async (req:express.Request)=>{
        let payload:JWTpayload = requireJWTPayload(req)
        let user_id = payload.id
        let userProfile = await this.userProfileService.getUserProfile(user_id)
        return userProfile
    }


}