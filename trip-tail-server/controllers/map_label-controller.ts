import express from "express";
import { Multer } from "multer";
import { HttpError } from "../http-error";
import { requireJWTPayload } from "../jwt-helper";
import { JWTpayload, MapLabelDTO } from "../models";
import { MapLabelService } from "../services/mapLabel-services";
import { wrapControllerMethod } from "./rest-controller";

export class MapLabelController{
    router = express.Router()

    constructor(private upload:Multer,private mapLabelService:MapLabelService){
        
        this.router.get(
            "/map_label/getAll",wrapControllerMethod(this.getAllMapLabel)
        )
        this.router.post(
            "/map_label/create", this.upload.single('map_label_photo'),wrapControllerMethod(this.createMapLabel)
        )
        this.router.get(
            "/map_label/getAllWithUserId",wrapControllerMethod(this.getAllMapLabelByUserId)
        )
        this.router.delete(
            "/map_label/id/:id/delete",wrapControllerMethod(this.deleteMapLabelById)
        )
        this.router.put(
            "/map_label/id/:id/updateLikeOrDisLike",wrapControllerMethod(this.updateLikeOrDisLike)
        )
        this.router.put(
            "/map_label/id/:id/updateLabel",wrapControllerMethod(this.updateMapLabel)
        )
        
    }

    getAllMapLabel = async(req:express.Request)=>{
        //TODO get labels depends on user gps position
        let mapLabels = await this.mapLabelService.getAllMapLabel()
        return mapLabels
    }

    createMapLabel = async(req: express.Request)=>{
        let payload = requireJWTPayload(req);
        let user_id = payload.id
        // let mapLabel = JSON.parse(JSON.stringify(req.body))
        let mapLabel = req.body
        console.log(mapLabel)
        let file:any = req.file
        if (!mapLabel.title)
        throw new HttpError("missing title in request body", 400);
        if (!mapLabel.label_type_id)
        throw new HttpError("missing  label_type_id in request body", 400);
        if (!file)
        throw new HttpError("missing photo in request body", 400);
        if (!mapLabel.gps_latitude || !mapLabel.gps_longitude)
        throw new HttpError("missing gps data in request body", 400);
        let photo=file.key || file.filename
        let mapLabelDTO:MapLabelDTO = {...mapLabel,image_filename:photo,user_id,like_members:[],dislike_members:[]}
        let newItemId = this.mapLabelService.createMapLabel(mapLabelDTO)
        return {newItemId}
    }
    
    getAllMapLabelByUserId = async(req:express.Request)=>{
        let payload:JWTpayload = requireJWTPayload(req)
        let user_id = payload.id
        
        let allMapLabel = await this.mapLabelService.getAllMapLabelWithUserId(user_id)
        return allMapLabel as MapLabelDTO[]
    }

    deleteMapLabelById = async(req:express.Request)=>{
        let id = +req.params.id;
        if (!id) {
            throw new HttpError("expect numeric req.params.id", 400);
          }
        let payload:JWTpayload = requireJWTPayload(req)
        let user_id = payload.id
        let deletedItemId = await this.mapLabelService.deleteMapLabelById(id,user_id)
        return {deletedItemId}
    }


    updateLikeOrDisLike = async(req:express.Request)=>{
        let id = +req.params.id;
        if (!id) {
            throw new HttpError("expect numeric req.params.id", 400);
          }
        let mapLabel:MapLabelDTO = req.body
        if(!mapLabel){
            throw new HttpError("missing map label in in request body", 400)
        }
         let payload:JWTpayload = requireJWTPayload(req)
         let action_user_id = payload.id
        //   let user_id = payload.id
          delete mapLabel['username']
          delete mapLabel['nickname']
          delete mapLabel['gender']
          delete mapLabel['icon_photo']
          if(!mapLabel.id)return 
        let currentMapLabel:MapLabelDTO = await this.mapLabelService.getMapLabelById(mapLabel.id)
        let like_members = currentMapLabel.like_members
        let dislike_members = currentMapLabel.dislike_members
        if(!like_members) like_members = []
        if(!dislike_members) dislike_members = []
        if(mapLabel.like_members?.includes(action_user_id)){
            like_members?.push(action_user_id)
        }else if(mapLabel.dislike_members?.includes(action_user_id)){
            dislike_members?.push(action_user_id)
        }else{
            let index = like_members?.indexOf(action_user_id)

                if(index>-1){
                    console.log(like_members)
                    like_members.splice(index,1)
                    console.log(like_members)

            }else{
                let index = dislike_members?.indexOf(action_user_id)

                    if(index>-1){
                        console.log(dislike_members)
                        dislike_members.splice(index,1)
                        console.log(dislike_members)
                    }

            }
            
        }
        
        if(like_members.length>1){

            like_members = [...new Set(like_members)]
        }
        if(dislike_members.length>1){

            dislike_members = [...new Set(dislike_members)]
        }

        let updatedItemId = await this.mapLabelService.updateMapLabelById({...currentMapLabel,like_members,dislike_members},id)
        return {updatedItemId}
    }

    updateMapLabel= async(req:express.Request)=>{
        let id = +req.params.id;
        if (!id) {
            throw new HttpError("expect numeric req.params.id", 400);
          }
        let mapLabel = req.body
        if(!mapLabel){
            throw new HttpError("missing map label in in request body", 400)
        }
        let payload:JWTpayload = requireJWTPayload(req)
        let user_id = payload.id
        // let map_label:MapLabelDTO = mapLabel
        delete mapLabel['username']
        delete mapLabel['nickname']
        delete mapLabel['gender']
        delete mapLabel['icon_photo']
        // console.log({mapLabel})
        let updatedItemId = await this.mapLabelService.updateMapLabelById(mapLabel,id,user_id)
        return {updatedItemId}
    }
}

