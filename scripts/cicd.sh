set -e

##Variables
root="$PWD"
server="trip-tail-server"
server_root="~/trip-tail-server"
image_name="docker-trip-tail-server"
image_tag="production"
image="$image_name:$image_tag"



cd "$root/trip-tail-server"
npm i 
# npm test
docker build . -t "$image"

cd "$root"
docker images | grep "$image_name" | grep "$image_tag"
docker save "$image" | zstd -c  - | ssh "$server" "unzstd -d - | docker load"
scp docker-compose.yml "$server:$server_root/"
ssh "$server" "cd $server_root && docker-compose up -d"