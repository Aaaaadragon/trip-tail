import { deleteWithToken, getWithToken, postWithToken, putWithToken } from "../../api";
import { RootDispatch } from "../action";
import { setGetMapLabelsStatusAction } from "../map/action";
import { MapLabel } from "../map/state";
import { RootState } from "../state";
import { setProfileMapLabelsAction, setProfileMapLabelStatusAction, setTrailRecordsWithPostsActions, setTrailRecordStatusAction, setUserProfileAction, setUserProfileStatusAction } from "./action";

export function getAllTrailRecordThunk() {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    let token = getState().auth.user?.token;
    let json = await getWithToken(token, "/trail_record/getAll");
    if (json.error) {
      dispatch(
        setTrailRecordStatusAction({ type: "fail", message: json.error })
      );
    } else {
      let trailRecords = json.trailRecords
      let allTrailRecordPosts = json.allTrailRecordPosts
      dispatch(setTrailRecordsWithPostsActions(trailRecords,allTrailRecordPosts))
      dispatch(setTrailRecordStatusAction({ type: "success" }));
    }
  };
}

export function getAllMapLabelByUserIdThunk() {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    let token = getState().auth.user?.token;
    let json = await getWithToken(token,"/map_label/getAllWithUserId");
    if (json.error) {
      dispatch(
        setProfileMapLabelStatusAction({ type: "fail", message: json.error })
      );

    } else{
      dispatch(setProfileMapLabelsAction(json))
      dispatch(setProfileMapLabelStatusAction({ type: "success" }));
    }

  };
}

export function getUserProfileThunk(){
  return async(dispatch:RootDispatch, getState:()=>RootState)=>{
    let token = getState().auth.user?.token
    let json = await getWithToken(token, '/user_profile/get')
    if(json.error){
      dispatch(setUserProfileStatusAction({type:"fail",message:json.error}))
    }else{
      dispatch(setUserProfileAction(json))
      dispatch(setUserProfileStatusAction({type:'success'}))
    }
  }
}

export function updateUserProfileThunk(userProfile:FormData){
  return async(dispatch:RootDispatch, getState:()=>RootState)=>{
    let token = getState().auth.user?.token
    let withPhoto = true
    let json = await postWithToken(token, '/user_profile/update', userProfile,withPhoto)
    if(json.error){
      dispatch(setUserProfileStatusAction({type:"fail",message:json.error}))
    }else{
      dispatch(setUserProfileAction(json.newItem))
      dispatch(setUserProfileStatusAction({type:'success'}))
      // setTimeout(()=>{
      //   dispatch(setTrailRecordStatusAction({type:'idle'}))
      // },1000)
    }
  }
}

export function updateTrailNameThunk(id:number,trail_name:string){
  return async (dispatch:RootDispatch, getState:()=>RootState)=>{
    let token = getState().auth.user?.token
    let json = await putWithToken(token, `/trail_record/id/${id}/updateName`, {trail_name})
    if(json.error){
      console.log('update trail name failed, error:',json.error)
    }else{
      setTimeout(()=>{
        dispatch(setTrailRecordStatusAction({type:'idle'}))
      },1000)
    }
  }
}
export function updateMapLabelThunk(id:number,mapLabel:MapLabel){
  return async (dispatch:RootDispatch, getState:()=>RootState)=>{
    let token = getState().auth.user?.token
    let json = await putWithToken(token, `/map_label/id/${id}/updateLabel`, mapLabel)
    if(json.error){
      console.log('update map label failed, error:',json.error)
    }else{
      setTimeout(()=>{
        dispatch(setGetMapLabelsStatusAction({type:'idle'}))
        dispatch(setProfileMapLabelStatusAction({type:'idle'}))
      },1000)
    }
  }
}

export function deleteTrailRecordThunk(id:number){
  return async (dispatch:RootDispatch, getState:()=>RootState)=>{
    let token = getState().auth.user?.token
    let json = await deleteWithToken(token,`/trail_record/id/${id}/delete`)
    if(json.error){
      console.log('delete trail record failed, error:',json.error)
    }else{
      setTimeout(()=>{
        dispatch(setTrailRecordStatusAction({type:'idle'}))
      },1000)
    }
  }
}
export function deleteMapLabelThunk(id:number){
  return async (dispatch:RootDispatch, getState:()=>RootState)=>{
    let token = getState().auth.user?.token
    let json = await deleteWithToken(token,`/map_label/id/${id}/delete`)
    if(json.error){
      console.log('delete map labe; failed, error:',json.error)
    }else{
      setTimeout(()=>{
        dispatch(setGetMapLabelsStatusAction({type:'idle'}))
        dispatch(setProfileMapLabelStatusAction({type:'idle'}))
      },1000)
    }
  }
}