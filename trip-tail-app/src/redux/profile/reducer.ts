import { ProfileAction } from "./action";
import { ProfileState } from "./state";

function initialState(): ProfileState {
  return {
    fetch: {
      statisticDataStatus: { type: "idle" },
      trailRecordStatus: { type: "idle" },
      mapLabelStatus: { type: "idle" },
      profileStatus: { type: "idle" },
    },
    trailRecords: [],
    profileMapLabels: [],
    userProfile: {},
    trailRecordPosts: [],
  };
}

export function profileReducer(
  state: ProfileState = initialState(),
  action: ProfileAction
): ProfileState {
  let fetch = state.fetch;
  switch (action.type) {
    case "@@Profile/setStatisticDataStatus":
      // fetch.statisticDataStatus= action.result
      fetch = { ...fetch, statisticDataStatus: action.result };
      return { ...state, fetch };
    case "@@Profile/setTrailRecordStatus":
      fetch = { ...fetch, trailRecordStatus: action.result };
      return { ...state, fetch };
    case "@@Profile/setProfileMapLabelStatus":
      fetch = { ...fetch, mapLabelStatus: action.result };
      return { ...state, fetch };
    case "@@Profile/setTrailRecords":
      if (action.trailRecordPosts) {
        return {
          ...state,
          trailRecords: action.trailRecords,
          trailRecordPosts: action.trailRecordPosts,
        };
      }
      return { ...state, trailRecords: action.trailRecords };
    case "@@Profile/setProfileMapLabels":
      return { ...state, profileMapLabels: action.profileMapLabels };
    case "@@Profile/setUserProfileStatus":
      fetch = { ...fetch, profileStatus: action.result };
      return { ...state, fetch };
    case "@@Profile/setUserProfile":
      return { ...state, userProfile: action.userProfile };
    default:
      return state;
  }
}
