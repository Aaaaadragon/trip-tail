export type ProfileState = {
    fetch:FetchStatus
    trailRecords:TrailRecord[]
    profileMapLabels:ProfileMapLabel[]
    userProfile:UserProfile
    trailRecordPosts:TrailRecordPosts
}


export type FetchStatus = {
    statisticDataStatus: ProfileAPIStatus
    trailRecordStatus:ProfileAPIStatus
    mapLabelStatus:ProfileAPIStatus    
    profileStatus:ProfileAPIStatus
}

export type ProfileAPIStatus =   {type : 'idle'} | {type:'success'} | {type:'fail';message:string}

export type TrailRecord = {
    id:number
    trail_name:string,
    gps_record:string, //JSON.parse(GPSRecord)
    created_at:any,
    duration:number,
    distance:number,
    user_id?:number,
    mountain_id?:number,
    is_private?:boolean,
    updated_at?:any,
  }

type trail_record_id = number



export type TrailRecordPost = {
  user_id?:number
  trail_record_id:number
  title:string
  gps_latitude:number
  gps_longitude:number
  image_filename?:string
  description:string
}

export type TrailRecordPosts = Record<number,TrailRecordPost[]>

export type ProfileMapLabel={
    label_type_id:number,
    title:string,
    image_filename:string,
    gps_latitude:number,
    gps_longitude:number,
    like_member?:number[],
    dislike_member?:number[],
    id?:number,
    user_id?:number,
    is_private?:boolean,
    created_at?:string,
  }

export type UserProfile = {
  nickname?:string,
  gender?:'default'|'male'|'female',
  district_id?:number,
  icon_photo?:string,
  hike_frequency?:number //How many times per month
  is_private?:boolean 
}

