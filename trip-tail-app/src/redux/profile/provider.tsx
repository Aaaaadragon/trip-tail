import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRole } from "../../hooks/useRole";
import { RootState } from "../state";
import {
  getAllMapLabelByUserIdThunk,
  getAllTrailRecordThunk,
  getUserProfileThunk,
} from "./thunk";

export function TrailRecordProvider(props: { children?: any,onLoaded?:()=>void }) {
  const dispatch = useDispatch();
  const role = useRole();
  const trailRecordStatusType = useSelector(
    (state: RootState) => state.profile.fetch.trailRecordStatus.type
  );
  


  useEffect(() => {
    if (
      role === "user" &&
      (trailRecordStatusType === "idle" || trailRecordStatusType === "fail")
    ) {
      dispatch(getAllTrailRecordThunk());
    }
    
  }, [dispatch, trailRecordStatusType,role]);

  
  

  return <>{props.children}</>;
}

export function ProfileMapLabelsProvider(props: { children?: any,onLoaded?:()=>void }) {
  const dispatch = useDispatch();
  const role = useRole();
  const profileMapLabelsStatusType = useSelector(
    (state: RootState) => state.profile.fetch.mapLabelStatus.type
  );
  useEffect(() => {
    if (
      (role === "user" && (profileMapLabelsStatusType === "idle" ||
      profileMapLabelsStatusType === "fail"))
    ) {
      dispatch(getAllMapLabelByUserIdThunk());
    }
    
  }, [dispatch, profileMapLabelsStatusType,role]);
  return <>{props.children}</>;
}

export function UserProfileProvider(props: { children?: any,onLoaded?:()=>void }) {
  const dispatch = useDispatch();
  const role = useRole();
  const profileStatusType = useSelector(
    (state: RootState) => state.profile.fetch.profileStatus.type
  );
  useEffect(() => {
    if (
      (role === "user" && (profileStatusType === "idle"||
      profileStatusType === "fail"))
    ) {
      dispatch(getUserProfileThunk());
    }
    
  }, [dispatch, profileStatusType,role]);
  return <>{props.children}</>;
}
