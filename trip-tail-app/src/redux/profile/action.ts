import { ProfileAPIStatus, ProfileMapLabel, TrailRecord, TrailRecordPost, UserProfile } from "./state";

export function setStatisticDataStatusAction(result:ProfileAPIStatus){
    return {type: '@@Profile/setStatisticDataStatus' as const, result}
}

export function setTrailRecordStatusAction(result:ProfileAPIStatus){
    return {type: '@@Profile/setTrailRecordStatus' as const, result}
}

export function setProfileMapLabelStatusAction(result:ProfileAPIStatus){
    return {type: '@@Profile/setProfileMapLabelStatus' as const, result}
}

export function setTrailRecordsWithPostsActions(trailRecords:TrailRecord[],trailRecordPosts?:Record<number,TrailRecordPost[]>){
return {type:'@@Profile/setTrailRecords' as const, trailRecords,trailRecordPosts}
  }

export function setProfileMapLabelsAction(profileMapLabels:ProfileMapLabel[]){
    return {type:'@@Profile/setProfileMapLabels'as const, profileMapLabels}
}

export function setUserProfileAction(userProfile:UserProfile){
    return {type:'@@Profile/setUserProfile' as const, userProfile}
}

export function setUserProfileStatusAction(result:ProfileAPIStatus){
    return {type:'@@Profile/setUserProfileStatus' as const, result}
}

export type ProfileAction = 
| ReturnType<typeof setStatisticDataStatusAction>
| ReturnType<typeof setTrailRecordStatusAction>
| ReturnType<typeof setProfileMapLabelStatusAction>
| ReturnType<typeof setTrailRecordsWithPostsActions>
| ReturnType<typeof setProfileMapLabelsAction>
| ReturnType<typeof setUserProfileAction>
| ReturnType<typeof setUserProfileStatusAction>