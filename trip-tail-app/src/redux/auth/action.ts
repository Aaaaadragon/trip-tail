import { AuthAPIResult, GetEmailCodeResult } from './state'

export function logoutAction() {
  return { type: '@@Auth/logout' as const }
}

export function setLoginResultAction(result: AuthAPIResult) {
  return { type: '@@Auth/setLoginResult' as const, result }
}
export function setRegisterResultAction(result: AuthAPIResult) {
  return { type: '@@Auth/setRegisterResult' as const, result }
}
export function setGetEmailCodeResultAction(result:GetEmailCodeResult){
  return {type:'@@Auth/setGetEmailCodeResult' as const, result}
}


export type AuthAction =
  | ReturnType<typeof setLoginResultAction>
  | ReturnType<typeof setRegisterResultAction>
  | ReturnType<typeof logoutAction>
  | ReturnType<typeof setGetEmailCodeResultAction>
