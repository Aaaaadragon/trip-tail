export type AuthState = {
    user: null | AuthUser
    registerResult: AuthAPIResult
    loginResult: AuthAPIResult
    getEmailCodeResult:GetEmailCodeResult
  }
  
  export type AuthAPIResult =
    | { type: 'idle' }
    | { type: 'success'; token: string }
    | { type: 'fail'; message: string }

   export type GetEmailCodeResult =  
   | { type: 'idle' }
   | { type: 'success'}
   | { type: 'fail'; message: string }

  export type AuthUser = {
    token: string
    payload: JWTPayload
  }
  
  export type JWTPayload = {
    id: number // user_id
    exp: number // expire_at (ms)
    username?: string
    email?: string
  }
  