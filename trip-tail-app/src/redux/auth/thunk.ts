import { post } from "../../api";
import { RootDispatch } from "../action";
import {
  setProfileMapLabelsAction,
  setProfileMapLabelStatusAction,
  setTrailRecordStatusAction,
  setTrailRecordsWithPostsActions,
  setUserProfileAction,
  setUserProfileStatusAction,
} from "../profile/action";
import { RootState } from "../state";
import {
  logoutAction,
  setGetEmailCodeResultAction,
  setLoginResultAction,
  setRegisterResultAction,
} from "./action";

export function logoutThunk() {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    localStorage.removeItem("token");
    dispatch(setGetEmailCodeResultAction({ type: "idle" }));
    dispatch(logoutAction());
    dispatch(setUserProfileAction({}));
    dispatch(setTrailRecordsWithPostsActions([]));
    dispatch(setProfileMapLabelsAction([]));
  };
}

export function loginWithPasswordThunk(user: {
  username: string;
  password: string;
}) {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    let json = await post("/user/login/password", user);
    if (json.error) {
      dispatch(setLoginResultAction({ type: "fail", message: json.error }));
    } else {
      localStorage.setItem("token", json.token);
      dispatch(
        setLoginResultAction({
          type: "success",
          token: json.token,
        })
      );
      dispatch(setUserProfileStatusAction({ type: "idle" }));
      dispatch(setProfileMapLabelStatusAction({ type: "idle" }));
      dispatch(setTrailRecordStatusAction({ type: "idle" }));
    }
  };
}

export function GetRegisterEmailCodeThunk(user: {
  username: string;
  email: string;
}) {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    let json = await post("/user/register/email", user);
    if (json.error) {
      console.log('failed get email code')
      dispatch(
        
        setGetEmailCodeResultAction({ type: "fail", message: json.error })
      );
    } else {
      console.log('success get email code')
      dispatch(setGetEmailCodeResultAction({ type: "success" }));
      setTimeout(() => {
        dispatch(setGetEmailCodeResultAction({ type: "idle" }));
      }, 30 * 60 * 1000);
    }
  };
}

export function GetEmailCodeThunk(user: { email: string }) {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    let json = await post("/user/login/email", user);
    if (json.error) {
      dispatch(
        setGetEmailCodeResultAction({ type: "fail", message: json.error })
      );
    } else {
      dispatch(setGetEmailCodeResultAction({ type: "success" }));
      setTimeout(() => {
        dispatch(setGetEmailCodeResultAction({ type: "idle" }));
      }, 30 * 60 * 1000);
    }
  };
}

export function LoginWithEmailCode(user: {
  email: string;
  email_code: number;
}) {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    let json = await post("/user/verify/emailCode", user);
    if (json.error) {
      dispatch(
        setLoginResultAction({ type: "fail", message: json.error })
      );
    } else {
      localStorage.setItem("token", json.token);
      dispatch(
        setLoginResultAction({
          type: "success",
          token: json.token,
        })
        );
        dispatch(setUserProfileStatusAction({ type: "idle" }));
        dispatch(setProfileMapLabelStatusAction({ type: "idle" }));
        dispatch(setTrailRecordStatusAction({ type: "idle" }));
    }
  };
}

export function registerThunk(user: { username: string; password: string }) {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    let json = await post("/user/register", user);
    if (json.error) {
      dispatch(setRegisterResultAction({ type: "fail", message: json.error }));
    } else {
      localStorage.setItem("token", json.token);
      dispatch(
        setRegisterResultAction({
          type: "success",
          token: json.token,
        })
      );
    }
  };
}
