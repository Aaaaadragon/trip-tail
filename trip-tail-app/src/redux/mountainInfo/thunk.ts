import { get } from "../../api";
import { RootDispatch } from "../action";
import {
  setDistrictListStatusAction,
  setDistrictListAction,
  setMountainInfosAction,
  setMountainInfoStatusAction,
} from "./action";

export function getAllMountainInfosThunk() {
  return async (dispatch: RootDispatch) => {
    let json = await get("/mountain_info/getAllInfo");
    if (json.error) {
      dispatch(
        setMountainInfoStatusAction({ type: "fail", message: json.error })
      );
    } else {
      dispatch(setMountainInfosAction(json));
      dispatch(setMountainInfoStatusAction({ type: "success" }));
    }
  };
}

export function getDistrictThunk() {
  return async (dispatch: RootDispatch) => {
    let json = await get("/mountain_info/getDistrict");
    if (json.error) {
      dispatch(
        setDistrictListStatusAction({ type: "fail", message: json.error })
      );
    } else {
      dispatch(setDistrictListAction(json));
      dispatch(setDistrictListStatusAction({ type: "success" }));
    }
  };
}
