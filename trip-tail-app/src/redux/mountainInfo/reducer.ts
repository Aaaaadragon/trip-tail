import { MountainInfoAction } from "./action";
import { MountainInfoState } from "./state";

function initialState(): MountainInfoState {
  return {
    fetch: {
      mountainInfoStatus: { type: "idle" },
      districtListStatus: { type: "idle" },
    },
    mountainInfos: [],
    districtList: [],
  };
}

export function mountainInfoReducer(
  state: MountainInfoState = initialState(),
  action: MountainInfoAction
): MountainInfoState {
  let fetch = state.fetch;
  switch (action.type) {
    case "@@MountainInfo/setMountainInfoStatus":
      fetch = { ...fetch, mountainInfoStatus: action.result };
      return {
        ...state,
        fetch,
      };
    case "@@MountainInfo/setMountainInfos":
      return {
        ...state,
        mountainInfos: action.MountainInfos,
      };
    case "@@MountainInfo/setDistrictListStatus":
      fetch = { ...fetch, districtListStatus: action.result };
      return {
        ...state,
        fetch,
      };
    case "@@MountainInfo/setDistrictList":
      return {
        ...state,
        districtList: action.DistrictList,
      };

    default:
      return state;
  }
}
