import { APIResultStatus } from "../state";
import { District, MountainInfo } from "./state";

export function setMountainInfoStatusAction(result: APIResultStatus) {
  return { type: "@@MountainInfo/setMountainInfoStatus" as const, result };
}

export function setMountainInfosAction(MountainInfos: MountainInfo[]) {
  return { type: "@@MountainInfo/setMountainInfos" as const, MountainInfos };
}

export function setDistrictListStatusAction(result: APIResultStatus) {
  return { type: "@@MountainInfo/setDistrictListStatus" as const, result };
}

export function setDistrictListAction(DistrictList: District[]) {
  return { type: "@@MountainInfo/setDistrictList" as const, DistrictList };
}

export type MountainInfoAction =
  | ReturnType<typeof setMountainInfoStatusAction>
  | ReturnType<typeof setMountainInfosAction>
  | ReturnType<typeof setDistrictListStatusAction>
  | ReturnType<typeof setDistrictListAction>;
