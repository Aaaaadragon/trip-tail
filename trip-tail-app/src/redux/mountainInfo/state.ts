import { APIResultStatus } from "../state";

export type MountainInfoState = {
  fetch: FetchStatus;
  mountainInfos: MountainInfo[];
  districtList: District[];
};

export type FetchStatus = {
  mountainInfoStatus: APIResultStatus;
  districtListStatus: APIResultStatus;
};

export type MountainInfo = {
  id: number;
  district_id: number;
  name: string;
  distance: number;
  difficulty: number;
  scenery_rating: number;
  description: string;
  image_filename?: string;
  gps_latitude?: number;
  gps_longitude?: number;
  gps_filename?: string;
  transportation?: string;
};

export type District = {
  id: number;
  name: string;
};
