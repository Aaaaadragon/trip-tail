import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../state";
import { getAllMountainInfosThunk } from "./thunk";

export function MountainInfoProvider(props: { children?: any, onLoaded?:()=>void }) {
  const dispatch = useDispatch();
  const mountainInfoStatusType = useSelector(
    (state: RootState) => state.mountainInfo.fetch.mountainInfoStatus.type
  );
  useEffect(() => {
    if (mountainInfoStatusType === "idle" || mountainInfoStatusType === "fail") {
      dispatch(getAllMountainInfosThunk());
    }
  }, [dispatch, mountainInfoStatusType]);
  return <>{props.children}</>;
}
