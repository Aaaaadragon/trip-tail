import { LatLngExpression } from "leaflet";
import { APIResultStatus } from "../state";

export type MapState = {
    fetch:{
      getMapLabelsResult:APIResultStatus
      createMapLabelResult:APIResultStatus
      updateMapLabelResult:APIResultStatus
    }
    currentPosition:LatLngExpression
    isLoading:boolean
    isRecording:boolean
    clockData:ClockData
    recordingData:RecordingData
    uploadRecordResult: APIResultStatus
    mapLabels:MapLabel[]
    tempTrailPost:TempTrailPost[]
    
  }

export type MapLabel={
  label_type_id:number,
  title:string,
  image_filename:string,
  gps_latitude:number,
  gps_longitude:number,
  like_members?:number[],
  dislike_members?:number[],
  id?:number,
  user_id?:number,
  is_private?:boolean,
  username?:string,
  nickname?:string,
  icon_photo?:string,
  gender?:string,
  created_at?:string,
}


export type ClockData = {startedAt:number,passedTime:number, timer:any}

export type RecordingData = {
  trail_name:string,
  mountain_id?:number,
  GPSData:LatLngExpression[]|undefined,
  duration:number,
  distance:number,
  gps_timer:any,
}

export type TempTrailPost = {
  title:string,
  trail_record_photo:any,
  description:string,
  gps_latitude:number,
  gps_longitude:number,
}