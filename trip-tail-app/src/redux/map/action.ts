import { LatLngExpression } from "leaflet"
import { APIResultStatus } from "../state"
import { MapLabel, TempTrailPost } from "./state"


export function setPositionAction(position:LatLngExpression) {
  return { type: '@@Map/setPosition' as const ,position}
}

export function setStartRecordingAction(title:string,mountain_id?:number){
  return {type:'@@Map/setStartRecording' as const, title,mountain_id}
}
export function setStopRecordingAction(){
  return {type:'@@Map/setStoptRecording' as const}
}

export function setStartGPSTimerAction(timer:any){
  return {type:'@@Map/setStartGPSTimer' as const,timer}
}

export function updateRecordingDataAction(gps_data:LatLngExpression, duration:number, distance:number){
  return {type: '@@Map/updateRecordingData' as const, gps_data, duration, distance}
}

export function setClockTimeAction(startedAt:number,passedTime:number){
  return {type:'@@Map/setClockTime' as const, startedAt,passedTime}
}

export function setClockTimerAction(timer:any){
  return {type:'@@Map/setClockTimer' as const,timer}
}

export function setUploadRecordResultAction(result:APIResultStatus){
   return {type:'@@Map/setUploadRecordResult' as const,result}
}

export function setMapLabelsAction(mapLabels:MapLabel[]){
  return {type:'@@Map/setMapLabels' as const, mapLabels}
}

export function setUpdateMapLabelLikeAction(mapLabel:MapLabel){
  return {type:'@@Map/setUpdateMapLabelLike' as const, mapLabel}
}

export function setGetMapLabelsStatusAction(result:APIResultStatus){
  return {type:'@@Map/setGetMapLabelsStatus' as const, result}
}

export function setCreateMapLabelsStatusAction(result:APIResultStatus){
  return {type:'@@Map/setCreateMapLabelsStatus' as const, result}
}

export function setUpdateMapLabelStatusAction(result:APIResultStatus){
  return {type:'@@Map/setUpdateMapLabelStatus' as const, result}
}

export function setTempTrailPostAction(tempTrailPost:TempTrailPost[]){
  return {type:'@@Map/setTempTrailPost' as const, tempTrailPost}
}

export type MapAction =
  | ReturnType<typeof setPositionAction>
  | ReturnType<typeof setStartRecordingAction>
  | ReturnType<typeof setStopRecordingAction>
  | ReturnType<typeof setStartGPSTimerAction>
  | ReturnType<typeof updateRecordingDataAction>
  | ReturnType<typeof setClockTimeAction>
  | ReturnType<typeof setClockTimerAction>
  | ReturnType<typeof setUploadRecordResultAction>
  | ReturnType<typeof setMapLabelsAction>
  | ReturnType<typeof setUpdateMapLabelLikeAction>
  | ReturnType<typeof setGetMapLabelsStatusAction>
  | ReturnType<typeof setCreateMapLabelsStatusAction>
  | ReturnType<typeof setUpdateMapLabelStatusAction>
  | ReturnType<typeof setTempTrailPostAction>

