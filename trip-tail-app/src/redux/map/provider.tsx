import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../state";
import { getMapLabelsThunk } from "./thunk";

export function MapLabelProvider(props: { children?: any, onLoaded?:()=>void }) {
  const dispatch = useDispatch();
  const getMapLabelsStatusType = useSelector(
    (state: RootState) => state.map.fetch.getMapLabelsResult.type
  );
  useEffect(() => {
    if (getMapLabelsStatusType === "idle" || getMapLabelsStatusType === "fail") {
      dispatch(getMapLabelsThunk());
    }
    
  }, [dispatch, getMapLabelsStatusType]);
  return <>{props.children}</>;
}
