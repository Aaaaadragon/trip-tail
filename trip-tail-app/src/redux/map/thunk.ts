
import { get, postWithToken, putWithToken } from "../../api";
import { RootDispatch } from "../action";
import {
  setProfileMapLabelStatusAction,
  setTrailRecordStatusAction,
} from "../profile/action";
import { RootState } from "../state";
import {
  setClockTimeAction,
  setClockTimerAction,
  setCreateMapLabelsStatusAction,
  setGetMapLabelsStatusAction,
  setMapLabelsAction,
  setStartGPSTimerAction,
  setStopRecordingAction,
  setTempTrailPostAction,
  setUpdateMapLabelStatusAction,
  setUploadRecordResultAction,
  updateRecordingDataAction,
} from "./action";
import { distanceInKmBetweenEarthCoordinates } from "./shareFunction";
import { MapLabel } from "./state";

export function startClockThunk() {
  return (dispatch: RootDispatch, getState: () => RootState) => {
    dispatch(setClockTimeAction(Date.now(), 0));
    let timer = setInterval(() => {
      let startedAt = getState().map.clockData.startedAt;
      let passedTime = Date.now() - startedAt;
      dispatch(setClockTimeAction(startedAt, passedTime));
    }, 500);
    dispatch(setClockTimerAction(timer));
  };
}

export function stopClockThunk() {
  return (dispatch: RootDispatch, getState: () => RootState) => {
    let timer = getState().map.clockData.timer;
    if (!timer) {
      return;
    }
    clearInterval(timer);
    dispatch(setClockTimerAction(null));
  };
}

export function startRecordGPSDataThunk() {
  return (dispatch: RootDispatch, getState: () => RootState) => {
    let timer = setInterval(() => {
      let mapState = getState().map;
      let currentPosition = mapState.currentPosition;
      let passedTime = mapState.clockData.passedTime;
      let GPSData = mapState.recordingData.GPSData || [];
      let totalGPSRecordLength = GPSData?.length || 0;
      let totalDistance = mapState.recordingData.distance;
      let distance;
      if (totalGPSRecordLength < 1) {
        distance = 0;
      } else if (totalGPSRecordLength === 1) {
        const lastPosition = GPSData![0];
        distance = distanceInKmBetweenEarthCoordinates(
          currentPosition as number[],
          lastPosition as number[]
        );
      } else {
        const lastPosition = GPSData!.slice(-1)[0];
        distance = distanceInKmBetweenEarthCoordinates(
          currentPosition as number[],
          lastPosition as number[]
        );
      }
      totalDistance += distance as number;
      // if(GPSData&&distance<0.1&&GPSData.length>2){
      //   currentPosition = GPSData?.slice(-1)[0] 
      // }
      dispatch(
        updateRecordingDataAction(currentPosition, passedTime, totalDistance)
      );
    }, 5000);
    dispatch(setStartGPSTimerAction(timer));
  };
}

export function stopRecordGPSDataThunk() {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    let timer = getState().map.recordingData.gps_timer;
    if (!timer) {
      return;
    }
    clearInterval(timer);

    let state = getState();
    let token = state.auth.user?.token;
    let recordingData = state.map.recordingData;
    let { trail_name, GPSData, duration, distance } = recordingData;
    if (
      trail_name &&
      GPSData!.length > 2 &&
      duration >= 30000 &&
      distance >= 0.1
    ) {
      let trailRecord = {
        trail_name: trail_name,
        gps_record: GPSData,
        duration: duration,
        distance: distance,
      };
      let json = await postWithToken(
        token,
        "/trail_record/create",
        trailRecord
      );
      if (json.error) {
        console.log(json.error);
        dispatch(
          setUploadRecordResultAction({ type: "fail", message: json.error })
        );
      } else {
        let trail_record_id = json.newItemId.id;
        // console.log("post gps recording record success, id", trail_record_id);
        dispatch(setUploadRecordResultAction({ type: "success" }));
        let withPhoto = true;
        let allTrailPost = getState().map.tempTrailPost;
        if (allTrailPost.length > 0) {
          allTrailPost.map(async (trailPost) => {
            let postFormData = new FormData();
            for (let [key, value] of Object.entries({
              ...trailPost,
              trail_record_id,
            })) {
              if (key === "trail_record_photo" && value !== "") {
                console.log("insert photo to FormData");
                postFormData.append(key, value, trailPost.title);
              } else {
                postFormData.append(key, value);
              }
            }
            let json = await postWithToken(
              token,
              "/trail_record_post/create",
              postFormData,
              withPhoto
            );
            if (json.error) {
              console.log(json.error);
            }
          });

          dispatch(setTrailRecordStatusAction({ type: "idle" }));
        }
      }
    }
    dispatch(setTempTrailPostAction([]));
    dispatch(setStopRecordingAction());
  };
}

// export function createTrailPostThunk(trail_record_id: number) {
//   return async (dispatch: RootDispatch, getState: () => RootState) => {

//   };
// }

export function getMapLabelsThunk() {
  return async (dispatch: RootDispatch) => {
    let json = await get("/map_label/getAll");
    if (json.error) {
      console.log(json.error);
      dispatch(
        setGetMapLabelsStatusAction({ type: "fail", message: json.error })
      );
    } else {
      dispatch(setMapLabelsAction(json));
      dispatch(setGetMapLabelsStatusAction({ type: "success" }));
    }
  };
}

export function createMapLabelThunk(mapLabel: FormData) {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    let token = getState().auth.user?.token;
    let withPhoto = true;
    let json = await postWithToken(
      token,
      "/map_label/create",
      mapLabel,
      withPhoto
    );
    if (json.error) {
      console.log(json.error);
      dispatch(
        setCreateMapLabelsStatusAction({ type: "fail", message: json.error })
      );
    } else {
      dispatch(setCreateMapLabelsStatusAction({ type: "success" }));
      setTimeout(() => {
        dispatch(setGetMapLabelsStatusAction({ type: "idle" }));
        dispatch(setProfileMapLabelStatusAction({ type: "idle" }));
        // console.log("latest status", getState().map.fetch.getMapLabelsResult);
      }, 1000);
    }
  };
}

export function updateMapLabelThunk(mapLabel: MapLabel) {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    let token = getState().auth.user?.token;
    let this_user_id = getState().auth.user?.payload.id;
    let id = mapLabel.id;
    let json = await putWithToken(
      token,
      `/map_label/id/${id}/updateLikeOrDisLike`,
      mapLabel
    );
    if (json.error) {
      console.log(json.error);
      dispatch(
        setUpdateMapLabelStatusAction({ type: "fail", message: json.error })
      );
    } else {
      dispatch(setUpdateMapLabelStatusAction({ type: "success" }));

      setTimeout(() => {
        dispatch(setGetMapLabelsStatusAction({ type: "idle" }));
        // if (mapLabel.user_id === this_user_id) {
        //   dispatch(setProfileMapLabelStatusAction({ type: "idle" }));
        // }
        // console.log("latest status", getState().map.fetch.getMapLabelsResult);
      }, 1000);
    }
  };
}
