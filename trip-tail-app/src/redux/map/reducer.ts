import { MapAction } from "./action";
import { MapState } from "./state";

function initialState(): MapState {
  return {
    fetch:{
      getMapLabelsResult:{type:"idle"},
      createMapLabelResult:{type:"idle"},
      updateMapLabelResult:{type:"idle"},
    },
    currentPosition: [22.3, 114.18],
    isLoading: true,
    isRecording: false,
    clockData: { startedAt: Date.now(), passedTime: 0, timer: null },
    recordingData: { trail_name: "", GPSData: [], duration: 0, distance: 0 ,gps_timer:null},
    uploadRecordResult:{type:'idle'},
    mapLabels:[],
    tempTrailPost:[],
  };
}

export function mapReducer(
  state: MapState = initialState(),
  action: MapAction
): MapState {
  let clockData = state.clockData;
  let recordingData = state.recordingData;
  let fetch = state.fetch
  switch (action.type) {
    case "@@Map/setPosition":
      return {
        ...state,
        currentPosition: action.position,
        isLoading: false,
      };
    case "@@Map/setStartRecording":
      recordingData = {
        ...recordingData,
        trail_name: action.title,
        mountain_id: action?.mountain_id,
      };
      return {
        ...state,
        isRecording: true,
        recordingData,
      };
    case "@@Map/setStoptRecording":
      recordingData = { trail_name: "", GPSData: [], duration: 0, distance: 0 ,gps_timer:null};
      return {
        ...state,
        isRecording: false,
        recordingData,
      };

    case '@@Map/setStartGPSTimer':
        recordingData = {...recordingData,gps_timer:action.timer}
        return {
            ...state,
            recordingData
        }

    case "@@Map/updateRecordingData":
        let GPSData = recordingData.GPSData
        GPSData?.push(action.gps_data)
      recordingData = {
        ...recordingData,
        GPSData,
        duration: action.duration,
        distance: action.distance,
      };
      return {
        ...state,
        recordingData,
      };

    case "@@Map/setClockTime":
      clockData = {
        ...clockData,
        startedAt: action.startedAt,
        passedTime: action.passedTime,
      };
      return {
        ...state,
        clockData,
      };
    case "@@Map/setClockTimer":
      clockData = { ...clockData, timer: action.timer };
      return {
        ...state,
        clockData,
      };

    case "@@Map/setUploadRecordResult":
      
      return {
        ...state,
        uploadRecordResult:action.result
      }
    case "@@Map/setMapLabels":
      return {
        ...state,mapLabels: action.mapLabels
      }

    case "@@Map/setUpdateMapLabelLike":

      let updateMapLabelId = action.mapLabel.id

      let filteredArray = state.mapLabels.filter(mapLabel=> mapLabel.id !== updateMapLabelId)

      filteredArray.push(action.mapLabel)

      let newMapLabels = filteredArray

      return{
        ...state,mapLabels:newMapLabels
      }

    case '@@Map/setGetMapLabelsStatus':
      fetch={...fetch,getMapLabelsResult:action.result}
      return{
        ...state,fetch
      }

    case '@@Map/setUpdateMapLabelStatus':
      fetch = {...fetch,updateMapLabelResult:action.result}
      return {
        ...state,fetch
      }
      case '@@Map/setTempTrailPost':
          return {
            ...state,tempTrailPost:action.tempTrailPost
          }
    default:
      return state;
  }
}
