import { Dispatch } from "react";
import { AuthAction} from "./auth/action";
import {CallHistoryMethodAction} from 'connected-react-router'
import { MapAction } from "./map/action";
import { ProfileAction } from "./profile/action";
import { MountainInfoAction } from "./mountainInfo/action";

export type RootAction = AuthAction | MapAction |CallHistoryMethodAction | ProfileAction|MountainInfoAction

export type RootDispatch = Dispatch<RootAction>
