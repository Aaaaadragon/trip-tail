import { AuthState } from "./auth/state";
import { RouterState } from "connected-react-router";
import { MapState } from "./map/state";
import { ProfileState } from "./profile/state";
import { MountainInfoState } from "./mountainInfo/state";

export type RootState = {
  auth: AuthState;
  map: MapState;
  profile: ProfileState;
  router: RouterState;
  mountainInfo: MountainInfoState;
};

export type APIResultStatus =  {type : 'idle'} | {type:'success'} | {type:'fail';message:string}