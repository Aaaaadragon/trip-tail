import { authReducer} from "./auth/reducer";
import {combineReducers} from 'redux'
import { RootState } from "./state";
import {connectRouter} from 'connected-react-router'
import { history } from "./enhancer";
import { mapReducer } from "./map/reducer";
import { profileReducer } from "./profile/reducer";
import { mountainInfoReducer } from "./mountainInfo/reducer";


export const rootReducer = combineReducers<RootState,any>({
    auth:authReducer,
    map:mapReducer,
    profile:profileReducer,
    mountainInfo:mountainInfoReducer,
    router: connectRouter(history),
})