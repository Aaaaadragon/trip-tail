import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupIonicReact,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import {
  bookSharp,
  earthSharp,
  footsteps,
  navigateCircleSharp,
  peopleSharp,
} from "ionicons/icons";
// import FriendActivityTab from './pages/FriendActivityTab';
// import MountainInfoTab from './pages/MountainInfoTab';
// import MainTab from './pages/MainTab';
// import MapTab from './pages/MapTab';
// import UserProfileTab from './pages/UserProfileTab';

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.css";
import "animate.css";
import "leaflet/dist/leaflet.css";
import "leaflet-geosearch/dist/geosearch.css";
import "./App.css"

/* Theme variables */
import "./theme/variables.css";
import { routes, Routes } from "./routes";
import { history } from "./redux/enhancer";
setupIonicReact({swipeBackEnabled:false});

const App: React.FC = () => {


  return (
    <IonApp >
      <IonReactRouter history={history} >
        <IonTabs>
          <IonRouterOutlet >
            <Routes />
          </IonRouterOutlet>
          <IonTabBar slot="bottom" id="app-tab-bar">
            <IonTabButton
              tab="friend-activity"
              href={routes.tab.friendActivity}
            >
              <IonIcon icon={peopleSharp} />
              <IonLabel>山友動態</IonLabel>
            </IonTabButton>
            <IonTabButton tab="mountain-info" href={routes.tab.mountainInfo}>
              <IonIcon icon={bookSharp} />
              <IonLabel>山典</IonLabel>
            </IonTabButton>
            <IonTabButton tab="main" href={routes.tab.main}>
              <IonIcon icon={earthSharp} />
              <IonLabel>主頁</IonLabel>
            </IonTabButton>
            <IonTabButton tab="map" href={routes.tab.map}>
              <IonIcon icon={navigateCircleSharp} />
              <IonLabel>地圖</IonLabel>
            </IonTabButton>
            <IonTabButton tab="user-profile" href={routes.tab.userProfile}>
              <IonIcon icon={footsteps} />
              <IonLabel>你的足跡</IonLabel>
            </IonTabButton>
          </IonTabBar>
        </IonTabs>
      </IonReactRouter>
    </IonApp>
  );
};
export default App;
