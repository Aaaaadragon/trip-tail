import { useSelector } from "react-redux"
import { RootState } from "../redux/state"

export function useTrailRecords(){
    return useSelector((state:RootState)=>state.profile.trailRecords)
}