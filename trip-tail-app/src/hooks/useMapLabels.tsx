import { useSelector } from "react-redux";
import { RootState } from "../redux/state";

export function useMapLabels(){
    return useSelector((state:RootState)=>state.map.mapLabels)
}