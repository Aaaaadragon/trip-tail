import { useSelector } from "react-redux";
import { RootState } from "../redux/state";

export function useProfileMapLabels(){
    return useSelector((state:RootState)=>state.profile.profileMapLabels)
}