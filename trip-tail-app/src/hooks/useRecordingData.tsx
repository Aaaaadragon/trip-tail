import { useSelector } from "react-redux";
import { RootState } from "../redux/state";

export function useRecordingData() {
    return useSelector((state: RootState) => state.map.recordingData)
  }
  