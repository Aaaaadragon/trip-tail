import { useEffect } from "react"

type AsyncFn = (any?:any) => Promise<any>

export function useAsync(asyncFn:AsyncFn, onSuccess:React.Dispatch<React.SetStateAction<any>>){
    useEffect(()=>{
        let isActive = true;
        asyncFn().then(data=>{
            if(isActive) onSuccess(data)
        })
        return ()=>{isActive=false}
    },[asyncFn,onSuccess])
}