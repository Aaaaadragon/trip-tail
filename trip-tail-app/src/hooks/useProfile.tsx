import { useSelector } from "react-redux";
import { RootState } from "../redux/state";

export function useProfile(){
    return useSelector((state:RootState)=>state.profile.userProfile)
}