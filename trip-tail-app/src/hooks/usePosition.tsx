import { useSelector } from 'react-redux'
import { RootState } from '../redux/state'

export function usePosition() {
  return useSelector((state: RootState) => state.map.currentPosition)
}
