import { useSelector } from "react-redux";
import { RootState } from "../redux/state";

export function useMountainInfo(){
    return useSelector((state:RootState)=>state.mountainInfo)
}