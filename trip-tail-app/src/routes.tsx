import { IonRouterOutlet } from "@ionic/react";
import { Redirect, Route } from "react-router";
import { useRole } from "./hooks/useRole";
import { LoginPage } from "./pages/auth_pages/LoginPage";
import { RegisterPage } from "./pages/auth_pages/RegisterPage";
import FriendActivityTab from "./pages/FriendActivityTab";
import { LandingPage } from "./pages/LandingPage";
import MainTab from "./pages/MainTab";
import MapTab from "./pages/MapTab";
import MountainInfoTab from "./pages/MountainInfoTab";
import NotMatchPage from "./pages/NotMatchPage";
import UserProfileTab from "./pages/UserProfileTab";
import { ProfileEditPage } from "./pages/user_profile_pages/ProfileEditPage";
import { SettingPage } from "./pages/user_profile_pages/SettingPage";

export let routes = {
  landingPage:'/landing',
  tab: {
    friendActivity: "/tab/home",
    mountainInfo: "/tab/mountainInfo",
    main: "/tab/main",
    map: "/tab/map",
    userProfile: "/tab/profile",
  },
  authPages: {
    login: "/login",
    register: "/register",
  },
  userProfilePages: {
    profile: "/user/profile",
    setting: "/user/setting",
    terms: "/terms",
    privacy: "/privacy",
  },
};

export const GuestRoute = (props: {
  exact?: boolean;
  path: string;
  component: React.FC;
}) => {
  const role = useRole();
  const Component = props.component;
  return (
    <Route exact={props.exact} path={props.path}>
      {role === "guest" ? <Component /> : <Redirect to={routes.tab.userProfile} />}
    </Route>
  );
};

export const UserRoute = (props: {
  exact?: boolean;
  path: string;
  component: React.FC;
}) => {
  const role = useRole();
  const Component = props.component;
  return (
    <Route exact={props.exact} path={props.path}>
      {role === "user" ? (
        <Component />
      ) : (
        <Redirect to={routes.authPages.login} />
      )}
    </Route>
  );
};

export function Routes() {
  return (
    <IonRouterOutlet >
      <Route exact path="/">
        <Redirect to={routes.landingPage} />
      </Route>
      <Route exact path={routes.landingPage} component={LandingPage} />
      <GuestRoute exact path={routes.authPages.login} component={LoginPage} />
      <GuestRoute
        exact
        path={routes.authPages.register}
        component={RegisterPage}
      />
      <Route path="/tab">
        <IonRouterOutlet>
          <UserRoute
            path={routes.tab.friendActivity}
            component={FriendActivityTab}
          />
          <Route path={routes.tab.mountainInfo} component={MountainInfoTab} />
          <Route path={routes.tab.main} component={MainTab} />
          <Route path={routes.tab.map} component={MapTab} />
          <UserRoute path={routes.tab.userProfile} component={UserProfileTab} />
        </IonRouterOutlet>
      </Route>
      <Route path="/user">
        <IonRouterOutlet>
          <UserRoute
            path={routes.userProfilePages.setting}
            component={SettingPage}
          />
          <UserRoute
            path={routes.userProfilePages.profile}
            component={ProfileEditPage}
          />
        </IonRouterOutlet>
      </Route>

      <Route component={NotMatchPage} />
    </IonRouterOutlet>
  );
}
