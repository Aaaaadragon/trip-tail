import L from 'leaflet'
import myPositionSVG from '../icons/icons8-circle-80.png'
import geoSearchPNG from '../icons/geo-search-pin.png'

export const myPositionIcon = L.icon({
    iconUrl:myPositionSVG,
    iconSize:[30,30],
    // iconAnchor:[22,64],// point of the icon which will correspond to marker's location
    // popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
})


export const searchLocationIcon = L.icon({
    iconSize: [38, 32],
    iconAnchor: [10, 41],
    popupAnchor: [10, -40],
    iconUrl: geoSearchPNG,
    // shadowUrl: "https://unpkg.com/leaflet@1.6/dist/images/marker-shadow.png"
  });
  

 export const  defaultIcon = L.icon({
    iconUrl: "https://unpkg.com/leaflet@1.0.3/dist/images/marker-icon.png",
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [30, 45],
    iconAnchor: [20, 20],
    popupAnchor: [-5, -15],
    shadowSize: [30, 30],
    shadowAnchor: [15, 15]
  });