import { faBeerMugEmpty, faCircleExclamation, faCircleQuestion, faPanorama, faPaw, faRestroom, faStore, IconDefinition } from "@fortawesome/free-solid-svg-icons";

export const labelIdToIcon: Record<number, IconWithColor> = {
    1: [faCircleExclamation,'red'],
    2: [faPanorama,'#28b814'],
    3: [faPaw,'brown'],
    4: [faStore,'#ffb303'],
    5: [faBeerMugEmpty,'#14bdfa'],
    6: [faRestroom,'#1862f5'],
    7: [faCircleQuestion,'#616161']
  };

  type IconWithColor = [IconDefinition,string]
