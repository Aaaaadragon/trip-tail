import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import L from "leaflet";
import ReactDOMServer from "react-dom/server";


export function createMarkerByFaIcon(icon:IconProp,options:Options={color:'#628A66', opacity:1, size:'0.9rem'}){

  const {color, opacity, size} = options

  const iconHTML = ReactDOMServer.renderToString(
      <div
        className="custom-marker-pin"
        style={{ backgroundColor: color,opacity }}
      >
        <FontAwesomeIcon
          className="custom-material-icons"
          style={{width:size}}
          icon={icon}
          color={color}
        />
      </div>
    );
 
    const customIcon = L.divIcon({
      className: "custom-marker-icon",
      html: iconHTML,
      iconSize: [30, 42],
      iconAnchor: [15, 42],
      popupAnchor:[0,-32],
    });

    return customIcon
}

type Options = {
  color?:string 
  opacity?:number 
  size?:string 
}

