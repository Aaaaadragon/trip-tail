import { env } from "./env"

async function handleResponse(resPromise:Promise<Response>){
  try{
    let res = await resPromise
    let json = await res.json()
    return json
  }catch(error){
    return {error:String(error)}
  }
}

export function post(url:string, body?:any){
  let resPromise = fetch(env.API_ORIGIN+url, {
    method:'POST',
    headers:{
      'Content-Type':'application/json'
    },
    body:JSON.stringify(body),
  })
  return handleResponse(resPromise)
}

export function postWithToken(
  token:string|null|undefined,
  url:string,
  body?:any,
  withPhoto?:boolean,
){
  if(!token){
    return {error:'This API is not available to guest'}
  }

  const init = withPhoto?{
    method:'POST',
    headers:{
      Authorization:'Bearer '+token,
    },body
    
  }:{
    method:'POST',
    headers:{
      'Content-Type':'application/json',
      Authorization:'Bearer '+token,
    },
    body:JSON.stringify(body),
  }

  let resPromise = fetch(env.API_ORIGIN+url, init as any)
  return handleResponse(resPromise)
}

export async function get(url:string){
  let resPromise = fetch(env.API_ORIGIN+url,
    {
      method:'GET',
    })
  return handleResponse(resPromise)
}

export async function getWithToken(
  token:string|null|undefined,
  url:string,
){
  if(!token){
    return {error:'This API is not available to guest'}
  }
  let resPromise = fetch(env.API_ORIGIN+url,
    {
      method:'GET',
      headers:{
        Authorization:'Bearer '+token,
      }
    })
  return handleResponse(resPromise)
}

export async function deleteWithToken(token:string|null|undefined, url:string, body?:any){
  if(!token){
    return {error:'This API is not available to guest'}
  }
  let resPromise = fetch(env.API_ORIGIN+url,
    {
      method:'DELETE',
      headers:{
        Authorization:'Bearer '+token,
      },
      body:JSON.stringify(body)
    })
  return handleResponse(resPromise)
}

export function putWithToken(
  token:string|null|undefined,
  url:string,
  body?:any,
  withPhoto?:boolean,
){
  if(!token){
    return {error:'This API is not available to guest'}
  }

  const init = withPhoto?{
    method:'PUT',
    headers:{
      Authorization:'Bearer '+token,
    },body
    
  }:{
    method:'PUT',
    headers:{
      'Content-Type':'application/json',
      Authorization:'Bearer '+token,
    },
    body:JSON.stringify(body),
  }

  let resPromise = fetch(env.API_ORIGIN+url, init as any)
  return handleResponse(resPromise)
}