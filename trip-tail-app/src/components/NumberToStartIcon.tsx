import { IonIcon } from "@ionic/react";
import { star, starHalf } from "ionicons/icons";

export function NumberToStar(
  num: number,
  size: string = "0.7rem",
  color: string = "#709E84"
) {
  let numInList = num.toString().split(".");
  if (numInList.length > 1) {
    //With 0.5 star
    let starHTML: any = [];
    let numOfFullStar = +numInList[0];
    Array.from({ length: numOfFullStar }, (x, i) => {
      starHTML.push(<IonIcon key={i} icon={star} style={{ color, fontSize: size }} />);
      return null
    });
    // add only one half start icon at last
    starHTML.push(
      <IonIcon  key={0.5} icon={starHalf} style={{ color, fontSize: size }} />
    );
    return [...starHTML];
  } else {
    // all full star
    let starHTML: any = [];
    let numOfFullStar = +numInList[0];
    Array.from({ length: numOfFullStar }, (x, i) => {
      starHTML.push(<IonIcon key={i}icon={star} style={{ color, fontSize: size }} />);
      return null
    });
    return [...starHTML];
  }
}
