import { IonButton } from "@ionic/react";
import { useState } from "react";

export const TrailsCardDetail = (props: { detail: string }) => {
  const detail = props.detail;
  const [isDescriptionDetail, setIsDescriptionDetail] = useState(false);

  const JoinDetail = detail.trim().split("。").join("。\n");

  return (
    <>
      {isDescriptionDetail ? (
        <>
          <div
            className="description ion-margin-top"
            style={descriptionDetailCSS}
          >
            {JoinDetail}
          </div>
          <IonButton
            className="detailBtn"
            expand="block"
            size="small"
            onClick={() => setIsDescriptionDetail(!isDescriptionDetail)}
          >
            縮小
          </IonButton>
        </>
      ) : (
        <>
          <div className="description ion-margin-top" style={descriptionCSS}>
            {JoinDetail}
          </div>
          <IonButton
            className="detailBtn"
            expand="block"
            size="small"
            onClick={() => setIsDescriptionDetail(!isDescriptionDetail)}
          >
            更多
          </IonButton>
        </>
      )}
    </>
  );
};

const descriptionCSS: React.CSSProperties = {
  maxHeight: "7.5rem",
  overflow: "hidden",
  display: "-webkit-box",
};

const descriptionDetailCSS: React.CSSProperties = {
  height: "auto",
  overflow: "unset",
  display: "block",
};
