import { IonButton, IonLabel } from "@ionic/react";
import { useState } from "react";
import { MountainInfo } from "../../redux/mountainInfo/state";
import ModalCard from "../ModalCard";
import { NumberToStar } from "../NumberToStartIcon";

export const TrailsCard = (props: { trailsInfo: MountainInfo }) => {
  const { name, distance, difficulty, scenery_rating } = props.trailsInfo;

  const [isOpenModal, setIsOpenModal] = useState(false);
  // function openModal(status: boolean) {
  //   setIsOpenModal(status);
  // }

  return (
    <>
      <IonButton
        color="primary"
        expand="block"
        className="trail-btn"
        onClick={() => setIsOpenModal(!isOpenModal)}
      >
        <div className="trailName col-3 trail-col">{name}</div>
        <div className="star diffStar col-3 trail-col">
          {NumberToStar(difficulty, "0.9rem", "#FFF278")}
        </div>
        <div className="star sceStar col-3 trail-col">
          {NumberToStar(scenery_rating, "0.9rem", "#FFF278")}
        </div>
        <div className="distance col-3 trail-col">{distance}KM</div>
      </IonButton>
      <IonLabel>
        <ModalCard
          isOpen={isOpenModal}
          setIsOpenModal={() => setIsOpenModal(false)}
          trailsInfo={props.trailsInfo}
        />
      </IonLabel>
    </>
  );
};
