import { IonCard, IonButton, IonImg } from "@ionic/react";
import { useState } from "react";
import { MountainInfoProvider } from "../../redux/mountainInfo/provider";
import { MountainInfo } from "../../redux/mountainInfo/state";
import { TrailsCard } from "./TrailsCard";

export default function DistrictCard(props: {
  trailDistrict: (num: number) => MountainInfo[];
  districtID: number;
  districtName: string;
  districtImg: string;
}) {
  const [isDistrictDetail, setIsDistrictDetail] = useState(false);

  return (
    <IonCard className="district-card" color="tertiary">
      <MountainInfoProvider />

      <IonButton
        className="district-btn"
        expand="full"
        onClick={() => setIsDistrictDetail(!isDistrictDetail)}
      >
        <div className="col-3">{props.districtName}</div>
        <IonCard className="district-photo-card ion-padding">
          <IonCard>
            <IonImg
              src={props.districtImg}
              alt="District Name"
              className="district-photo"
            ></IonImg>
          </IonCard>
        </IonCard>
      </IonButton>
      {isDistrictDetail ? (
        <>
          <div className="row trail-row">
            <div className="col-3 trail-col">路徑</div>
            <div className="col-3 trail-col">難度</div>
            <div className="col-3 trail-col">景觀</div>
            <div className="col-3 trail-col">長度</div>
          </div>
          {props.trailDistrict(props.districtID).map((trail, index) => {
            return (
              <TrailsCard key={trail.district_id + index} trailsInfo={trail} />
            );
          })}
        </>
      ) : null}
    </IonCard>
  );
}
