import { IonSearchbar, IonToolbar } from "@ionic/react";
import React, { useEffect, useState } from "react";


export const SearchBar: React.FC = () => {
  const [searchText, setSearchText] = useState("");

  useEffect(() => {}, [searchText]);

  return (
    <IonToolbar>
      <IonSearchbar
        value={searchText}
        onIonChange={(e) => setSearchText(e.detail.value!)}
        debounce={1000}
        animated
        className="search-bar"
      ></IonSearchbar>
    </IonToolbar>
  );
};
