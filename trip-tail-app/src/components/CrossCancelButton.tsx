import { IonButton, IonIcon } from "@ionic/react";
import { closeCircleOutline } from "ionicons/icons";

export const CrossCancelButton = (props:{routerLink?:string,color?:string, onTouchEnd:()=>void ,onClick?:()=>void}) => {
    const {routerLink,color,onTouchEnd, onClick} = props

    const toParent = () =>{
      console.log('click')

      onTouchEnd()
    }

  return (
    <IonButton
    style={{width:'8rem',height:'8rem',zIndex:2000}}
    onClick={onClick}
    onTouchEnd={()=>toParent()}
      routerLink={routerLink}
      fill="clear"
    //   size="large"
      color={color?color:"dark"}
    >
      <IonIcon  style={{width:'8rem',height:'8rem'}} icon={closeCircleOutline}></IonIcon>
    </IonButton>
  );
};
