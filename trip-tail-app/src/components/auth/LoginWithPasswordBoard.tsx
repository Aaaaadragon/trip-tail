import {
  IonButton,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonLoading,
  useIonAlert,
} from "@ionic/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setLoginResultAction } from "../../redux/auth/action";
import { loginWithPasswordThunk } from "../../redux/auth/thunk";


import { RootState } from "../../redux/state";
import { APIResult } from "./APIResult";
type FormState = {
  username: string;
  password: string;
};

export const LoginWithPasswordBoard = () => {
  const dispatch = useDispatch();
  const [presentAlert] = useIonAlert();
  const [formState, setFormState] = useState<FormState>({
    username: "",
    password: "",
  });

  useEffect(()=>{
    dispatch(setLoginResultAction({type:'idle'}))
  },[])

  const loginResult = useSelector(
    (state: RootState) => state.auth.loginResult
  );

  const submit = async () => {
    if (formState.password === "" || formState.username === "") {
      presentAlert("請輸入帳號及密碼", [{ text: "取消", role: "cancel" }]);
      return;
    } else if (formState.password.length < 3 || formState.password.length < 8) {
      presentAlert("帳號或密碼錯誤", [{ text: "取消", role: "cancel" }]);
      return;
    }
    dispatch(loginWithPasswordThunk(formState));
  };

  return (
    <div className="w-100">
      
      <IonList>
        <IonItem>
          <IonLabel
            color={"primary"}
            position="floating"
          >
            帳號
          </IonLabel>
          <IonInput
            type="text"
            value={formState.username}
            onIonChange={(e) => {
              setFormState({
                ...formState,
                username: e.detail.value || "",
              });
            }}
          ></IonInput>
        </IonItem>

        <IonItem>
          <IonLabel
            color={"primary"}
            position="floating"
          >
            密碼
          </IonLabel>
          <IonInput
            type="password"
            value={formState.password}
            onIonChange={(e) => {
              setFormState({
                ...formState,
                password: e.detail.value || "",
              });
            }}
          ></IonInput>
        </IonItem>
        {/* <p
          className="w-100 text-center"
          style={{
            height: "5px",
            marginTop: "1em",
            color: loginResult === "fail" ? "red" : "white",
          }}
        >
          登入失敗，帳號或密碼錯誤。
        </p> */}
        <div style={{ height: "3vh" }}  className="w-100 text-center">
            <APIResult result={loginResult} />
          </div>
      </IonList>
      <div className="w-100 text-center mt-3">
        <IonButton onClick={submit} className="w-50">
          {loginResult.type === "idle" || loginResult.type === "fail" ? (
            "登入"
          ) : (
            <IonLoading isOpen={true} />
          )}
        </IonButton>
      </div>
    </div>
  );
};
