import {
  IonButton,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonSpinner,
  IonText,
  useIonAlert,
} from "@ionic/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRole } from "../../hooks/useRole";
import {
  setGetEmailCodeResultAction,
  setLoginResultAction,
} from "../../redux/auth/action";
import { GetEmailCodeThunk, LoginWithEmailCode } from "../../redux/auth/thunk";
import { RootState } from "../../redux/state";
import { APIResult } from "./APIResult";

type FormState = {
  email: string;
  emailCode: string;
};

function ValidateEmail(email: string) {
  if (/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return true;
  }
  return false;
}

export const LoginWithEmailCodeBoard = () => {
  const dispatch = useDispatch();
  const [presentAlert] = useIonAlert();
  const [isSendCodeLoading, setIsSendCodeLoading] = useState(false);
  const [timeWhenCodeIsSend, setTimeWhenCodeIsSend] = useState(0);
  const [passedTimeAfterSent, setPassedTimeAfterSent] = useState(0);
  const [isCodeSent, setIsCodeSent] = useState(false);
  // const result = useSelector((state: RootState) => state.auth.registerResult)

  const role = useRole();
  useEffect(() => {
    dispatch(setLoginResultAction({ type: "idle" }));
    dispatch(setGetEmailCodeResultAction({ type: "idle" }));
  }, []);

  const [formState, setFormState] = useState<FormState>({
    email: "",
    emailCode: "",
  });
  //TODO register input validation checking to be improved...(e.g. only allow 0-9,a-z,~.\/=-)

  const getEmailCodeResult = useSelector(
    (state: RootState) => state.auth.getEmailCodeResult
  );
  const loginResult = useSelector((state: RootState) => state.auth.loginResult);
  useEffect(() => {
    if (role === "guest") {
      setIsSendCodeLoading(false);
      setIsCodeSent(false);
      setTimeWhenCodeIsSend(0);
      setPassedTimeAfterSent(0);
      setFormState({
        email: "",
        emailCode: "",
      });
    }
  }, [role]);

  useEffect(() => {
    if (isCodeSent && timeWhenCodeIsSend !== 0) {
      let timer = setInterval(() => {
        setPassedTimeAfterSent(Date.now() - timeWhenCodeIsSend);
      }, 500);

      return () => {
        clearInterval(timer);
      };
    }
  }, [isCodeSent, timeWhenCodeIsSend]);

  useEffect(() => {
    if (getEmailCodeResult.type === "success") {
      setIsCodeSent(true);
      setTimeWhenCodeIsSend(Date.now());
      setIsSendCodeLoading(false);
    }

    if (loginResult.type === "success") {
      setIsCodeSent(false);
    }
  }, [dispatch, getEmailCodeResult, loginResult]);

  const messages = {
    email: !ValidateEmail(formState.email) ? "請輸入完整有效的電郵地址" : "",
    emailCode:
      isCodeSent && formState.emailCode.length !== 6
        ? "請輸入六位數驗證碼"
        : "",
  };

  function getHintField() {
    for (let [key, value] of Object.entries(formState)) {
      const field = key as keyof FormState;
      const validateMessage = messages[field];
      if (!value || validateMessage) {
        return field;
      }
    }
  }
  const hintField = getHintField();

  const getEmailCode = () => {
    const invalidMessage = Object.values(messages).find(
      (message) => message.length > 0
    );
    if (invalidMessage) {
      presentAlert(invalidMessage, [{ text: "Dismiss", role: "cancel" }]);
      return;
    }
    setIsSendCodeLoading(true);
    dispatch(
      GetEmailCodeThunk({
        email: formState.email,
      })
    );
  };

  const submitCode = () => {
    const invalidMessage = Object.values(messages).find(
      (message) => message.length > 0
    );
    if (invalidMessage) {
      presentAlert(invalidMessage, [{ text: "Dismiss", role: "cancel" }]);
      return;
    }

    dispatch(
      LoginWithEmailCode({
        email: formState.email,
        email_code: +formState.emailCode,
      })
    );
  };

  const reSendEmailCode = () => {
    if (passedTimeAfterSent <= 60 * 1000) return;

    // const invalidMessage = Object.values(messages).find(
    //   (message) => message.length > 0
    // );
    setTimeWhenCodeIsSend(0);
    setPassedTimeAfterSent(0);
    // if (invalidMessage) {
    //   presentAlert(invalidMessage, [{ text: "Dismiss", role: "cancel" }]);
    //   return;
    // }
    setIsSendCodeLoading(true);
    dispatch(
      GetEmailCodeThunk({
        email: formState.email,
      })
    );
  };

  const renderField = (props: {
    field: keyof FormState;
    title: string;
    inputType: "email" | "number";
  }) => {
    const { field, inputType, title } = props;
    const validateMessage = messages[field];
    const value = formState[field];

    return (
      <>
        <IonItem>
          <IonLabel
            color={
              !value
                ? hintField === field
                  ? "primary"
                  : undefined
                : validateMessage
                ? "danger"
                : undefined
            }
            position="floating"
          >
            {title}
          </IonLabel>
          <IonInput
            disabled={inputType === "email" && isCodeSent}
            type={inputType}
            value={value}
            onIonChange={(e) =>
              setFormState({ ...formState, [field]: e.detail.value || "" })
            }
          ></IonInput>
        </IonItem>
        <IonText
          style={{ paddingLeft: "20px" }}
          color={!value ? "medium" : "danger"}
        >
          {validateMessage}
        </IonText>
      </>
    );
  };

  return (
    <div className="w-100">
      {isCodeSent ? (
        <>
          <IonList>
            {renderField({
              field: "email",
              title: "電郵",
              inputType: "email",
            })}
            {renderField({
              field: "emailCode",
              title: "驗證碼",
              inputType: "number",
            })}
          </IonList>
          <div style={{ height: "3vh" }} className="w-100 text-center">
              <APIResult result={loginResult} />
            </div>
          <div className="w-100 text-center mt-3">
            <IonButton onClick={submitCode} className="w-25">
              確認
            </IonButton>
            <IonButton onClick={reSendEmailCode} className="w-50">
              {passedTimeAfterSent !== 0 && passedTimeAfterSent < 60 * 1000
                ? `${60 - +(passedTimeAfterSent / 1000).toFixed(0)}`
                : "重新發送驗證碼"}
            </IonButton>
          </div>
          <div>
            <IonLabel color="primary" style={{ fontSize: "0.8rem" }}>
              驗證碼已經成功發送，請在30分鐘內完成登入，逾時失效。
            </IonLabel>
          </div>
          
        </>
      ) : (
        <>
          <IonList>
            {renderField({
              field: "email",
              title: "電郵",
              inputType: "email",
            })}
          </IonList>

          <div style={{ height: "3vh" }} className="w-100 text-center">
            <APIResult result={getEmailCodeResult} />
          </div>
          <div className="w-100 text-center mt-3">
            <IonButton onClick={getEmailCode} className="w-50">
              {isSendCodeLoading && getEmailCodeResult.type === "idle" ? (
                <IonSpinner />
              ) : (
                "發送驗證碼"
              )}
            </IonButton>
          </div>
        </>
      )}
    </div>
  );
};
