import { IonButton, IonInput, IonItem, IonLabel, IonList, IonText, useIonAlert } from "@ionic/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { setRegisterResultAction } from "../../redux/auth/action";
import { registerThunk } from "../../redux/auth/thunk";
import { RootState } from "../../redux/state";
import { routes } from "../../routes";
import { APIResult } from "./APIResult";

type FormState = {
    username: string;
    password: string;
    confirm_password: string;
  };

export const SignUpWithPasswordBoard=()=>{
    const dispatch = useDispatch();
    const [presentAlert] = useIonAlert();
    useEffect(()=>{
      dispatch(setRegisterResultAction({type:'idle'}))
    },[])
    // const result = useSelector((state: RootState) => state.auth.registerResult)
    const [formState, setFormState] = useState<FormState>({
      username: "",
      password: "",
      confirm_password: "",
    });
    //TODO register input validation checking to be improved...(e.g. only allow 0-9,a-z,~.\/=-)
    const messages = {
      username: formState.username.length < 3 ? "用戶名稱長度至少三個字元" : "",
      password: formState.password.length < 8 ? "密碼長度至少八個字元" : "",
      confirm_password:
        formState.confirm_password !== formState.password
          ? "前後密碼輸入不一致"
          : "",
    };
    function getHintField() {
      for (let [key, value] of Object.entries(formState)) {
        const field = key as keyof FormState;
        const validateMessage = messages[field];
        if (!value || validateMessage) {
          return field;
        }
      }
    }

    const result = useSelector((state: RootState) => state.auth.registerResult)
    const hintField = getHintField();
  
    const submit = () => {
      const invalidMessage = Object.values(messages).find(
        (message) => message.length > 0
      );
      if (invalidMessage) {
        presentAlert(invalidMessage, [{ text: "Dismiss", role: "cancel" }]);
        return;
      }
      dispatch(registerThunk(formState));
    };
  
    const renderField = (props: {
      field: keyof FormState;
      title: string;
      inputType: "text" | "password";
    }) => {
      const { field, inputType, title } = props;
      const validateMessage = messages[field];
      const value = formState[field];
  
      return (
        <>
          <IonItem>
            <IonLabel
              color={
                !value
                  ? hintField === field
                    ? "primary"
                    : undefined
                  : validateMessage
                  ? "danger"
                  : undefined
              }
              position="floating"
            >
              {title}
            </IonLabel>
            <IonInput
              type={inputType}
              value={value}
              onIonChange={(e) =>
                setFormState({ ...formState, [field]: e.detail.value || "" })
              }
            ></IonInput>
          </IonItem>
          <IonText
            style={{ paddingLeft: "20px" }}
            color={!value ? "medium" : "danger"}
          >
            {validateMessage}
          </IonText>
        </>
      );
    };

    return(
        <div className="w-100" >
        

        <IonList>
          {renderField({
            field: "username",
            title: "用戶名稱",
            inputType: "text",
          })}
          {renderField({
            field: "password",
            title: "密碼",
            inputType: "password",
          })}
          {renderField({
            field: "confirm_password",
            title: "確認密碼",
            inputType: "password",
          })}
        </IonList>
        <div style={{height:'3vh'}}  className="w-100 text-center">

        <APIResult result={result} />
        </div>
        <div className="w-100 text-center mt-3">
          <IonButton onClick={submit} className="w-50">
            註冊
          </IonButton>
        </div>
        <p className="mt-3 text-center">
          已有帳號？<Link to={routes.authPages.login}>登入</Link>
        </p>
        </div>
    )
}