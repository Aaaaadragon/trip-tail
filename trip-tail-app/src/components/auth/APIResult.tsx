import { IonText } from '@ionic/react'
import { APIResultStatus } from '../../redux/state'


export function APIResult(props: { result: APIResultStatus }) {
  const { result } = props
  return (
    <>
      {result.type !== 'fail' ? null : (
        <p>
          <IonText color={result.type === 'fail' ? 'danger' : 'primary'}>
            {result.message}
          </IonText>
        </p>
      )}
    </>
  )
}
