import {
  IonButton,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonSpinner,
  IonText,
  useIonAlert,
} from "@ionic/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  setGetEmailCodeResultAction,
  setRegisterResultAction,
} from "../../redux/auth/action";
import {
  GetRegisterEmailCodeThunk,
  LoginWithEmailCode,
} from "../../redux/auth/thunk";
import { RootState } from "../../redux/state";
import { routes } from "../../routes";
import { APIResult } from "./APIResult";

type FormState = {
  username: string;
  email: string;
  emailCode: string;
};

function ValidateEmail(email: string) {
  if (/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return true;
  }
  return false;
}

export const SignUpWithEmailBoard = () => {
  const dispatch = useDispatch();
  const [presentAlert] = useIonAlert();
  const [isSendCodeLoading, setIsSendCodeLoading] = useState(false);
  // const result = useSelector((state: RootState) => state.auth.registerResult)

  const [formState, setFormState] = useState<FormState>({
    username: "",
    email: "",
    emailCode: "",
  });
  const [isCodeSent, setIsCodeSent] = useState(false);
  //TODO register input validation checking to be improved...(e.g. only allow 0-9,a-z,~.\/=-)

  const getEmailCodeResult = useSelector(
    (state: RootState) => state.auth.getEmailCodeResult
  );

  useEffect(() => {
    if (getEmailCodeResult.type === "success") {
      setIsCodeSent(true);
      setIsSendCodeLoading(false);
    } else {
      setIsCodeSent(false);
    }
  }, [dispatch, getEmailCodeResult]);
  useEffect(() => {
    dispatch(setGetEmailCodeResultAction({ type: "idle" }));
  }, []);
  const messages = {
    username: formState.username.length < 3 ? "用戶名稱長度至少三個字元" : "",
    email: !ValidateEmail(formState.email) ? "請輸入完整有效的電郵地址" : "",
    emailCode:
      isCodeSent && formState.emailCode.length !== 6
        ? "請輸入六位數驗證碼"
        : "",
  };

  function getHintField() {
    for (let [key, value] of Object.entries(formState)) {
      const field = key as keyof FormState;
      const validateMessage = messages[field];
      if (!value || validateMessage) {
        return field;
      }
    }
  }
  const hintField = getHintField();

  const getEmailCode = () => {
    const invalidMessage = Object.values(messages).find(
      (message) => message.length > 0
    );
    if (invalidMessage) {
      presentAlert(invalidMessage, [{ text: "Dismiss", role: "cancel" }]);
      return;
    }
    setIsSendCodeLoading(true);
    dispatch(
      GetRegisterEmailCodeThunk({
        username: formState.username,
        email: formState.email,
      })
    );
  };
  const submitCode = () => {
    const invalidMessage = Object.values(messages).find(
      (message) => message.length > 0
    );
    if (invalidMessage) {
      presentAlert(invalidMessage, [{ text: "Dismiss", role: "cancel" }]);
      return;
    }

    dispatch(
      LoginWithEmailCode({
        email: formState.email,
        email_code: +formState.emailCode,
      })
    );
  };

  const reSignUp = () => {
    setFormState({
      username: "",
      email: "",
      emailCode: "",
    });
    setIsCodeSent(false);
    setIsSendCodeLoading(false);
    dispatch(setGetEmailCodeResultAction({ type: "idle" }));
  };

  const renderField = (props: {
    field: keyof FormState;
    title: string;
    inputType: "text" | "email" | "number";
  }) => {
    const { field, inputType, title } = props;
    const validateMessage = messages[field];
    const value = formState[field];

    return (
      <>
        <IonItem>
          <IonLabel
            color={
              !value
                ? hintField === field
                  ? "primary"
                  : undefined
                : validateMessage
                ? "danger"
                : undefined
            }
            position="floating"
          >
            {title}
          </IonLabel>
          <IonInput
            disabled={inputType === "text" && isCodeSent}
            type={inputType}
            value={value}
            onIonChange={(e) =>
              setFormState({ ...formState, [field]: e.detail.value || "" })
            }
          ></IonInput>
        </IonItem>
        <IonText
          style={{ paddingLeft: "20px" }}
          color={!value ? "medium" : "danger"}
        >
          {validateMessage}
        </IonText>
      </>
    );
  };

  return (
    <div className="w-100">
      {isCodeSent ? (
        <>
          <IonList >
            {renderField({
              field: "email",
              title: "電郵",
              inputType: "email",
            })}
            {renderField({
              field: "emailCode",
              title: "驗證碼",
              inputType: "number",
            })}
          </IonList>
          <div className="w-100 text-center mt-3">
            <div>
              <IonButton onClick={submitCode} className="w-50">
                確認
              </IonButton>
              <IonButton onClick={reSignUp}>重新註冊</IonButton>
            </div>
          </div>
          <div>
            <IonLabel color="primary" style={{ fontSize: "0.8rem" }}>
              驗證碼已經成功發送，請在30分鐘內完成註冊，逾時失效。
            </IonLabel>
          </div>
        </>
      ) : (
        <>
          <IonList>
            {renderField({
              field: "username",
              title: "用戶名稱",
              inputType: "text",
            })}
            {renderField({
              field: "email",
              title: "電郵",
              inputType: "email",
            })}
          </IonList>
          <div style={{ height: "3vh" }}  className="w-100 text-center">
            <APIResult result={getEmailCodeResult} />
          </div>
          <div className="w-100 text-center mt-3">
            <IonButton onClick={getEmailCode} className="w-50">
              {isSendCodeLoading&&getEmailCodeResult.type==='idle' ? <IonSpinner /> : "發送驗證碼"}
            </IonButton>
          </div>
        </>
      )}
      <p className="mt-3 text-center">
        已有帳號？<Link to={routes.authPages.login}>登入</Link>
      </p>
    </div>
  );
};
