import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonImg,
  IonModal,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { MountainInfo } from "../redux/mountainInfo/state";
import { TrailsCardDetail } from "./mountainInfo/TrailsCardDetail";

export default function ModalCard(props: {
  trailsInfo: MountainInfo;
  isOpen: boolean;
  setIsOpenModal: (status: boolean) => void;
}) {
  const url = "http://www.oasistrek.com/";
  const {
    name,
    image_filename,
    difficulty,
    scenery_rating,
    description,
    transportation,
  } = props.trailsInfo;

  let tran = transportation!
    .split(/[-,]+/)
    .join(" | ")
    .split("。")
    .join("、")
    .replace("起點 | ", "起點 :\n")
    .replace("| 終點 | ", "\n終點 :\n")
    .split("min | ")
    .join("min\n")
    .split("hr | ")
    .join("hr\n");

  return (
    <IonModal
      className="trail-modal"
      isOpen={props.isOpen}
      swipeToClose={true}
      onDidDismiss={() => props.setIsOpenModal(false)}
    >
      <IonHeader translucent>
        <IonToolbar>
          <IonButtons slot="end">
            <IonButton
              color="secondary"
              onClick={() => props.setIsOpenModal(false)}
            >
              返回
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent color="tertiary">
        <div className="ion-padding">
          <IonHeader>
            <IonTitle>{name}</IonTitle>
          </IonHeader>
          <IonImg
            className="ion-margin-bottom"
            hidden={false}
            src={url + image_filename}
          />
          <div className="scenery">景觀 : {scenery_rating} / 5</div>
          <div className="difficulty ion-margin-top">
            難度 : {difficulty} / 5
          </div>
          <div className="transportation ion-margin-top">{tran}</div>
          <TrailsCardDetail detail={description} />
        </div>
      </IonContent>
    </IonModal>
  );
}
