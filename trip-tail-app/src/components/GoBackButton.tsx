import { IonButton, IonIcon } from "@ionic/react";
import { arrowBackCircleOutline } from "ionicons/icons";

export const GoBackButton = (props:{routerLink?:string, onTouchEnd?:()=>void ,onClick?:()=>void}) => {
    const {routerLink,onTouchEnd, onClick} = props
  return (
    <IonButton
    onClick={onClick}
    onTouchEnd={onTouchEnd}
      routerLink={routerLink}
      fill="clear"
      size="small"
      color="dark"
    >
      <IonIcon size="large" icon={arrowBackCircleOutline}></IonIcon>
    </IonButton>
  );
};
