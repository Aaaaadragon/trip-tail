import { IonContent, IonIcon, IonPage, useIonViewDidEnter } from "@ionic/react";
import { arrowBackCircleOutline } from "ionicons/icons";
import { LatLngExpression } from "leaflet";
import React from "react";
import { Circle, MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import { usePosition } from "../../hooks/usePosition";
import { myPositionIcon } from "../../icons/leaflet-icons";
import { MapLabel } from "../../redux/map/state";
import { MapButton } from "../map/MapButton";
import { MapLabels } from "../map/MapLabels";
import "./LabelMap.css";
export const LabelMap = (props: {
  mapLabel: MapLabel;
  leaveMap: () => void;
}) => {
  useIonViewDidEnter(() => {
    window.dispatchEvent(new Event("resize"));
  });
  const { gps_latitude, gps_longitude } = props.mapLabel;
  const position: LatLngExpression = [gps_latitude, gps_longitude];


  const leaveLabelMap = (e: React.TouchEvent) => {
    e.preventDefault();
    e.stopPropagation();
    props.leaveMap();
  };
  const currentPosition = usePosition();

  return (
    <IonPage>
      <IonContent>
      <MapContainer center={position} zoom={18} scrollWheelZoom={true} tap={false}>

        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Circle center={currentPosition} radius={20} />
        <Marker position={currentPosition} icon={myPositionIcon}>
          <Popup>呢到係你個位置🇭🇰</Popup>
        </Marker>
        <MapButton purpose="SetPositionView" />
        <MapLabels mapLabel={props.mapLabel} forProfile={true}/>
        <button style={leaveButtonStyles} onTouchEnd={leaveLabelMap}>
        <IonIcon
          icon={arrowBackCircleOutline}
          style={{ width: "4rem", height: "4rem" }}
          color="dark"
        ></IonIcon>
      </button>
      </MapContainer>
      </IonContent>
    </IonPage>
  );
};

const leaveButtonStyles: React.CSSProperties = {
  position: "fixed",
  bottom: "5%",
  left: "3%",
  width: "5rem",
  height: "5rem",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: "rgba(255,255,255,0.9)",
  boxShadow: "3px 3px 20px 0.1px rgba(0,0,0,0.5)",
  borderRadius: "3rem",
  zIndex: 2000,
};
