import { faFlag, faPersonHiking } from "@fortawesome/free-solid-svg-icons";
import { IonContent, IonIcon, IonPage, useIonViewDidEnter } from "@ionic/react";
import { arrowBackCircleOutline } from "ionicons/icons";
import { LatLngExpression } from "leaflet";
import React from "react";
import {
  MapContainer,
  Polyline,
  TileLayer,
  Marker,
  Popup,
  Circle,
} from "react-leaflet";
import { usePosition } from "../../hooks/usePosition";
import { createMarkerByFaIcon } from "../../icons/createMarkerIcon";
import { myPositionIcon } from "../../icons/leaflet-icons";
import { TempTrailPost } from "../../redux/map/state";
import { TrailRecordPost } from "../../redux/profile/state";
import { MapButton } from "../map/MapButton";
import { TrailPostLabel } from "../map/TrailPostLabel";
import "./RouteMap.css";
export const RouteMap = (props: {
  gps_record: LatLngExpression[];
  trailPosts: TrailRecordPost[]
  leaveMap: () => void;
}) => {
  useIonViewDidEnter(() => {
    window.dispatchEvent(new Event("resize"));
  });
  const { gps_record } = props;

  const leaveLabelMap = (e: React.TouchEvent) => {
    e.preventDefault();
    e.stopPropagation();
    props.leaveMap();
  };

  const position = usePosition();

  return (
    <IonPage>
      <IonContent>
      <MapContainer
        center={gps_record[Math.floor(gps_record.length / 2)]}
        zoom={14}
        scrollWheelZoom={true}
        tap={false}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker
          position={gps_record[0]}
          icon={createMarkerByFaIcon(faPersonHiking, { color: "#175733" })}
        ></Marker>
        <Polyline
          pathOptions={{ color: "blue", opacity: 0.5, weight: 5 }}
          positions={gps_record}
        />
        <Marker
          position={gps_record[gps_record.length - 1]}
          icon={createMarkerByFaIcon(faFlag, { color: "red", size: "0.9rem" })}
        ></Marker>
        <Circle center={position} radius={20} />
        <Marker position={position} icon={myPositionIcon}>
          <Popup>呢到係你個位置🇭🇰</Popup>
        </Marker>
        <MapButton purpose="SetPositionView" />
        {props.trailPosts.map((trailPost,idx)=>{
          let tempTrailPost:TempTrailPost = {...trailPost,trail_record_photo:trailPost.image_filename}
          return (<TrailPostLabel key={idx} trailPost={tempTrailPost}/>)
        }
        )}

      </MapContainer>
      <button style={leaveButtonStyles} onTouchEnd={leaveLabelMap}>
        <IonIcon
          icon={arrowBackCircleOutline}
          style={{ width: "4rem", height: "4rem" }}
          color="dark"
        ></IonIcon>
      </button>
      </IonContent>
    </IonPage>
  );
};

const leaveButtonStyles: React.CSSProperties = {
  position: "fixed",
  bottom: "5%",
  left: "3%",
  width: "5rem",
  height: "5rem",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: "rgba(255,255,255,0.9)",
  boxShadow: "3px 3px 20px 0.1px rgba(0,0,0,0.5)",
  borderRadius: "3rem",
  zIndex: 2000,
};
