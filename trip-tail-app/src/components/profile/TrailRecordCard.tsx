import {
  faImage,
  faPencil,
  faPenToSquare,
  faPersonHiking,
  faRoute,
  faStopwatch,
  faTrashCan,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  IonItem,
  IonItemOption,
  IonItemOptions,
  IonItemSliding,
  IonLabel,
  IonModal,
} from "@ionic/react";
import { useState } from "react";
import SweetAlert from "react-bootstrap-sweetalert";
import { useDispatch } from "react-redux";
import { env } from "../../env";
import { TrailRecordPost } from "../../redux/profile/state";
import {
  deleteTrailRecordThunk,
  updateTrailNameThunk,
} from "../../redux/profile/thunk";
import { RouteMap } from "./RouteMap";
import "./TrailRecordCard.css";
import defaultPhoto from "../../trail-record-default-photo.png";

export type TrailRecordData = {
  title: string;
  duration: number;
  distance: number;
  date: number;
  gps_record: any;
};

export const TrailRecordCard = (props: {
  id: number;
  trailRecordData: TrailRecordData;
  trailPhotoSrc?: string;
  trailPosts: TrailRecordPost[];
}): JSX.Element => {
  const dispatch = useDispatch();
  const [openRouteMap, setOpenRouteMap] = useState(false);
  const [deleteAlert, setDeleteAlert] = useState(false);
  const [editAlert, setEditAlert] = useState(false);
  const { title, duration, distance, date, gps_record } = props.trailRecordData;
  const [editTitle, setEditTitle] = useState<string | null>(title);
  const [showRemindMessage, setShowRemindMessage] = useState(false);
  const totalSeconds = duration / 1000;
  const hour = Math.floor(totalSeconds / (60 * 60));
  const minute = Math.floor((totalSeconds - hour * (60 * 60)) / 60);
  let durationInfo =
    totalSeconds < 60 ? "不足1分鐘" : `${hour} 小時 ${minute}分鐘`;
  let previewPhoto: any;
  props.trailPosts.map((trailPost) => {
    if (
      trailPost.image_filename &&
      trailPost.image_filename.length > 5 &&
      previewPhoto === undefined
      
    ) {
      previewPhoto = env.AWS_S3_UPLOADS_ORIGIN + trailPost.image_filename;
      return null
    }
    return null
  });
  if (!previewPhoto) previewPhoto = defaultPhoto;

  const deleteRecord = () => {
    dispatch(deleteTrailRecordThunk(props.id));
    setDeleteAlert(false);
  };

  const updateRecord = () => {
    if (!editTitle || editTitle?.length <= 0 || editTitle?.length > 10) {
      setShowRemindMessage(true);
      setTimeout(() => {
        setShowRemindMessage(false);
      }, 2000);
      return;
    }

    if (editTitle === title) {
      setEditAlert(false);
      return;
    }
    dispatch(updateTrailNameThunk(props.id, editTitle));
    setEditAlert(false);
  };

  return (
    <IonItemSliding>
      <IonItem lines="full">
        <div
          style={{
            height: "13vh",
            width: "30%",
            fontSize: "10px",
          }}
        >
          <p className="m-0" style={{ fontSize: "1rem", fontWeight: "bolder" }}>
            {title}
          </p>
          <IonLabel color="primary" className="m-0">
            <FontAwesomeIcon
              icon={faStopwatch}
              style={{
                fontSize: "1rem",
                color: "#628A66",
                marginRight: "0.7rem",
              }}
            />
            {durationInfo}
          </IonLabel>
          <IonLabel color="primary" className="m-0">
            <FontAwesomeIcon
              icon={faPersonHiking}
              style={{
                fontSize: "1rem",
                color: "#628A66",
                marginRight: "0.8rem",
              }}
            />
            {distance.toFixed(1)} 公里
          </IonLabel>
          <IonLabel color="primary" className="m-0">
            <FontAwesomeIcon
              icon={faRoute}
              style={{
                fontSize: "1rem",
                color: "#628A66",
                marginRight: "0.5rem",
              }}
            />
            {new Date(date).toISOString().split("T")[0]}
          </IonLabel>
        </div>
        <div style={{width:"20%",marginRight:'0.4rem'}}>
          <div style={PostIconAndTextContainer}>
          <div style={labelIconContainerStyles}>
            <FontAwesomeIcon
              className="custom-material-icons"
              icon={faPencil}
              color={"#e8be31"}
              
            />
          </div>
          <IonLabel color="primary">{props.trailPosts.filter(post=>post.image_filename?false:true).length}</IonLabel>
          </div>
          <div style={PostIconAndTextContainer}>
          <div style={labelIconContainerStyles}>
            <FontAwesomeIcon
              className="custom-material-icons"
              icon={faImage}
              color={"#e8be31"}
            />
          </div>
          <IonLabel color="primary">{props.trailPosts.filter(post=>post.image_filename?true:false).length}</IonLabel>
          
          </div>

        </div>
        <div
          className="preview-trail-record-map-container"
          style={{
            height: "13vh",
            width: "43%",
          }}
        >
          <img
            src={previewPhoto}
            style={{ width: "100%", height: "13vh", objectFit: "cover" }}
            alt="trail record preview"
          />
          {/* <StaticMap gps_record={gps_record} /> */}
        </div>
        <button
          className="trail-record-card-view-icon"
          onClick={() => setOpenRouteMap(true)}
        >
          <FontAwesomeIcon icon={faRoute} />
        </button>
        <IonModal isOpen={openRouteMap}>
          <RouteMap
            leaveMap={() => setOpenRouteMap(false)}
            gps_record={gps_record}
            trailPosts={props.trailPosts}
          />
        </IonModal>
      </IonItem>
      <SweetAlert
        warning
        showCancel
        cancelBtnText="取消"
        confirmBtnText="確定刪除"
        confirmBtnBsStyle="danger"
        title={"刪除足跡《" + title + "》"}
        onConfirm={deleteRecord}
        onCancel={() => setDeleteAlert(false)}
        focusCancelBtn
        show={deleteAlert}
      >
        刪除後紀錄將不能恢復，確定刪除嗎?
      </SweetAlert>
      <SweetAlert
        openAnim={false}
        title="更改足跡名稱"
        showCancel
        showConfirm
        cancelBtnText="取消"
        confirmBtnText="確定"
        onConfirm={updateRecord}
        onCancel={() => {
          setEditAlert(false);
          setEditTitle(title);
        }}
        show={editAlert}
      >
        <input
          type="text"
          defaultValue={editTitle!}
          onChange={(e) => setEditTitle(e.target.value!)}
        ></input>
        <div>
          <div style={{ color: showRemindMessage ? "red" : "white" }}>
            標題不能為空或超過十個字元
          </div>
        </div>
      </SweetAlert>
      <IonItemOptions side="start">
        <IonItemOption color="danger" onClick={() => setDeleteAlert(true)}>
          <FontAwesomeIcon icon={faTrashCan} size="2x" />
        </IonItemOption>
        <IonItemOption color="secondary" onClick={() => setEditAlert(true)}>
          <FontAwesomeIcon icon={faPenToSquare} size="2x" />
        </IonItemOption>
      </IonItemOptions>
    </IonItemSliding>
  );
};
const labelIconContainerStyles = {
  height: "2rem",
  width: "2rem",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: "100%",
  border: "5px solid #e8be31",
  marginTop:'0.2rem',
  marginRight:'0.3rem'
};

const PostIconAndTextContainer = {
  display:'flex',
  justifyContent:'center',
  alignItems:'center',
}