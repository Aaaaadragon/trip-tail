import { IonBadge } from "@ionic/react";

export const StatisticLabel = (props: {
  title: string;
  data: string | number;
  unit: string;
  color?: string;
}) => {

    return (
        <div className="w-100 d-flex justify-content-around mt-1">
        <p className="m-0">{props.title}&nbsp;:</p>
        <p
          className="m-0 d-flex align-items-center"
          style={{ width: "9em" }}
        >
          <IonBadge color={props.color}>{props.data}</IonBadge>
          &nbsp;&nbsp;{props.unit}
        </p>
      </div>
    )
};
