import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCircleCheck,
  faCircleXmark,
  faMapLocation,
  faPenToSquare,
  faTrashCan,
} from "@fortawesome/free-solid-svg-icons";
import { labelIdToIcon } from "../../icons/icons";
import { MapLabel } from "../../redux/map/state";
import { env } from "../../env";
import { useState } from "react";
import {
  IonItem,
  IonItemOption,
  IonItemOptions,
  IonItemSliding,
  IonLabel,
  IonModal,
  IonToggle,
} from "@ionic/react";
import { LabelMap } from "./LabelMap";
import SweetAlert from "react-bootstrap-sweetalert";
import { deleteMapLabelThunk, updateMapLabelThunk } from "../../redux/profile/thunk";
import { useDispatch } from "react-redux";

export const TrailLabelCard = (props: { mapLabel: MapLabel }): JSX.Element => {
  const dispatch = useDispatch();
  const [openLabelMap, setOpenLabelMap] = useState(false);
  const [deleteAlert, setDeleteAlert] = useState(false);
  const [editAlert, setEditAlert] = useState(false);
  const [showRemindMessage, setShowRemindMessage] = useState(false);

  const {
    id,
    label_type_id,
    created_at,
    title,
    like_members,
    dislike_members,
    image_filename,
    is_private,
  } = props.mapLabel;

  const defaultFrom = {
    title: title,
    label_type_id: label_type_id,
    is_private: is_private,
  };

  const [updateForm, setUpdateForm] = useState(defaultFrom);
  let likeNumber = like_members?.length ? like_members.length : 0;
  let dislikeNumber = dislike_members?.length ? dislike_members.length : 0;

  const deleteRecord = () => {
    dispatch(deleteMapLabelThunk(id!));
    setDeleteAlert(false);
  };

  const updateRecord = () => {
    console.log("update");
    if(!updateForm.title || updateForm.title.length<=0 || updateForm.title.length>10 ){
      setShowRemindMessage(true)
      setTimeout(()=>{
        setShowRemindMessage(false)
      },2000)
      return
    }

    if(updateForm===defaultFrom){
      setEditAlert(false)
      return
    }

    if(!id) return
    dispatch(updateMapLabelThunk(id,{...props.mapLabel,...updateForm}))
    setEditAlert(false)
  };

  return (
    <IonItemSliding className="map-label-sliding">
      <IonItem lines="full" style={{ LineHeight: "10vh" }}>
        <div
          style={{
            height: "90%",
            width: "55%",
            fontSize: "10px",
            display: "flex",
            flexDirection: "row",
          }}
        >
          <div
            style={{
              width: "6rem",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              marginTop: "10px",
            }}
          >
            {/* label logo and date */}
            <FontAwesomeIcon
              icon={labelIdToIcon[label_type_id][0]}
              size="2x"
              color={labelIdToIcon[label_type_id][1]}
            />
            <IonLabel  color="primary" className="m-0" style={{ fontSize: "10px" }}>
              {created_at?.split("T")[0]}
            </IonLabel>
          </div>

          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              textAlign: "center",
            }}
          >
            <p
              className="m-0 "
              style={{ fontSize: "0.8rem", fontWeight: "bolder" }}
            >
              {title}
            </p>
            {/* title */}

            <div
              className="d-flex"
              style={{ fontSize: "15px", alignItems: "center" }}
            >
              {" "}
              {/* like and dislike */}
              <FontAwesomeIcon
                icon={faCircleCheck}
                color="yellowgreen"
                style={{fontSize:'1.3rem'}}
              ></FontAwesomeIcon>
              <IonLabel color="primary" style={{ marginLeft: "0.5rem", marginRight: "0.5rem",fontSize:'1.3rem'}}>
                {likeNumber}{" "}
              </IonLabel>
              <FontAwesomeIcon
                icon={faCircleXmark}
                color="red"
                style={{fontSize:'1.3rem'}}
              ></FontAwesomeIcon>
              <IonLabel color="primary" style={{ marginLeft: "0.5rem", marginRight: "0.5rem",fontSize:'1.3rem' }}>
                {dislikeNumber}{" "}
              </IonLabel>
            </div>
          </div>
        </div>

        {/* photo */}
        <div
          style={{
            height: "100%",
            width: "30%",
            backgroundImage: `url(${
              env.AWS_S3_UPLOADS_ORIGIN + image_filename
            })`,
            backgroundPosition: "center",
            backgroundSize: "cover",
          }}
        ></div>
        <button
          className="trail-record-card-view-icon"
          onClick={() => setOpenLabelMap(true)}
        >
          <FontAwesomeIcon icon={faMapLocation} />
        </button>
        <IonModal isOpen={openLabelMap}>
          <LabelMap
            leaveMap={() => setOpenLabelMap(false)}
            mapLabel={props.mapLabel}
          />
        </IonModal>
      </IonItem>
      <SweetAlert
        warning
        showCancel
        cancelBtnText="取消"
        confirmBtnText="確定刪除"
        confirmBtnBsStyle="danger"
        title={"刪除標籤《" + title + "》"}
        onConfirm={deleteRecord}
        onCancel={() => setDeleteAlert(false)}
        focusCancelBtn
        show={deleteAlert}
      >
        刪除後紀錄將不能恢復，確定刪除嗎?
      </SweetAlert>
      <SweetAlert
        openAnim={false}
        title="編輯標籤"
        showCancel
        showConfirm
        cancelBtnText="取消"
        confirmBtnText="確定"
        onConfirm={updateRecord}
        onCancel={() => {
          setEditAlert(false);
          setUpdateForm(defaultFrom);
        }}
        show={editAlert}
      >
        <input
        style={{textAlign:'center'}}
          type="text"
          defaultValue={updateForm.title}
          onChange={(e) =>
            setUpdateForm({ ...updateForm, title: e.target.value })
          }
        ></input>
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            marginTop: "1rem",
          }}
        >
          <select
            defaultValue={updateForm.label_type_id}
            style={{
              border:'0',
              height: "2rem",
              width: "25vw",
              textAlign: "center",
              backgroundColor: "white",
            }}
            onChange={(e) => {
              setUpdateForm({ ...updateForm, label_type_id: +e.target.value });
            }}
          >
            <option value="1">警告</option>
            <option value="2">景點</option>
            <option value="3">動物</option>
            <option value="4">商鋪</option>
            <option value="5">飲品供應</option>
            <option value="6">洗手間</option>
            <option value="7">其他</option>
          </select>
          <div
            style={{
              marginLeft: "1rem",
              height: "3rem",
              width: "3rem",
              border: "3px dashed",
              borderRadius: "6px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <FontAwesomeIcon
              style={{ width: "3rem" }}
              icon={labelIdToIcon[updateForm.label_type_id][0]}
              size="2x"
              color={labelIdToIcon[updateForm.label_type_id][1]}
            />
          </div>
        </div>
        <div style={{width:'100%',display:'flex',justifyContent:'center',alignItems:'center',marginTop:'1rem'}}>

            <IonLabel >公開&nbsp;&nbsp;&nbsp;</IonLabel>
            <IonToggle
              checked={!updateForm.is_private}
              value="private"
              onIonChange={(e) =>
                setUpdateForm({ ...updateForm, is_private: !e.detail.checked })
              }
            />

        </div>
        <div>
          <div style={{ color: showRemindMessage ? "red" : "white" }}>
            標題不能為空或超過十個字元
          </div>
        </div>
      </SweetAlert>
      <IonItemOptions side="start">
        <IonItemOption color="danger" onClick={() => setDeleteAlert(true)}>
          <FontAwesomeIcon icon={faTrashCan} size="1x" />
        </IonItemOption>
        <IonItemOption color="secondary" onClick={() => setEditAlert(true)}>
          <FontAwesomeIcon icon={faPenToSquare} size="1x" />
        </IonItemOption>
      </IonItemOptions>
    </IonItemSliding>
  );
};
