import { faFlag, faPersonHiking } from "@fortawesome/free-solid-svg-icons";
import { useIonViewDidEnter } from "@ionic/react";
import { LatLngExpression } from "leaflet"
import { MapContainer, Marker, Polyline, TileLayer } from "react-leaflet"
import { createMarkerByFaIcon } from "../../icons/createMarkerIcon";
import './StaticMap.css'

export const StaticMap = (props:{gps_record:LatLngExpression[]}) =>{
    useIonViewDidEnter(() => {
        window.dispatchEvent(new Event('resize'));
     });
    const {gps_record} = props
    return (

        <MapContainer tap={false} {...interactionOptions} className="static-trail-record-map" center={gps_record[Math.floor(gps_record.length/2)]} zoom={13} >
          
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={gps_record[0]} icon={createMarkerByFaIcon(faPersonHiking, {color:'#175733',opacity:0.6,size:"0.9rem"})}></Marker>
        <Polyline
          pathOptions={{ color: "blue" }}
          positions={gps_record}
        />
        <Marker position={gps_record[gps_record.length - 1]} icon={createMarkerByFaIcon(faFlag,{color:'red',opacity:0.6,size:"0.9rem"})}></Marker>
      </MapContainer>
    )
}

const interactionOptions = {
    zoomControl:false,
    doubleClickZoom:false,
    closePopupOnClick:false,
    dragging:false,
    trackResize:false,
    touchZoom:false,
    scrollWheelZoom:false,
  }

