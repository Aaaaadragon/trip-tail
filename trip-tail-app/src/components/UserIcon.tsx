import React from "react";
import { env } from "../env";
import profileDefaultIcon from "../user-default-icon.png";
export const UserIcon = (props: {
  iconSize: string;
  photo?: string;
  style?: React.CSSProperties;
  onTouchEnd?: (e:React.TouchEvent) => void;
}) => {
  let { iconSize, photo } = props;
  if(typeof photo === 'string'&& photo.length>50){
    photo = photo
  }
  else if(typeof photo === 'string' && photo.length>10){
    photo = env.AWS_S3_UPLOADS_ORIGIN + photo
  }else{
    photo = profileDefaultIcon
  }
  
  return (
    <div
      onTouchEnd={props.onTouchEnd}
      style={{
        width: iconSize,
        height: iconSize,
        backgroundColor: "#c5e8cf",
        backgroundImage: `url(${photo})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        borderRadius: "1rem",
        marginTop: "0.5rem",
        ...props.style,
      }}
    ></div>
  );
};
