import { IonSpinner } from "@ionic/react";

export const LoadingDots = (props:{size:number,isOpen:boolean,color?:string}) => {
    if(!props.isOpen)return null
    
  return (
    <div
      className="d-flex"
      style={{ height: "60vh", justifyContent: "center", alignItems: "center" }}
    >
      <IonSpinner name="dots" color={props.color} style={{ transform: `scale(${props.size})` }} />
    </div>
  );
};
