import fetch from "node-fetch";
import { useEffect, useState } from "react";
import { IonCard, IonLabel, IonSpinner } from "@ionic/react";
import "./WeatherCard.css";
import uvIcon from "../../icons/ultraviolet.png";
import uvIndexIcon from "../../icons/uv-index.png";
import tempIcon from "../../icons/thermometer.png";
import humidityIon from "../../icons/drop.png";
import { useAsync } from "../../hooks/useAsync";

//  This Component will export :
//  WeatherCard() : 溫度 濕度 紫外線指數 日出 日落

const date = new Date();
const year = date.getFullYear();
const month = date.getMonth();
const day = date.getDate();

async function getDataFromObservatory() {
  let json1 = await fetch(
    "https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=rhrread&lang=tc"
  );
  let obj1 = await json1.json();
  let temperature: number = obj1.temperature.data[1].value;
  let humidity: number = obj1.humidity.data[0].value;
  let uvIndex = obj1.uvindex;
  let lastUpdateTime = obj1.temperature.recordTime;
  let iconURL = `https://www.hko.gov.hk/images/HKOWxIconOutline/pic${obj1.icon[0]}.png`;

  let json2 = await fetch(
    `https://data.weather.gov.hk/weatherAPI/opendata/opendata.php?dataType=SRS&lang=tc&rformat=json&year=${year}}&month=${month}&day=${day}`
  );

  let obj2 = await json2.json();
  let sunriseTime: string = obj2.data[0][1];
  let sunsetTime: string = obj2.data[0][3];
  // "日出 : " + riseTime + " ; 日落 : " + setTime;

  if (uvIndex === "") {
    return {
      temperature,
      humidity,
      sunriseTime,
      sunsetTime,
      iconURL,
      lastUpdateTime,
    } as WeatherData;
  } else {
    let uvIndex: number = obj1.uvindex.data[0].value;
    let uvIntensity: string = obj1.uvindex.data[0].desc; // 高,中,低
    return {
      temperature,
      uvIndex,
      uvIntensity,
      humidity,
      sunriseTime,
      sunsetTime,
      iconURL,
      lastUpdateTime,
    } as WeatherData;
  }
  // let iconNum: number = obj.icon[0];
}

type WeatherData = {
  temperature: number;
  humidity: number;
  sunriseTime: string;
  sunsetTime: string;
  uvIndex?: number;
  uvIntensity?: string;
  iconURL: string;
  lastUpdateTime: string;
};

export const WeatherCard: React.FC = () => {
  const [weatherData, setWeatherData] = useState<WeatherData>();

  // useEffect(() => {
  //   getDataFromObservatory().then((weatherData) => setWeatherData(weatherData));
  // }, []);

  useAsync(()=>getDataFromObservatory(),setWeatherData)

  let updateTime = weatherData?.lastUpdateTime.split("T")[1].split(":");

  return (
    <IonCard className="weather-container">
      {!weatherData?.temperature ? (
        <IonSpinner name="circles" />
      ) : (
        <>
          <div className="row">
            <div className="col-3 weather-icon">
              <img src={weatherData.iconURL} alt="weather icon" />
              {/* <IonIcon icon={cloudyNightOutline} style={{fontSize:'3rem'}}/> */}
            </div>
            <div className="col-3 weather-data">
              <div>
                <img
                  className="weather-png-icon"
                  src={tempIcon}
                  alt="temperature icon"
                />
                <img
                  className="weather-png-icon"
                  src={humidityIon}
                  alt="humidity icon"
                />
              </div>
              <IonLabel>溫度: {weatherData.temperature}°C</IonLabel>
              <IonLabel>濕度: {weatherData.humidity}%</IonLabel>
            </div>
            <div className="col-3 weather-data">
              <div>
                <img
                  className="weather-png-icon"
                  src={uvIcon}
                  alt="ultra violent icon"
                />
                <img
                  className="weather-png-icon"
                  src={uvIndexIcon}
                  alt="ultra violent index icon"
                />
              </div>
              <IonLabel>
                UV指數: {weatherData?.uvIndex ? weatherData.uvIndex : "N/A"}
              </IonLabel>
              <IonLabel>
                UV強度:{" "}
                {weatherData?.uvIntensity ? weatherData.uvIntensity : "N/A"}
              </IonLabel>
            </div>
            <div className="col-3 weather-data">
              <div>
                <img
                  className="weather-png-icon"
                  src="https://www.hko.gov.hk/images/icon_sunrise.png"
                  alt="sunrise icon"
                />
                <img
                  className="weather-png-icon"
                  src="https://www.hko.gov.hk/images/icon_sunset.png"
                  alt="sunset icon"
                />
              </div>
              <IonLabel>日出: {weatherData.sunriseTime}</IonLabel>
              <IonLabel>日落: {weatherData.sunsetTime}</IonLabel>
            </div>
            <IonLabel className="last-update-time">
              最後更新: {updateTime![0] + ":" + updateTime![1]}
            </IonLabel>
          </div>
        </>
      )}
    </IonCard>
  );
};

// Weather();
