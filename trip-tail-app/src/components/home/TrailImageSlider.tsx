import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, {
  Navigation,
  Pagination,
  Thumbs,
  EffectCards,
  Scrollbar,
  Autoplay,
} from "swiper";
import "swiper/css";
import React, { useEffect, useState } from "react";
import "./TrailImageSlider.css";
// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/effect-cards";
import "swiper/css/thumbs";
import "swiper/css/scrollbar";

import { useSelector } from "react-redux";
import { RootState } from "../../redux/state";
import { useMountainInfo } from "../../hooks/useMounainInfo";
import { IonLabel } from "@ionic/react";
import { NumberToStar } from "../NumberToStartIcon";
import { MountainInfoDetailModal } from "./MountainInfoDetailModal";
import { useProfile } from "../../hooks/useProfile";
import { MountainInfo } from "../../redux/mountainInfo/state";
import { UserProfile } from "../../redux/profile/state";
import { useRole } from "../../hooks/useRole";
import { UserProfileProvider } from "../../redux/profile/provider";

SwiperCore.use([
  Navigation,
  Pagination,
  Thumbs,
  EffectCards,
  Scrollbar,
  Autoplay,
]);

export const TrailImageSlider = () => {
  const [thumbsSwiper, setThumbsSwiper] = useState<any>(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedSlider, setSelectedSlider] = useState(0);
  const [filteredMountainInfo, setFilteredMountainInfo] = useState<
    MountainInfo[]
  >([]);

  let mountainInfos = useSelector(
    (state: RootState) => state.mountainInfo.mountainInfos
  );

  const mountainInfoState = useMountainInfo();
  const mountainInfoStatusType =
    mountainInfoState.fetch.mountainInfoStatus.type;
  const UserProfileStatusType = useSelector(
    (state: RootState) => state.profile.fetch.profileStatus.type
  );
  const role = useRole();

  const userProfile = useProfile();

  mountainInfos = mountainInfos.filter((mountainInfo) =>
    !mountainInfo.image_filename ? false : true
  );

  useEffect(() => {
    // console.log({ UserProfileStatusType });
    if (mountainInfoStatusType === "success") {
      if (role === "user") {
        if (UserProfileStatusType === "success") {
          // console.log("you are user");
          // console.log({ userProfile });
          mountainInfos = filterByUserProfile(mountainInfos, userProfile);
          setFilteredMountainInfo(mountainInfos);
        } else {
          // console.log("you are user but have not update profile yet");
          mountainInfos = shuffle(mountainInfos).slice(0, 6);
          setFilteredMountainInfo(mountainInfos);
        }
      } else {
        // console.log("you are guest");
        mountainInfos = shuffle(mountainInfos).slice(0, 6);
        setFilteredMountainInfo(mountainInfos);
      }
    }
  }, [mountainInfoState, userProfile]);

  const slides: any = [];
  filteredMountainInfo.map((mountainInfo, index) => {
    if (mountainInfoStatusType === "success") {
      // let indexToRandomMountainIndex:number;
      if (index > 5) return null;
      slides.push(
        <SwiperSlide
          key={`slide-${index}`}
          tag="li"
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            setIsModalOpen(true);
          }}
        >
          <MountainInfoDetailModal
            mountainInfo={filteredMountainInfo[selectedSlider]}
            isOpen={isModalOpen}
            onLeaveModal={() => setIsModalOpen(false)}
          />
          <div className="slider-card-image-container">
            <img
              style={{
                borderRadius: "1rem",
                height: "100%",
                width: "100%",
                objectFit: "cover",
              }}
              src={`https://www.oasistrek.com/${mountainInfo.image_filename}`}
              //   style={{ listStyle: 'none' }}
              alt={`Slide ${index}`}
            />
          </div>
        </SwiperSlide>
      );
    }
    return null;
  });
  const thumbs: any = [];
  filteredMountainInfo.map((mountainInfo, index) => {
    if (index > 5) return null;
    if (mountainInfoStatusType === "success") {
      thumbs.push(
        <SwiperSlide key={`thumb-${index}`} tag="li">
          <div className="scroll-card-container">
            <IonLabel style={mountainTitleStyle}>{mountainInfo.name}</IonLabel>
            <img
              style={{
                borderRadius: "0.5rem",
                height: "60%",
                width: "100%",
                objectFit: "cover",
              }}
              src={`https://www.oasistrek.com/${mountainInfo.image_filename}`}
              alt={`Thumbnail ${index}`}
            />
            <IonLabel style={{ display: "flex", flexDirection: "column" }}>
              <div style={{ fontSize: "0.80rem", color: "#175733" }}>
                難度：
                {NumberToStar(mountainInfo.difficulty, "0.65rem", "#FFF278")}
              </div>
              <div style={{ fontSize: "0.80rem", color: "#175733" }}>
                景點：
                {NumberToStar(
                  mountainInfo.scenery_rating,
                  "0.65rem",
                  "#FFF278"
                )}
              </div>
            </IonLabel>
          </div>
        </SwiperSlide>
      );
    }
    return null;
  });

  return (
    <React.Fragment>
      <Swiper
        autoplay={{
          delay: 3000,
          // disableOnInteraction: false,
        }}
        id="main"
        thumbs={{ swiper: thumbsSwiper }}
        tag="section"
        wrapperTag="ul"
        pagination
        spaceBetween={20}
        slidesPerView={1}
        effect="cards"
        // onInit={(swiper) => console.log("Swiper initialized!", swiper)}
        // onSlideChange={(swiper) => {
        //   console.log("Slide index changed to: ", swiper.activeIndex);
        // }}
        // onReachEnd={() => console.log("Swiper end reached")}
        onClick={(swiper) => setSelectedSlider(swiper.activeIndex)}
      >
        {slides}
      </Swiper>
      <Swiper
        scrollbar={
          {
            // hide: true,
          }
        }
        id="thumbs"
        spaceBetween={5}
        slidesPerView={3}
        onSwiper={setThumbsSwiper}
      >
        {thumbs}
      </Swiper>
      <UserProfileProvider />
    </React.Fragment>
  );
};

const mountainTitleStyle: React.CSSProperties = {
  position: "fixed",
  top: "30%",
  color: "white",
  fontSize: "1rem",
  fontWeight: "bolder",
  width: "100%",
  textAlign: "center",
};

function filterByUserProfile(
  mountainInfos: MountainInfo[],
  userProfile: UserProfile
) {
  const userDistrictId = userProfile.district_id;
  const userHikeFrequency = userProfile.hike_frequency;
  // console.log(userDistrictId, userHikeFrequency);

  // console.log({ ...useProfile });
  // console.log(mountainInfos.length);
  if (userDistrictId !== 0 && userHikeFrequency !== 0) {
    // console.log({ userDistrictId, userHikeFrequency });
    if (userHikeFrequency === 4) {
      mountainInfos = mountainInfos.filter((mountainInfo) => {
        if (
          mountainInfo.district_id !== userDistrictId ||(
          mountainInfo.difficulty <= 2 ||
          mountainInfo.difficulty > 4)
        ) {
          return false;
        }
        return true;
      });
    } else if (userHikeFrequency === 5) {
      mountainInfos = mountainInfos.filter((mountainInfo) => {
        if (
          mountainInfo.district_id !== userDistrictId ||
          mountainInfo.difficulty <= 3
        ) {
          return false;
        }
        return true;
      });
    } else {
      mountainInfos = mountainInfos.filter((mountainInfo) => {
        if (
          mountainInfo.district_id !== userDistrictId ||
          mountainInfo.difficulty > 2
        ) {
          return false;
        }
        return true;
      });
    }
  } else if (userHikeFrequency !== 0) {
    // console.log({ userHikeFrequency });
    if (userHikeFrequency === 4) {
      mountainInfos = mountainInfos.filter((mountainInfo) => {
        if (mountainInfo.difficulty <= 2 || mountainInfo.difficulty > 4) {
          return false;
        }
        return true;
      });
    } else if (userHikeFrequency === 5) {
      mountainInfos = mountainInfos.filter((mountainInfo) => {
        if (mountainInfo.difficulty <= 3) {
          return false;
        }
        return true;
      });
    } else {
      mountainInfos = mountainInfos.filter((mountainInfo) => {
        if (mountainInfo.difficulty > 2) {
          return false;
        }
        return true;
      });
    }
  } else if (userDistrictId !== 0) {
    mountainInfos = mountainInfos.filter(
      (mountainInfo) => mountainInfo.district_id !== userDistrictId
    );
  }

  let randomSixMountainInfoIndexList: number[] = [];

  let lengthOfMountainInfo = mountainInfos.length;

  if (lengthOfMountainInfo > 6) {
    for (let i = 1; i <= 6; i++) {
      let randomMountainIndex = Math.ceil(Math.random() * mountainInfos.length);
      while (randomSixMountainInfoIndexList.includes(randomMountainIndex)) {
        randomMountainIndex = Math.ceil(Math.random() * mountainInfos.length);
      }
      randomSixMountainInfoIndexList.push(randomMountainIndex);
    }
  } else {
    for (let i = lengthOfMountainInfo; i <= lengthOfMountainInfo; i++) {
      let randomMountainIndex = Math.ceil(Math.random() * mountainInfos.length);
      while (randomSixMountainInfoIndexList.includes(randomMountainIndex)) {
        randomMountainIndex = Math.ceil(Math.random() * mountainInfos.length);
      }
      randomSixMountainInfoIndexList.push(randomMountainIndex);
    }
  }

  mountainInfos = mountainInfos.filter((mountainInfo, index) => {
    if (randomSixMountainInfoIndexList.includes(index)) {
      return true;
    }
  });

  // console.log({ mountainInfos });

  return mountainInfos;
}

function shuffle(array: any[]) {
  let currentIndex = array.length,
    randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
}
