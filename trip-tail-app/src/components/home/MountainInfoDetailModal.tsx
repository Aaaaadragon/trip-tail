import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonImg,
  IonModal,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { MountainInfo } from "../../redux/mountainInfo/state";
import { TrailsCardDetail } from "../mountainInfo/TrailsCardDetail";

export const MountainInfoDetailModal = (props: {
  mountainInfo: MountainInfo;
  isOpen: boolean;
  onLeaveModal: () => void;
}) => {
  const {
    name,
    image_filename,
    scenery_rating,
    difficulty,
    transportation,
    description,
  } = props.mountainInfo;
  let imagePrefix = "http://www.oasistrek.com/";

  return (
    <IonModal isOpen={props.isOpen} className="trail-modal" >
      <IonHeader>
        <div
          style={{ width: "100%", height: "2vh", backgroundColor: "#709E84" }}
        ></div>
        <IonToolbar>
          <IonButtons slot="end">
            <IonButton
              className="mountain-detail-go-back"
              onClick={(e) => {
                e.stopPropagation();
                props.onLeaveModal();
              }}
            >
              返回
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent color="tertiary">
        <div className="ion-padding">
          <IonHeader>
            <IonTitle>{name}</IonTitle>
          </IonHeader>
          <IonImg
            className="ion-margin-bottom"
            hidden={false}
            src={imagePrefix + image_filename}
          />
          <div className="scenery">景觀 : {scenery_rating} / 5</div>
          <div className="difficulty ion-margin-top">
            難度 : {difficulty} / 5
          </div>
          <div className="transportation ion-margin-top">{transportation}</div>
          <TrailsCardDetail detail={description} />
        </div>
      </IonContent>
    </IonModal>
  );
};
