import {
  IonCol,
  IonGrid,
  IonRow,
  IonSkeletonText,
} from "@ionic/react";
import fetch from "node-fetch";
import { useEffect, useState } from "react";
import { useAsync } from "../../hooks/useAsync";

type MountainView = { name: string; view: number };
type UserDistance = { username: string; totalDistance: number };

async function getRankData() {
  let mountainRankJson = await fetch(
    "https://trip-tail.live/mountainRankAnalysis"
  );
  let userRankJson = await fetch("https://trip-tail.live/fakeUserRank");
  let mountainRankInfo: any[] = await mountainRankJson.json();
  let userRankInfo: any[] = await userRankJson.json();
  let mountainRankData: MountainView[] = [];
  let userRankData: UserDistance[] = [];

  mountainRankInfo.map((mountainRank) => {
    let name: string = mountainRank.name;
    let view: number = mountainRank.view;
    mountainRankData.push({ name, view });
    return null;
  });

  mountainRankData = mountainRankData
    .sort((a, b) => b.view - a.view)
    .slice(0, 20);

  userRankInfo.map((userRank) => {
    let username = userRank.username;
    let totalDistance = userRank.total_distance;
    userRankData.push({ username, totalDistance });
    return null
  });

  userRankData = userRankData
    .sort((a, b) => b.totalDistance - a.totalDistance)
    .slice(0, 20);

  // console.log(mountainRankData[0])
  return { userRankData, mountainRankData };
}

export const TopRankBoard = () => {
  const [mountainRankData, setMountainRankData] = useState<MountainView[]>([]);
  const [userRankData, setUserRankData] = useState<UserDistance[]>([]);

  const skeletonAnimation = () => {
    let skeletonGroup: any[] = [];
    for (let i = 0; i <= 20; i++) {
      skeletonGroup.push(
        <IonRow key={"skeleton" + i} style={{marginTop:"0.5rem"}}>
          <IonCol size="4" style={allToCenterStyle}>
            <IonSkeletonText animated style={{ width: "100%" }} />
          </IonCol>
          <IonCol size="2" style={allToCenterStyle}>
            <IonSkeletonText animated style={{ width: "100%" }} />
          </IonCol>
          <IonCol size="4" style={allToCenterStyle}>
            <IonSkeletonText animated style={{ width: "100%" }} />
          </IonCol>
          <IonCol size="2" style={allToCenterStyle}>
            <IonSkeletonText animated style={{ width: "100%" }} />
          </IonCol>
        </IonRow>
      );
    }
    return skeletonGroup;
  };
  // useEffect(() => {
  //     let timer = setTimeout(() => {
  //       getRankData().then((data) => {
  //         let mountainData = data.mountainRankData;
  //         let userData = data.userRankData;
  //         setMountainRankData(mountainData);
  //         setUserRankData(userData);
  //         // clearTimeout(timer);
  //         return (()=>{
  //          clearTimeout(timer) 
  //         });
  //       });
  //     }, 2000);
  // }, []);

  useEffect(()=>{
    let isMounted = true
    let timer = setTimeout(()=>{
      getRankData().then(data=>{
        if(isMounted) {
          let mountainData = data.mountainRankData;
            let userData = data.userRankData;
            setMountainRankData(mountainData);
            setUserRankData(userData);
            clearTimeout(timer)
        }
      })
    },2000)
    return ()=>{isMounted=false}
  },[])

  return (
    <div>
      <IonGrid>
        <IonRow style={{color:"#175733"}}>
          <IonCol size="4" style={{...allToCenterStyle,background:"linear-gradient(191deg, rgba(148,209,175,1) 42%, rgba(75,138,103,1) 100%, rgba(55,119,83,1) 100%, rgba(44,108,72,1) 100%, rgba(23,87,51,1) 100%)",borderRadius:"0.4rem 0 0 0.4rem",right:"0.5rem",}}>
            <div>話題山徑⛰️</div>
          </IonCol>
          <IonCol size="2" style={{...allToCenterStyle,background:"linear-gradient(148deg, rgba(235,198,111,1) 55%, rgba(238,152,54,1) 88%)",borderRadius:"0 0.4rem 0.4rem 0",right:"0.5rem"}}>
            <div>熱度</div>
          </IonCol>
          <IonCol size="4" style={{...allToCenterStyle,background:"linear-gradient(191deg, rgba(148,209,175,1) 42%, rgba(75,138,103,1) 100%, rgba(55,119,83,1) 100%, rgba(44,108,72,1) 100%, rgba(23,87,51,1) 100%)",borderRadius:"0.4rem 0 0 0.4rem",left:"0.5rem"}}>
            <div>活躍用戶👣</div>
          </IonCol>
          <IonCol size="2" style={{...allToCenterStyle,background:"linear-gradient(148deg, rgba(235,198,111,1) 55%, rgba(238,152,54,1) 88%)",borderRadius:"0 0.4rem 0.4rem 0",left:"0.5rem"}}>
            <div>公里</div>
          </IonCol>
        </IonRow>
        {mountainRankData.length > 3 && userRankData.length > 3
          ? mountainRankData.map((data, index) => {
              return (
                <IonRow
                  key={index}
                  style={{
                    marginTop:"0.5rem",
                    color:"#175733"
                  }}
                >


                  <IonCol size="4" style={{...allToCenterStyle,background:"linear-gradient(191deg, rgba(148,209,175,1) 42%, rgba(75,138,103,1) 100%, rgba(55,119,83,1) 100%, rgba(44,108,72,1) 100%, rgba(23,87,51,1) 100%)",borderRadius:"0.4rem 0 0 0.4rem",right:"0.5rem"}}>
                    <div>{data.name}</div>
                  </IonCol>
                  <IonCol size="2" style={{...allToCenterStyle,background:"linear-gradient(148deg, rgba(235,198,111,1) 55%, rgba(238,152,54,1) 88%)",borderRadius:"0 0.4rem 0.4rem 0",right:"0.5rem"}}>
                    <div style={{fontSize: "0.7rem" }}>{data.view}</div>
                  </IonCol>
                  <IonCol size="4" style={{...allToCenterStyle,background:"linear-gradient(191deg, rgba(148,209,175,1) 42%, rgba(75,138,103,1) 100%, rgba(55,119,83,1) 100%, rgba(44,108,72,1) 100%, rgba(23,87,51,1) 100%)",borderRadius:"0.4rem 0 0 0.4rem",left:"0.5rem"}}>
                    <div>{userRankData[index].username.split(" ")[0]}</div>
                  </IonCol>
                  <IonCol size="2" style={{...allToCenterStyle,background:"linear-gradient(148deg, rgba(235,198,111,1) 55%, rgba(238,152,54,1) 88%)",borderRadius:"0 0.4rem 0.4rem 0",left:"0.5rem"}}>
                    <div>{userRankData[index].totalDistance.toFixed(1)}</div>
                  </IonCol>
                </IonRow>
              );
            })
          : skeletonAnimation()}
      </IonGrid>
    </div>
  );
};

const allToCenterStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};
