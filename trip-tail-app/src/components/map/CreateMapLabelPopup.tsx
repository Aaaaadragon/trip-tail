import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IonButton, IonInput, IonLabel, IonToggle } from "@ionic/react";
import React, { useState } from "react";
import ReactDOM from "react-dom";
import { usePosition } from "../../hooks/usePosition";
import defaultPhoto from "../../trail-record-default-photo.png";
import { labelIdToIcon } from "../../icons/icons";
import imageCompression from "browser-image-compression";
import { useDispatch } from "react-redux";
import { createMapLabelThunk } from "../../redux/map/thunk";
import { IconProp } from "@fortawesome/fontawesome-svg-core";

const imageCompressOptions = {
  maxSizeMB: 0.5,
  fileType:'.jpg'
};

export const CreateMapLabelPopup = (props: {
  open: boolean;
  onClose: () => void;
  onDecided: () => void;
}) => {
  const currentLocation: any = usePosition();
  const dispatch = useDispatch();

  const defaultFormState: FormState = {
    title: "",
    label_type_id: 0,
    map_label_photo: "",
    gps_latitude: 0,
    gps_longitude: 0,
    is_private: false,
  };

  let remindMessageList = {
    title: "標題不能為空，最多十個字元",
    map_label_photo: "請上傳照片作佐證及參考用途",
    label_type_id: "請選擇標籤類別",
  };

  const mountToNode = document.getElementById("portal")!;
  const [formState, setFormState] = useState<FormState>(defaultFormState);
  const [isInsufficient, setIsInsufficient] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [remindMessage, setRemindMessage] = useState("none");
  const [remindTextClassName, setRemindTextClassName] =
    useState("animate__animated ");

  if (!props.open) return null;
  const remindTextColor = isInsufficient ? "red" : "white";

  const toParent = (e: React.TouchEvent) => {
    e.preventDefault();
    if (formState.title.length > 10 || formState.title.length <= 0) {
      setRemindMessage(remindMessageList.title);
      triggerReminder();
      setIsInsufficient(true);
      return;
    } else if (previewImage === "") {
      setRemindMessage(remindMessageList.map_label_photo);
      triggerReminder();
      setIsInsufficient(true);
      return;
    } else if (formState.label_type_id === 0) {
      setRemindMessage(remindMessageList.label_type_id);
      triggerReminder();
      setIsInsufficient(true);
      return;
    }

    let mapLabelFormData = new FormData();
    for (let [key, value] of Object.entries({
      ...formState,
      gps_latitude: currentLocation[0],
      gps_longitude: currentLocation[1],
    })) {
      if (key === "map_label_photo") {
        mapLabelFormData.append(key, value, formState.title);
      } else {
        mapLabelFormData.append(key, value);
      }
    }

    //   for (var pair of mapLabelFormData.entries()) {
    //     console.log(pair[0]+ ', ' + pair[1]);
    // }

    dispatch(createMapLabelThunk(mapLabelFormData));
    setIsInsufficient(false);
    setFormState(defaultFormState);
    setPreviewImage("");
    setRemindMessage("none");
    props.onDecided();
    props.onClose();
  };

  const triggerReminder = () => {
    setRemindTextClassName(
      (remindTextClassName) => (remindTextClassName += "animate__headShake")
    );

    setTimeout(() => {
      setRemindTextClassName("animate__animated ");
    }, 2000);
    return;
  };

  const onFileChange = async (fileChangesEvent: any) => {
    let photoFile = fileChangesEvent.target.files[0];

    try {
      const compressedPhotoFile: Blob = await imageCompression(
        photoFile,
        imageCompressOptions
      );
  
      setFormState({ ...formState, map_label_photo: compressedPhotoFile });
      let objURL = URL.createObjectURL(compressedPhotoFile);
      setPreviewImage(objURL);
    } catch (error) {
      console.log("Image compression failed, ERROR:", error);
    }
  };

  return ReactDOM.createPortal(
    <div style={overlayStyles}>
      <div style={modalStyles}>
        <div style={{ height: "4rem" }}>
          <IonInput
            placeholder="標籤標題"
            style={inputStyles}
            type="text"
            value={formState.title}
            onIonChange={(e) => {
              setFormState({ ...formState, title: e.detail.value || "" });
            }}
          ></IonInput>
        </div>
        <IonLabel
          className="w-100 ion-text-center m-0"
          style={{ fontSize: "0.8rem", color: "grey" }}
        >
          (最多十個字元)
        </IonLabel>
        <div
          className="w-100"
          style={{
            margin: "1rem",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            height: "41vh",
          }}
        >
          <div
            style={{
              height: "28vh",
              width: "100%",
              marginTop: "0.5rem",
              marginBottom: "0.5rem",
              border: "3px dashed #175733",
              borderRadius: "6px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <img
              style={{
                height: "100%",
                width: "auto",
                padding: "0.2rem",
              }}
              className="img-fluid"
              src={previewImage === "" ? defaultPhoto : previewImage}
              alt="preview"
            />
          </div>
          <IonButton>
            <label htmlFor="files">上傳相片</label>
          </IonButton>
          <input
            id="files"
            style={{ visibility: "hidden", width: "0" }}
            type="file"
            onChange={(e) => onFileChange(e)}
            accept="image/*"
          ></input>
        </div>
        <div
          style={{
            height:'1vh',
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <select
            defaultValue="0"
            style={{
              height: "2rem",
              width: "35vw",
              textAlign: "center",
              backgroundColor: "white",
              border:'0',
            }}
            onChange={(e) => {
              setFormState({ ...formState, label_type_id: +e.target.value });
            }}
          >
            <option value="0" disabled hidden>
              選擇標籤
            </option>
            <option value="1">警告</option>
            <option value="2">景點</option>
            <option value="3">動物</option>
            <option value="4">商鋪</option>
            <option value="5">飲品供應</option>
            <option value="6">洗手間</option>
            <option value="7">其他</option>
          </select>
          <div
            style={{
              marginLeft: "1rem",
              height: "3rem",
              width: "3rem",
              border: "3px dashed",
              borderRadius: "6px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            {formState.label_type_id === 0 ? (
              <FontAwesomeIcon
                style={{ width: "3rem" }}
                icon={faQuestion as IconProp}
                size="2x"
                color="grey"
              />
            ) : (
              <FontAwesomeIcon
                style={{ width: "3rem" }}
                icon={labelIdToIcon[formState.label_type_id][0]  as IconProp}
                size="2x"
                color={labelIdToIcon[formState.label_type_id][1]}
              />
            )}
          </div>
        </div>
        {/* {is_private} */}
          <div
            style={{
              height:'100%',
              width: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              marginTop: "2rem",
              marginBottom:"1rem",
            }}
          >
            <IonLabel>公開&nbsp;&nbsp;&nbsp;</IonLabel>
            <IonToggle
              checked={!formState.is_private}
              value="private"
              onIonChange={(e) =>
                setFormState({ ...formState, is_private: !e.detail.checked })
              }
            />
          </div>
        {/* {remind message} */}
        {/* <label className="animate__animated animate__bounce" htmlFor="">animation</label> */}
        <div style={{ height: "2rem" }} className={remindTextClassName}>
          <IonLabel
            hidden={!isInsufficient}
            style={{
              width: "100%",
              margin: 0,
              textAlign: "center",
              color: remindTextColor,
              fontSize: "0.8rem",
            }}
          >
            {remindMessage}
          </IonLabel>
        </div>

        {/* {button group} */}
        <div
          style={{
            height: "3rem",
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <IonButton
            style={{ margin: "1em" }}
            onTouchStart={() => {
              setFormState(defaultFormState);
              setPreviewImage("");
              setIsInsufficient(false);
              setRemindMessage("none");
            }}
            onTouchEnd={props.onClose}
          >
            取消
          </IonButton>
          <IonButton style={{ margin: "1em" }} onTouchEnd={(e) => toParent(e)}>
            確定
          </IonButton>
        </div>
      </div>
    </div>,
    mountToNode
  );
};

const modalStyles: React.CSSProperties = {
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
  backgroundColor: "#FFF",
  padding: "30px",
  width: "90%",
  height: "85vh",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
  borderRadius: "0.5rem",
  zIndex: 1000,
};

const overlayStyles: React.CSSProperties = {
  position: "fixed",
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  backgroundColor: "rgba(0,0,0,0.6)",
  zIndex: 1000,
  // display:'none',
};

const inputStyles: React.CSSProperties = {
  margin: "5px",
  border: "1px solid grey",
  borderRadius: "0.5rem",
  textAlign: "center",
};

type FormState = {
  title: string;
  label_type_id: number;
  map_label_photo: any;
  gps_latitude: number;
  gps_longitude: number;
  is_private: boolean;
};
