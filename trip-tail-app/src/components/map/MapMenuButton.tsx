import { faBars } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { IonMenuToggle } from "@ionic/react"

export const MapMenuButton = () =>{
    return (
        <IonMenuToggle >
        <button style={buttonStyle} ><FontAwesomeIcon
        icon={faBars}
        size="2x"
        color={"#4B86F7"}
      /></button>
      </IonMenuToggle>
    )
}

const buttonStyle:React.CSSProperties ={
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    position: "fixed",
    top: "10%",
    left: "3%",
    width: "3.5rem",
    height: "3.5rem",
    backgroundColor: "rgba(255,255,255,0.9)",
    boxShadow: "3px 3px 20px 0.1px rgba(0,0,0,0.5)",
    borderRadius: "0.5rem",
    zIndex: 999,
}