import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import React, { useEffect, useState } from "react";
import styles from "../../pages/MapTab.module.css";
import { useIonViewDidEnter } from "@ionic/react";

export const SearchButton = () => {
  const [isOpenSearchBar, setIsOpenSearchBar] = useState(false);
  // console.log(geoSearch?.classList)

  useIonViewDidEnter(() => {
    document
      .getElementsByClassName("leaflet-control-geosearch")[1]
      .classList.add("d-none");
  });

  useEffect(() => {
    const searchBar = document.getElementsByClassName(
      "leaflet-control-geosearch"
    );
    if (searchBar) {
      if (isOpenSearchBar) {
        document
          .getElementsByClassName("leaflet-control-geosearch")[1]
          ?.classList.remove("d-none");
      } else {
        document
          .getElementsByClassName("leaflet-control-geosearch")[1]
          ?.classList.add("d-none");
      }
    }
  }, [isOpenSearchBar]);

  return (
    <button
      className={styles.allToCenter}
      style={searchStyles}
      onTouchEnd={() => setIsOpenSearchBar(!isOpenSearchBar)}
    >
      <FontAwesomeIcon
        icon={faMagnifyingGlass}
        size="3x"
        color={isOpenSearchBar ? "grey" : "#4B86F7"}
      />
    </button>
  );
};

const searchStyles: React.CSSProperties = {
  position: "fixed",
  top: "10%",
  right: "3%",
  width: "3.5rem",
  height: "3.5rem",
  backgroundColor: "rgba(255,255,255,0.9)",
  boxShadow: "3px 3px 20px 0.1px rgba(0,0,0,0.5)",
  borderRadius: "3rem",
  zIndex: 999,
};
