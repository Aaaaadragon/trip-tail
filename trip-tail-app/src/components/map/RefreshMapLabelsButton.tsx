import {
  faArrowsRotate,

} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IonToast } from "@ionic/react";
import { cloudDoneOutline } from "ionicons/icons";
import {  useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setGetMapLabelsStatusAction } from "../../redux/map/action";
import { getMapLabelsThunk } from "../../redux/map/thunk";
import { RootState } from "../../redux/state";

export const RefreshMapLabelsButton = () => {
  const dispatch = useDispatch();
  const [currentRefreshAngle, setCurrentRefreshAngle] = useState(0);
  const [refreshRotateAngle, setRefreshRotateAngle] = useState(0);
  const [isToastOpen, setIsToastOpen] = useState(false);
  const getLatestLabels = async () => {
    dispatch(setGetMapLabelsStatusAction({ type: "idle" }));
    dispatch(getMapLabelsThunk());

    let timer = setInterval(() => {
      setRefreshRotateAngle((refreshRotateAngle) => {
        // if (refreshRotateAngle === 360) {
        //   refreshRotateAngle = 0;
        // }
        if (refreshRotateAngle >= currentRefreshAngle + 720) {
          clearInterval(timer);
          setCurrentRefreshAngle(refreshRotateAngle);
          if (labelStatus === "success") {
            setIsToastOpen(true);
          }
        }

        return refreshRotateAngle + 30;
      });
    }, 200);
  };

  const buttonStyle: React.CSSProperties = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "fixed",
    top: "39%",
    right: "5%",
    width: "1.9rem",
    height: "1.9rem",
    backgroundColor: "rgba(255,255,255,0.9)",
    borderRadius: "0.3rem",
    zIndex: 999,
  };

  const labelStatus = useSelector(
    (state: RootState) => state.map.fetch.getMapLabelsResult.type
  );

  return (
    <>
      <button style={buttonStyle} onTouchEnd={() => getLatestLabels()}>
        <FontAwesomeIcon
          style={{
            transform: `rotate(${refreshRotateAngle}deg)`,
            transition: "all 0.5s",
          }}
          icon={faArrowsRotate}
          size="1x"
          color={"#4B86F7"}
        />
      </button>
      <IonToast
        isOpen={isToastOpen}
        color="secondary"
        position="middle"
        icon={cloudDoneOutline}
        message={

          labelStatus === "success"
            ? "成功獲取最新地圖標籤!"
            : "獲取最新地圖標籤失敗，請重試。"
        }
        duration={1500}
        onDidDismiss={() => setIsToastOpen(false)}
      />
    </>
  );
};