import { faMountain } from "@fortawesome/free-solid-svg-icons";
import { IonButton, IonLabel } from "@ionic/react";
import { LatLngExpression } from "leaflet";
import { useState } from "react";
import { Marker, Popup } from "react-leaflet";
import { createMarkerByFaIcon } from "../../icons/createMarkerIcon";
import { MountainInfo } from "../../redux/mountainInfo/state";
import { MountainInfoDetailModal } from "../home/MountainInfoDetailModal";
import { NumberToStar } from "../NumberToStartIcon";
import "./MountainLabel.css";

export const MountainLabel = (props: { mountainInfo: MountainInfo }) => {
  const mountainInfo = props.mountainInfo;
  const {
    name,
    gps_latitude,
    gps_longitude,
    difficulty,
    scenery_rating,
    image_filename,
  } = mountainInfo;
  // if (!gps_latitude || !gps_longitude) return null;

  const [isModalOpen, setIsModalOpen] = useState(false);

  let position: LatLngExpression = [gps_latitude!, gps_longitude!];

  return (
    <>
      <Marker position={position} icon={createMarkerByFaIcon(faMountain)}>
        <Popup>
          <div className="mountain-map-label-popup">
            <img
              style={{ borderRadius: "0.5rem",boxShadow:"2px 2px 5px 1px rgba(0,0,0,0.3)" }}
              src={`https://www.oasistrek.com/${image_filename}`}
              alt={"mountain"}
            />
            <IonLabel style={mountainTitleStyle}>《{name}》</IonLabel>
            <IonLabel
              style={ratingAreaStyles}
            >
              <div
                style={ratingLabelStyles}
              >
                難度：{NumberToStar(difficulty, "1.2rem", "#FFF278")}
              </div>
              <div
                style={ratingLabelStyles}
              >
                景點：{NumberToStar(scenery_rating, "1.2rem", "#FFF278")}
              </div>
            </IonLabel>
            <IonButton onTouchEnd={() => setIsModalOpen(true)}>
              了解更多
            </IonButton>
          </div>
        </Popup>
      </Marker>
      <MountainInfoDetailModal
        mountainInfo={props.mountainInfo}
        isOpen={isModalOpen}
        onLeaveModal={() => setIsModalOpen(false)}
      />
    </>
  );
};
const mountainTitleStyle: React.CSSProperties = {
  fontSize: "1.5rem",
  marginTop: "0.5rem",
};

const ratingAreaStyles:React.CSSProperties = {
    display: "flex",
    flexDirection: "column",
    marginBottom: "0.5rem",
  }

const ratingLabelStyles:React.CSSProperties = {
    fontSize: "1.2rem",
    display: "flex",
    alignItems: "center",
  }