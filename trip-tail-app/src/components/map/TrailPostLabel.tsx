import { faImage, faPencil } from "@fortawesome/free-solid-svg-icons";
import {
  IonButton,
  IonHeader,

  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonModal,
  IonTextarea,
  IonToolbar,
} from "@ionic/react";

import { useState } from "react";
import { Marker, Popup } from "react-leaflet";
import { env } from "../../env";
import { createMarkerByFaIcon } from "../../icons/createMarkerIcon";
import { TempTrailPost } from "../../redux/map/state";
import { PageHeader } from "../PageHeader";
import { LabelPreviewImageContainer } from "./LabelPreviewImageContainer";
import defaultPhoto from '../../trail-record-default-photo.png'
export const TrailPostLabel = (props: { trailPost: TempTrailPost }) => {
  const [openModal, setOpenModal] = useState(false);
  const trailPost = props.trailPost;
  if (!trailPost.gps_latitude || !trailPost.gps_longitude) return null;

  let image = trailPost.trail_record_photo;
  if (typeof image !== "string") {
    image = URL.createObjectURL(image as Blob);
  } else if(image.length>1) {
    image = env.AWS_S3_UPLOADS_ORIGIN + image;
  }

  return (
    <Marker
      position={[trailPost.gps_latitude, trailPost.gps_longitude]}
      icon={createMarkerByFaIcon(image!==''?faImage:faPencil, { color: "#e8be31" })}
    >
      <Popup>
        <div
          style={{
            width: "55vw",
            height: "auto",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "flex-start",
          }}
        >
          <IonLabel style={titleStyles}>《{trailPost.title}》</IonLabel>
          {trailPost.trail_record_photo ? (
            <LabelPreviewImageContainer image={trailPost.trail_record_photo} />
          ) : null}
          <IonButton onClick={() => setOpenModal(true)}>打開詳情</IonButton>
        </div>
        <IonModal
          isOpen={openModal}
          swipeToClose={true}
          onIonModalDidDismiss={() => setOpenModal(false)}
        >

          <IonHeader>

            <PageHeader />
          <div style={{width:'100%',height:'4vh'}}></div>

            {/* {solid line to remind user this page can be swipe to close} */}
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
                position: "fixed",
                top: "6vh",
              }}
            >
              <div
                style={{
                  height: "0.5rem",
                  width: "30vw",
                  backgroundColor: "#2e2e2e",
                  borderRadius: "0.5rem",
                }}
              ></div>
            </div>
            <IonToolbar
              className="w-100 text-center"
              style={{ color: "white", fontSize: "1.5rem" }}
            >
              足跡途中紀錄
            </IonToolbar>
          </IonHeader>
          <IonList>
            <IonItem>
              <IonLabel position="stacked">標題</IonLabel>
              <IonInput type="text" readonly={true} value={trailPost.title}>
              </IonInput>
            </IonItem>
            <IonItem>
              <div style={{ height: "35vh" }}>
                <div
                  style={{
                    width: "100vw",
                    marginTop: "0.5rem",
                    marginBottom: "0.5rem",
                    //   border: `7px inset ${iconContent[1]}`,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <img
                    onTouchEnd={() => setOpenModal(true)}
                    style={{
                      height: "auto",
                      width: "75vw",
                      objectFit: "cover",
                    }}
                    src={image?image:defaultPhoto}
                    alt="preview"
                    loading="lazy"
                  />
                </div>
              </div>
            </IonItem>
            <IonItem>
              <IonItem>
                <IonLabel position="stacked">描述</IonLabel>
                <IonTextarea
                  value={trailPost.description}
                  autoGrow={true}
                  readonly={true}
                ></IonTextarea>
              </IonItem>
            </IonItem>
          </IonList>
          <div style={buttonGroupStyles}></div>
        </IonModal>
      </Popup>
    </Marker>
  );
};

const titleStyles: React.CSSProperties = {
  width: "100%",
  fontSize: "1.2rem",
  color: "white",
  textAlign: "center",
};

const buttonGroupStyles: React.CSSProperties = {
  marginTop: "0.5rem",
  width: "100%",
  display: "flex",
  justifyContent: "center",
};
