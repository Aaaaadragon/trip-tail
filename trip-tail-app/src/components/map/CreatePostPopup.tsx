import { IonButton, IonInput, IonLabel, IonTextarea } from "@ionic/react";
import React, { useState } from "react";
import ReactDOM from "react-dom";
import { usePosition } from "../../hooks/usePosition";
import defaultPhoto from "../../trail-record-default-photo.png";
import imageCompression from "browser-image-compression";
import { TempTrailPost } from "../../redux/map/state";

const imageCompressOptions = {
  maxSizeMB: 0.5,
};

export const CreatePostPopup = (props: {

  open: boolean;
  onClose: () => void;
  onDecided: (trailPost:TempTrailPost) => void;
}) => {
  const currentLocation: any = usePosition();


  const defaultFormState: FormState = {
    title: "",
    trail_record_photo: "",
    description: "",
    gps_latitude: currentLocation[0],
    gps_longitude: currentLocation[1],
  };

  let remindMessageList = {
    title: "標題不能為空，最多二十個字元",
  };

  const mountToNode = document.getElementById("portal")!;
  const [formState, setFormState] = useState<FormState>(defaultFormState);
  const [isInsufficient, setIsInsufficient] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [remindMessage, setRemindMessage] = useState("none");
  const [remindTextClassName, setRemindTextClassName] =
    useState("animate__animated ");

  if (!props.open) return null;
  const remindTextColor = isInsufficient ? "red" : "white";

  const submit = (e: React.TouchEvent) => {
    e.preventDefault();
    if (formState.title.length > 20 || formState.title.length <= 0) {
      setRemindMessage(remindMessageList.title);
      console.log("title problem");
      triggerReminder();
      setIsInsufficient(true);
      return;
    }


    // do it finally once only
    // let postFormData = new FormData();
    // for (let [key, value] of Object.entries({
    //   ...formState,
    //   gps_latitude: currentLocation[0],
    //   gps_longitude: currentLocation[1],
    // })) {
    //   if (key === "map_label_photo") {
    //     console.log("insert photo to FormData");
    //     postFormData.append(key, value, formState.title);
    //   } else {
    //     postFormData.append(key, value);
    //   }
    // }

    // currentPosts.push(formState)
    // dispatch(setTempTrailPostAction(currentPosts))


    //   for (var pair of mapLabelFormData.entries()) {
    //     console.log(pair[0]+ ', ' + pair[1]);
    // }

    // dispatch(createMapLabelThunk(postFormData));
    let toParentFormState =  {...formState,gps_latitude:currentLocation[0],gps_longitude:currentLocation[1]}
    setIsInsufficient(false);
    setPreviewImage("");
    setFormState(defaultFormState);
    setRemindMessage("none");
    props.onDecided(toParentFormState);
    props.onClose();
  };

  const triggerReminder = () => {
    setRemindTextClassName(
      (remindTextClassName) => (remindTextClassName += "animate__headShake")
    );

    setTimeout(() => {
      setRemindTextClassName("animate__animated ");
    }, 2000);
    return;
  };

  const onFileChange = async (fileChangesEvent: any) => {
    let photoFile = fileChangesEvent.target.files[0];
    // console.log('originalFile instanceof Blob', photoFile instanceof Blob); // true
    // console.log(`originalFile size ${photoFile.size / 1024 / 1024} MB`);
    try {
      const compressedPhotoFile: Blob = await imageCompression(
        photoFile,
        imageCompressOptions
      );
      // console.log('compressedFile instanceof Blob', compressedPhotoFile instanceof Blob); // true
      // console.log(`compressedFile size ${compressedPhotoFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
      setFormState({ ...formState, trail_record_photo: compressedPhotoFile });
      let objURL = URL.createObjectURL(compressedPhotoFile);
      setPreviewImage(objURL);
    } catch (error) {
      console.log("Image compression failed, ERROR:", error);
    }
  };

  return ReactDOM.createPortal(
    <div style={overlayStyles}>
      <div style={modalStyles}>
        <div style={{ height: "4rem" }}>
          <IonInput
            placeholder="標題"
            style={inputStyles}
            type="text"
            value={formState.title}
            onIonChange={(e) => {
              setFormState({ ...formState, title: e.detail.value || "" });
            }}
          ></IonInput>
        </div>
        <IonLabel
          className="w-100 ion-text-center m-0"
          style={{ fontSize: "0.8rem", color: "grey" }}
        >
          (不能為空，最多二十個字元)
        </IonLabel>
        <div
          className="w-100"
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            height: "45vh",
          }}
        >
          <div
            style={{
              height: "29vh",
              width: "100%",
              marginTop: "0.5rem",
              marginBottom: "0.5rem",
              border: "3px dashed #175733",
              borderRadius: "6px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <img
              style={{
                height: "100%",
                width: "auto",
                padding: "0.2rem",
              }}
              className="img-fluid"
              src={previewImage === "" ? defaultPhoto : previewImage}
              alt="preview"
            />
          </div>
          <IonLabel
          className="w-100 ion-text-center m-0"
          style={{ fontSize: "0.8rem", color: "grey" }}
        >
          (附上相片可使得紀錄更充實)
        </IonLabel>
          <IonButton>
          <label htmlFor="files">上傳相片</label>
          </IonButton>
          <input
            id="files"
            style={{ visibility: "hidden", width: "0" }}
            type="file"
            onChange={(e) => onFileChange(e)}
            accept="image/*"
          ></input>
        </div>

        {/* {description textArea} */}
        <div style={textAreaContainerStyle}>
        <div className="w-100 ion-text-center">描述</div>
        <IonTextarea
            style={textAreaStyle}
          placeholder="描述一下這個紀錄..."
          value={formState.description}
          onIonChange={(e) =>
            setFormState({ ...formState, description: e.detail.value! })
          }
        ></IonTextarea>
        </div>

        {/* remind message */}
        <div style={{ height: "2rem" }} className={remindTextClassName}>
          <IonLabel
            hidden={!isInsufficient}
            style={{
              width: "100%",
              margin: 0,
              textAlign: "center",
              color: remindTextColor,
              fontSize: "0.8rem",
            }}
          >
            {remindMessage}
          </IonLabel>
        </div>
        {/* {cancel and confirm button} */}
        <div
          style={{
            marginTop:'0.5rem',
            height: "3rem",
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <IonButton
            style={{ margin: "1em" }}
            onTouchStart={() => {
              setFormState(defaultFormState);
              setPreviewImage("");
              setIsInsufficient(false);
              setRemindMessage("none");
            }}
            onTouchEnd={props.onClose}
          >
            取消
          </IonButton>
          <IonButton style={{ margin: "1em" }} onTouchEnd={(e) => submit(e)}>
            確定
          </IonButton>
        </div>
      </div>
    </div>,
    mountToNode
  );
};

const modalStyles: React.CSSProperties = {
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
  backgroundColor: "#FFF",
  padding: "30px",
  width: "90%",
  height: "85vh",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
  borderRadius: "0.5rem",
  zIndex: 1200,
};

const overlayStyles: React.CSSProperties = {
  position: "fixed",
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  backgroundColor: "rgba(0,0,0,0.6)",
  zIndex: 1200,
  // display:'none',
};

const inputStyles: React.CSSProperties = {
    width:"70vw",
  margin: "5px",
  border: "1px solid grey",
  borderRadius: "0.5rem",
  textAlign: "center",
};

const textAreaContainerStyle:React.CSSProperties ={
    width:"70vw",
}

const textAreaStyle:React.CSSProperties = {
    border:'1px solid',
    padding:'0.5rem',
}

type FormState = {
  title: string;
  trail_record_photo: any;
  description: string;
  gps_latitude: number;
  gps_longitude: number;
};

