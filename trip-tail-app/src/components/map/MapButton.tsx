import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCrosshairs, faMapLocation } from "@fortawesome/free-solid-svg-icons";
import { useMap } from "react-leaflet";
import { usePosition } from "../../hooks/usePosition";
import React from "react";
import styles from "../../pages/MapTab.module.css";

export const MapButton = (props: {
  purpose: Purpose;
  color?: string;
  onTouchEnd?: () => void;
}) => {
  const position = usePosition();
  const map = useMap();

  const { purpose, color, onTouchEnd } = props;

  const setToPositionView = (e: React.TouchEvent) => {
    e.stopPropagation();
    e.preventDefault();
    map.flyTo(position, 18);
  };

  return (
    <button
      className={styles.allToCenter}
      style={purposeStyles[purpose] as any}
      onTouchEnd={
        purpose === "SetPositionView" ? (e) => setToPositionView(e) : onTouchEnd
      }
    >
      <FontAwesomeIcon
        icon={purposeIcons[purpose]}
        size="3x"
        color={color ? color : "#4B86F7"}
      />
    </button>
  );
};

type Purpose = "SetPositionView" | "CreateMapLabel";

const purposeIcons = {
  SetPositionView: faCrosshairs,
  CreateMapLabel: faMapLocation,
};

const purposeStyles = {
  SetPositionView: {
    position: "fixed",
    bottom: "25%",
    right: "3%",
    width: "3.5rem",
    height: "3.5rem",
    backgroundColor: "rgba(255,255,255,0.9)",
    boxShadow: "3px 3px 20px 0.1px rgba(0,0,0,0.5)",
    borderRadius: "3rem",
    zIndex: 999,
    
  },
  CreateMapLabel: {
    position: "fixed",
    bottom: "25%",
    left: "3.5%",
    width: "3.7rem",
    height: "3.7rem",
    backgroundColor: "rgba(255,255,255,0.9)",
    boxShadow: "3px 3px 20px 0.1px rgba(0,0,0,0.5)",
    borderRadius: "1rem",
    zIndex: 999,

  },
};
