import { IonIcon, IonLabel, IonModal } from "@ionic/react";
import { arrowDown } from "ionicons/icons";
import { useState } from "react";
import { env } from "../../env";

export const LabelPreviewImageContainer = (props: { image: any }) => {
  const [openModal, setOpenModal] = useState(false);
  let image = props.image;
  if (typeof image !== "string") {
    image = URL.createObjectURL(image as Blob);
  } else {
    image = env.AWS_S3_UPLOADS_ORIGIN + props.image;
  }
  return (
    <>
      <div
        style={{
          height: "auto",
          width: "50vw",
          marginTop: "0.5rem",
          marginBottom: "0.5rem",
          //   border: `7px inset ${iconContent[1]}`,
          borderRadius: "8px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          boxShadow: "6px 7px 8px -1px rgba(0,0,0,0.5)",
        }}
      >
        <img
          onTouchEnd={() => setOpenModal(true)}
          style={{
            height: "100%",
            width: "100%",
            borderRadius:'0.5rem',
          }}
          src={image}
          alt="preview"
          loading="lazy"
        />
      </div>
      <IonModal isOpen={openModal} className="modal-class" swipeToClose={true} onIonModalDidDismiss={()=>setOpenModal(false)}>
        <div className="modal-image ion-padding">
          <img src={image} alt="preview" />
          <IonIcon
            style={{ width: "5rem", height: "5rem" }}
            icon={arrowDown}
            color="light"
          ></IonIcon>
          <IonLabel color="light" style={{fontSize:'1.5rem',fontWeight:'bolder'}}>向下掃關閉預覽</IonLabel>
        </div>
      </IonModal>
    </>
  );
};
