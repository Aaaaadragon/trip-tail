import {
  faPersonHiking,
  faPlay,
  faRoute,
  faStop,
  faStopwatch,
  IconDefinition,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "../../pages/MapTab.module.css";
import {
  setStartRecordingAction,
} from "../../redux/map/action";
import { startClockThunk, startRecordGPSDataThunk, stopClockThunk, stopRecordGPSDataThunk } from "../../redux/map/thunk";
import { RootState } from "../../redux/state";
import { CreateTrailRecordPopup } from "./CreateTrailRecordPopup";
import { useRecordingData } from "../../hooks/useRecordingData";
import { useRole } from "../../hooks/useRole";
import { IonToast } from "@ionic/react";
import SweetAlert from 'react-bootstrap-sweetalert'
import { TrailPostButton } from "./TrailPostButton";
import { IconProp } from "@fortawesome/fontawesome-svg-core";

export const TracingMenuBar = (props:{onStop:()=>void}) => {
  const dispatch = useDispatch();
  const [barWidth, setBarWidth] = useState("3.8rem");
  const [hiddenRecordInfo, setHiddenRecordInfo] = useState(true);
  const [isCreateRecordOpen, setIsCreateRecordOpen] = useState(false);
  const [remindMessage, setRemindMessage] = useState('')
  const [triggerSweetAlert, setTriggerSweetAlert] = useState(false)
  const [trialPostButtonHidden,setTrailPostButtonHidden] = useState(true)
  let { isRecording, clockData } = useSelector((state: RootState) => state.map);
  const { passedTime } = clockData;
  let RecordingIcon: IconDefinition = isRecording ? faStop : faPlay;
  let transitionStyle = passedTime > 3000 && isRecording ? "unset" : "all 2s";

  let role = useRole()
  let recordingData = useRecordingData()
  let trail_name = recordingData.trail_name
  let totalDistance = recordingData.distance
  

  const startRecording = (title:string) => {
    dispatch(setStartRecordingAction(title,3));
    setBarWidth((barWidth) => (barWidth !== "95%" ? "95%" : barWidth));
    setTimeout(() => {
      setHiddenRecordInfo(false);
      setTrailPostButtonHidden(false)
    }, 2000);
    dispatch(startClockThunk());
    dispatch(startRecordGPSDataThunk())
  };

  const stopRecording = async() => {
    let {GPSData, duration, distance} = recordingData
 
    if(!GPSData ||GPSData.length < 3){
      setRemindMessage("GPS 紀錄不足，紀錄無法上傳至伺服器。")
    }
    else if(duration<30000){
      setRemindMessage("足跡時長低於30秒，紀錄無法上傳至伺服器。")
    }
    else if(distance<0.1){
      setRemindMessage("足跡不足0.1公里，紀錄無法上傳至伺服器。")
    }else{
      setTriggerSweetAlert(true)
    }
    setTimeout(()=>{
      setBarWidth("3.8rem");
      props.onStop()
    },200)
    dispatch(stopClockThunk());
    dispatch(stopRecordGPSDataThunk());
    setHiddenRecordInfo(true);
    setTrailPostButtonHidden(true)
    
  };

  const recordButtonToAction = (e: React.TouchEvent) => {
    if(role === 'guest'){
      setRemindMessage("登入後方可使用紀錄足跡功能")
      return
    }
    e.stopPropagation();
    e.preventDefault();
    
    isRecording? stopRecording():setIsCreateRecordOpen(true);
  };

  const decidedToRecording = (title?:string) => {
    if(!title){
      return
    }
    startRecording(title);
    
  }

  useEffect(() => {
    if (isRecording) {
      setBarWidth("95%");
      if (passedTime > 3000) {
        setHiddenRecordInfo(false);
      }
    }
  }, [dispatch, passedTime,isRecording]);

  return (
    <>
            

    <IonToast isOpen={remindMessage===""?false:true} message={remindMessage} duration={1000} onDidDismiss={()=>{setRemindMessage("")}}/>    
    <div
      className={styles.tracingMenuBar}
      style={{ width: barWidth, transition: transitionStyle }}
    >
      <TrailPostButton hidden={trialPostButtonHidden}/>
      <button
        className={styles.startRecordingButton + " " + styles.allToCenter}
        onTouchEnd={(e) => {
          recordButtonToAction(e);
        }}
      >
        <FontAwesomeIcon icon={RecordingIcon  as IconProp} size="2x" color="red" />
      </button>

      <CreateTrailRecordPopup
        open={isCreateRecordOpen}
        onClose={() => setIsCreateRecordOpen(false)}
        onDecided={(message)=>decidedToRecording(message)}
      />

      {isRecording ? (
        <div hidden={hiddenRecordInfo}>
          <div className={styles.recordTitle }>
            <FontAwesomeIcon icon={faPersonHiking  as IconProp} color="white" />
            &nbsp;&nbsp;{trail_name}
          </div>
          <p className={styles.recordTimeInfo}>
            <FontAwesomeIcon icon={faStopwatch  as IconProp} color="white" />
            &nbsp;{ms2Format(passedTime)}&nbsp;&nbsp;
            <FontAwesomeIcon icon={faRoute  as IconProp} color="white" />
            &nbsp;&nbsp;{totalDistance.toFixed(1)}公里
          </p>
        </div>
      ) : null}
    </div>
    <SweetAlert
    openAnim={triggerSweetAlert}
    hideOverlay={true}
    show={triggerSweetAlert}
    success
    title="成功錄製足跡！"
    onConfirm={()=>setTriggerSweetAlert(false)}
>
</SweetAlert>
    </>
  );
};

function ms2Format(ms: number) {
  let totalSecond = Math.floor(ms / 1000);
  let hour = Math.floor(totalSecond / (60 * 60));
  let minute = Math.floor((totalSecond - hour * 60 * 60) / 60);
  let second = totalSecond - hour * 60 * 60 - minute * 60;

  return (
    formTwoDigits(hour) +
    ":" +
    formTwoDigits(minute) +
    ":" +
    formTwoDigits(second)
  );
}

function formTwoDigits(number: number): string {
  if (number.toString().length === 1) {
    return "0" + number;
  }
  return number.toString();
}
