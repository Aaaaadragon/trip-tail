import { IonButton, IonInput, IonLabel } from "@ionic/react";
import React, { useState } from "react";
import ReactDOM from "react-dom";

const modalStyles: React.CSSProperties = {
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
  backgroundColor: "#FFF",
  padding: "30px",
  width: "70%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
  borderRadius: "0.5rem",
  zIndex: 1000,
};

const overlayStyles: React.CSSProperties = {
  position: "fixed",
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  backgroundColor: "rgba(0,0,0,0.6)",
  zIndex: 1000,
};

const inputStyles: React.CSSProperties = {
  margin: "5px",
  border: "1px solid grey",
  borderRadius: "0.5rem",
  textAlign: "center",
};

export const CreateTrailRecordPopup = (props: {
  open: boolean;
  onClose: () => void;
  onDecided: (title: string) => void;
}) => {
  const mountToNode = document.getElementById("portal")!;

  const [titleInput, setTitleInput] = useState("");
  const [isOverLengthLimit, setIsOverLengthLimit] = useState(false);
  const [remindTextClassName,setRemindTextClassName] = useState("animate__animated ")

  if (!props.open) return null;
  const remindTextColor = isOverLengthLimit ? "red" : "grey";

  const toParent = (e: React.TouchEvent) => {
    if (titleInput.length > 10 || titleInput.length <= 0) {
        setIsOverLengthLimit(true)
        setRemindTextClassName((remindTextClassName)=>
            remindTextClassName += "animate__shakeX"
        )
      setTimeout(() => {
        setRemindTextClassName("animate__animated ")
      }, 2000);

      return;
    }
    
    setIsOverLengthLimit(false)
    setTitleInput('')
    props.onDecided(titleInput);
    props.onClose()
  };


  return ReactDOM.createPortal(
    <div style={overlayStyles}>
      <div style={modalStyles}>
        <IonLabel>今次足跡名稱為 : </IonLabel>
        <IonInput
          style={inputStyles}
          type="text"
          value={titleInput}
          onIonChange={(e) => {
            setTitleInput(e.detail.value || "");

          }}
        ></IonInput>
        <IonLabel
          className={remindTextClassName}
          style={{
            width: "100%",
            margin: 0,
            textAlign: "center",
            color: remindTextColor,
            fontSize: "0.8rem",
          }}
        >
          {"(不能為空，最多十個字元)"}
        </IonLabel>
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <IonButton style={{ margin: "1em" }} onTouchEnd={props.onClose}>
            取消
          </IonButton>
          <IonButton style={{ margin: "1em" }} onTouchEnd={(e) => toParent(e)}>
            確定
          </IonButton>
        </div>
      </div>
    </div>,
    mountToNode
  );
};
