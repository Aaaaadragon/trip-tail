import { IonButton, IonChip, IonLabel, IonModal, IonToast } from "@ionic/react";
import { Marker, Popup } from "react-leaflet";
import ReactDOMServer from "react-dom/server";
import { env } from "../../env";
import { labelIdToIcon } from "../../icons/icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import L from "leaflet";
import "./MapLabels.css";
import "./MapLabels.scss";
import {
  faCircleCheck,
  faCircleXmark,
} from "@fortawesome/free-solid-svg-icons";
import { MapLabel } from "../../redux/map/state";
import { UserIcon } from "../UserIcon";
import { useState } from "react";
import { CrossCancelButton } from "../CrossCancelButton";
import { useAuthUser } from "../../hooks/useAuthUser";
import { useRole } from "../../hooks/useRole";
import { useDispatch } from "react-redux";
import { updateMapLabelThunk } from "../../redux/map/thunk";


type MapLabelAction = "like" | "dislike";

export const MapLabels = (props: {
  mapLabel: MapLabel;
  forProfile?: boolean;
}) => {
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const mapLabel = props.mapLabel;

  let { like_members, dislike_members, created_at } = mapLabel;

  if (!like_members) like_members = [];
  if (!dislike_members) dislike_members = [];

  const iconContent = labelIdToIcon[mapLabel.label_type_id];

  const userRole = useRole();

  let user_id = useAuthUser()?.payload.id;

  if (!user_id) user_id = 0;

  let inList: boolean;
  if (
    like_members.includes(user_id) ||
    dislike_members.includes(user_id) ||
    userRole === "guest"
  ) {
    inList = true;
  } else {
    inList = false;
  }

  const [isInLikeDislikeList, setIsInLikeDislikeList] = useState(inList);

  const iconHTML = ReactDOMServer.renderToString(
    <div
      className="custom-marker-pin"
      style={{ backgroundColor: iconContent[1] }}
    >
      <FontAwesomeIcon
        className="custom-material-icons"
        icon={iconContent[0]}
        color={iconContent[1]}
      />
    </div>
  );

  const customIcon = L.divIcon({
    className: "custom-marker-icon",
    html: iconHTML,
    iconSize: [30, 42],
    iconAnchor: [15, 42],
    popupAnchor: [0, -32],
  });

  const likeOrDislikeMapLabel = (action: MapLabelAction) => {
    if (isInLikeDislikeList && userRole === "guest") {
      setOpenToast(true);
      return;
    }
    if (!like_members) like_members = [];
    if (!dislike_members) dislike_members = [];
    if (isInLikeDislikeList) {
      if (!user_id) return;


      if (action === "like") {
        if (dislike_members?.includes(user_id)) return;
        let index = like_members.indexOf(user_id);
        if(index>-1) like_members.splice(index,1)
        like_members = [...new Set(like_members)]
        dispatchToUpdateMapLabel({ ...mapLabel, like_members });
        setIsInLikeDislikeList(false);

        return;
      } else if (action === "dislike") {
        if (like_members?.includes(user_id)) return;
        let index = dislike_members.indexOf(user_id);
        if(index>-1) dislike_members.splice(index,1)
        dislike_members = [...new Set(dislike_members)]
        dispatchToUpdateMapLabel({ ...mapLabel, dislike_members });
        setIsInLikeDislikeList(false);

        return;
      }
    }
    if (action === "like") {
      if (!like_members) like_members = [];
      like_members.push(user_id as number);

      dispatchToUpdateMapLabel({ ...mapLabel, like_members });
      setIsInLikeDislikeList(true);

    } else if (action === "dislike") {
      if (!dislike_members) dislike_members = [];
      dislike_members.push(user_id as number);
      dispatchToUpdateMapLabel({ ...mapLabel, dislike_members });
      setIsInLikeDislikeList(true);

    }
  };

  const dispatchToUpdateMapLabel = (mapLabel: MapLabel) => {
    // dispatch(setUpdateMapLabelLikeAction(mapLabel));
    dispatch(updateMapLabelThunk(mapLabel));
  };

  return (
    <>
      <Marker
        key={mapLabel.id}
        position={[mapLabel.gps_latitude, mapLabel.gps_longitude]}
        icon={customIcon}
      >
        <Popup>
          <div
            style={{
              height: "20vh",
              width: "50vw",
              marginTop: "0.5rem",
              marginBottom: "0.5rem",
              border: `7px inset ${iconContent[1]}`,
              borderRadius: "8px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              boxShadow: "6px 7px 8px -1px rgba(0,0,0,0.5)",
            }}
          >
            <img
              onTouchEnd={() => setOpenModal(true)}
              style={{
                height: "100%",
                width: "auto",
                padding: "0.2rem",
              }}
              src={env.AWS_S3_UPLOADS_ORIGIN + mapLabel.image_filename}
              alt="preview"
              loading="lazy"
            />
          </div>
          <div className="w-100 text-center">
            <IonLabel style={{ fontSize: "15px", fontWeight: "bolder" }}>
              {mapLabel.title}
            </IonLabel>
            <br />
            <IonLabel>{created_at!.split("T")[0]}</IonLabel>
            {props.forProfile ? null : (
              <div
                style={{
                  height: "3rem",
                  marginBottom: "0.9rem",
                  marginTop: "0.5rem",
                  display: "flex",
                  justifyContent: "center",
                  alignContent: "center",
                }}
              >
                <UserIcon
                  style={{ margin: 0 }}
                  iconSize="3rem"
                  photo={mapLabel.icon_photo}
                />
                <IonChip
                  color="tertiary"
                  style={{
                    height: "100%",
                    display: "flex",
                    fontSize: "1.3rem",
                    alignItems: "center",
                    marginLeft: "1rem",
                  }}
                >
                  <IonLabel>
                    {mapLabel.nickname ? mapLabel.nickname : mapLabel.username}
                  </IonLabel>
                </IonChip>
              </div>
            )}
          </div>

          <div
            className="d-flex"
            style={{
              backgroundColor: userRole === "guest" ? "rgba(0,0,0,0.1)" : "",
              borderRadius: "1rem",
              marginTop: "3px",
              alignItems: "center",
              width: "100%",
              justifyContent: "center",
            }}
          >
            {/* like and dislike */}
            <IonButton
              className={
                like_members.includes(user_id)
                  ? "inLikeOrDisLikeList"
                  : "defaultLikeList"
              }
              style={{
                backgroundColor: userRole === "guest" ? "rgba(0,0,0,0)" : "",
              }}
              color="white"
              onTouchEnd={() => likeOrDislikeMapLabel("like")}
            >
              <FontAwesomeIcon
                icon={faCircleCheck}
                color="yellowgreen"
                size="2x"
              ></FontAwesomeIcon>
              <div style={{ color: "#717E60" }}>
                &nbsp;&nbsp;&nbsp;
                {like_members?.length ? like_members.length : 0}
              </div>
            </IonButton>
            <IonButton
              className={
                dislike_members.includes(user_id)
                  ? "inLikeOrDisLikeList"
                  : "defaultLikeList"
              }
              style={{
                backgroundColor: userRole === "guest" ? "rgba(0,0,0,0)" : "",
              }}
              color="white"
              onTouchEnd={() => likeOrDislikeMapLabel("dislike")}
            >
              <FontAwesomeIcon
                icon={faCircleXmark}
                color="red"
                size="2x"
              ></FontAwesomeIcon>
              <div style={{ color: "#717E60" }}>
                &nbsp;&nbsp;&nbsp;
                {dislike_members?.length ? dislike_members.length : 0}
              </div>
            </IonButton>
          </div>
        </Popup>
      </Marker>
      <IonToast
        isOpen={openToast}
        duration={2000}
        onDidDismiss={() => setOpenToast(false)}
        message="Like or dislike 功能要註冊左先有呀。"
      ></IonToast>
      <IonModal isOpen={openModal} className="modal-class">
        <div className="modal-image ion-padding">
          <img
            src={env.AWS_S3_UPLOADS_ORIGIN + mapLabel.image_filename}
            alt="preview"
          />
          <CrossCancelButton
            onTouchEnd={() => setOpenModal(false)}
            color="light"
          />
        </div>
      </IonModal>
    </>
  );
};
