import  { useEffect } from "react";
import { useMap } from "react-leaflet";
import {  GeoSearchControl,OpenStreetMapProvider } from "leaflet-geosearch";
import { defaultIcon } from "../../icons/leaflet-icons";


export function GeoSearch() {
    const map = useMap();
    //@ts-ignore
    useEffect(() => {
      const provider = new OpenStreetMapProvider();
    // @ts-ignore
      const searchControl = new GeoSearchControl({
        className:"geo-search-bar",
        notFoundMessage: '不好意思，暫時沒有對於地址數據。',
        provider,
        marker: {icon: defaultIcon},
        style:'bar',
        animateZoom: true,
        updateMap: true,
      });
  
      map.addControl(searchControl);
  
      return () => map.removeControl(searchControl);
    }, [map]);
  
    return null;
  }


