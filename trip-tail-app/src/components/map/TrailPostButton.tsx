import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLocationDot,  } from "@fortawesome/free-solid-svg-icons";
import React, { useState } from "react";
import styles from "../../pages/MapTab.module.css";
import { CreatePostPopup } from "./CreatePostPopup";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/state";
import { TempTrailPost } from "../../redux/map/state";
import { setTempTrailPostAction } from "../../redux/map/action";
import { TrailPostLabel } from "./TrailPostLabel";

export const TrailPostButton = (props: { hidden?: boolean }) => {
  const [openCreatePostPopup, setOpenCreatePostPopup] = useState(false);
  const dispatch = useDispatch();
  let currentTrailPosts = useSelector(
    (state: RootState) => state.map.tempTrailPost
  );

  const createPost = (trailPost: TempTrailPost) => {
    currentTrailPosts.push(trailPost);
    dispatch(setTempTrailPostAction(currentTrailPosts));
  };



  return (
    <>
      <button
        hidden={props.hidden}
        className={styles.allToCenter}
        style={trailPostButtonStyles}
        onTouchEnd={() => setOpenCreatePostPopup(true)}
      >
        <FontAwesomeIcon icon={faLocationDot} size="3x" color={"#e8be31"} />
      </button>
      <CreatePostPopup
        open={openCreatePostPopup}
        onClose={() => setOpenCreatePostPopup(false)}
        onDecided={(trailPost) => createPost(trailPost)}
      />
      {currentTrailPosts.map((trailPost, idx) => {
        return (
            <TrailPostLabel key={idx} trailPost={trailPost}/>
        );
      })}
    </>
  );
};

const trailPostButtonStyles: React.CSSProperties = {
  position: "fixed",
  bottom: "14.5%",
  right: "5%",
  width: "3rem",
  height: "3rem",
  backgroundColor: "rgba(255,255,255,0.9)",
  boxShadow: "3px 3px 20px 0.1px rgba(0,0,0,0.5)",
  borderRadius: "1rem",
  zIndex: 999,
};

