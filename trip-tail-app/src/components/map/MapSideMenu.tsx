import {
  faMapLocation,
  faMountain,
  faUser,
  faUsers,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonMenu,
  IonToggle,
  IonToolbar,
} from "@ionic/react";
import { useRole } from "../../hooks/useRole";
import useLocalStorageState from "use-local-storage-state";
export const MapSideMenu = () => {
  let role = useRole();

  const [mapSetting, setMapSetting] = useLocalStorageState("mapSetting", {
    ssr: true,
    defaultValue: {
      personalLabel: true,
      publicLabel: true,
      mountainLabel: true,
    },
  });

  return (
    <IonMenu
      swipeGesture={false}
      side="start"
      menuId="map-menu"
      contentId="map"
      className="my-custom-menu"
    >
      <IonHeader>
        <IonToolbar color="primary">
          <div style={{ width: "50vw", textAlign: "center" }}>地圖設定</div>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          <IonItem>
            <IonLabel style={{ textAlign: "center", width: "100%" }}>
              標籤
            </IonLabel>
          </IonItem>
          <IonItem>
            <IonLabel>
              <FontAwesomeIcon
                icon={faUser}
                size="2x"
                color={"#94D1AF"}
                style={{ marginLeft: "0.4rem", marginRight: "0.9rem" }}
              />
              <FontAwesomeIcon
                icon={faMapLocation}
                size="2x"
                color={"#628A66"}
              />
            </IonLabel>
            <IonToggle
             style={{marginRight:'1rem'}}
              value="personal label"
              disabled={role === "user" ? false : true}
              checked={mapSetting.personalLabel}
              onIonChange={(e) =>
                setMapSetting({
                  ...mapSetting,
                  personalLabel: e.detail.checked,
                })
              }
            />
          </IonItem>

          <IonItem>
            <IonLabel>
              <FontAwesomeIcon
                icon={faUsers}
                size="2x"
                color={"#94D1AF"}
                style={{ marginRight: "0.5rem" }}
              />
              <FontAwesomeIcon
                icon={faMapLocation}
                size="2x"
                color={"#628A66"}
              />
            </IonLabel>
            <IonToggle
              style={{marginRight:'1rem'}}
              value="public label"
              checked={mapSetting.publicLabel}
              onIonChange={(e) =>
                setMapSetting({ ...mapSetting, publicLabel: e.detail.checked })
              }
            />
          </IonItem>
          <IonItem>
            <IonLabel
              className="w-100"
              style={{ display: "flex", justifyContent: "center" }}
            >
              <div style={labelIconContainerStyles}>
                <FontAwesomeIcon
                  className="custom-material-icons"
                  icon={faMountain}
                  color={"#628A66"}
                />
              </div>
            </IonLabel>
            <IonToggle
             style={{marginRight:'1rem'}}
              value="public label"
              checked={mapSetting.mountainLabel}
              onIonChange={(e) =>
                setMapSetting({
                  ...mapSetting,
                  mountainLabel: e.detail.checked,
                })
              }
            />
          </IonItem>
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

const labelIconContainerStyles = {
  height: "2.5rem",
  width: "2.5rem",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: "100%",
  border: "5px solid #628A66",
};
