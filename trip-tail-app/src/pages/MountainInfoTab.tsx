import {
  IonContent,
  IonHeader,
  IonList,
  IonPage,
  IonSearchbar,
  IonToolbar,
} from "@ionic/react";
import { useSelector } from "react-redux";
import { LoadingDots } from "../components/LoadingDots";
import DistrictCard from "../components/mountainInfo/DistrictCard";
import { MountainInfoProvider } from "../redux/mountainInfo/provider";
import { RootState } from "../redux/state";
import React, { useState } from "react";
import NT from "../icons/new-territories.jpg";
import HKIsland from "../icons/hong-kong-island16-9.jpg";
import KL from "../icons/kowloon.jpg";
import Lantau from "../icons/lantau-island16-9.jpg";
import "./MountainInfoTab.css";

export const MountainInfoTab: React.FC = () => {


  const districtDistinct = [
    { id: 1, name: "新界", src: NT },
    { id: 2, name: "九龍", src: KL },
    { id: 3, name: "香港島", src: HKIsland },
    { id: 4, name: "離島", src: Lantau },
]

  const mountainInfoState = useSelector(
    (state: RootState) => state.mountainInfo
  );
  const mountainInfoStatus = mountainInfoState.fetch.mountainInfoStatus.type;
  const mountainList = mountainInfoState.mountainInfos;


  // Sort district
  mountainList.sort((a, b) => {
    if (a.district_id > b.district_id) {
      return 1;
    } else if (a.district_id < b.district_id) {
      return -1;
    } else {
      return 0;
    }
  });

  //  filter from district ID
  function trailDistrict(trailInfo: number) {
    return mountainList.filter((trail) => {
      return trail.district_id === trailInfo;
    });
  }

  //  SearchBar
  const SearchBar: React.FC = () => {
    const [searchText, setSearchText] = useState("");

    // const trails = Array.from(document.querySelector(".trail-btn")?.children);

    return (
      <IonToolbar>
        <IonSearchbar
          value={searchText}
          onIonChange={(e) => setSearchText(e.detail.value!)}
          debounce={1000}
          animated
          className="search-bar ion-margin-top"
        ></IonSearchbar>
      </IonToolbar>
    );
  };

  return (
    <IonPage>
      <MountainInfoProvider />
      <IonHeader>
        <SearchBar />
      </IonHeader>
      <IonContent fullscreen>
        {mountainInfoStatus === "idle" || mountainInfoStatus === "fail" ? (
          <LoadingDots isOpen={true} size={10} />
        ) : (
          <>
            <IonList>
              {districtDistinct.map((districtInfo,index) => (
                <DistrictCard
                key={index}
                  districtID={districtInfo.id}
                  districtName={districtInfo.name}
                  districtImg={districtInfo.src}
                  trailDistrict={trailDistrict}
                />
              ))}
            </IonList>
          </>
        )}
      </IonContent>
    </IonPage>
  );
};

export default MountainInfoTab;
