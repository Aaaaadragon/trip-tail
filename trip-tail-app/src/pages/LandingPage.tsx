import { faCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IonContent, IonPage, useIonViewWillEnter } from "@ionic/react";
import { useEffect, useState } from "react";
import { MapLabelProvider } from "../redux/map/provider";
import { MountainInfoProvider } from "../redux/mountainInfo/provider";
import {
  ProfileMapLabelsProvider,
  TrailRecordProvider,
  UserProfileProvider,
} from "../redux/profile/provider";
import mainLogo from "../icons/trip_tail_logo.png";
import "./LandingPage.css";
import { useDispatch } from "react-redux";
import { push } from "connected-react-router";
import { routes } from "../routes";

export const LandingPage = () => {
  useIonViewWillEnter(() => {
    const tabBar = document.getElementById("app-tab-bar");
    if (tabBar !== null) {
      tabBar.style.display = "none";
    }
  });

  const dispatch = useDispatch()
  const [dotState, setDotState] = useState(1);
  const [passedTime, setPassedTime] = useState(0)
  const [loaded,setLoaded] = useState(false)

  const dotStateColor = (index: number) => {
    if (index <= dotState) {
      return "#709E84";
    }
    return "grey";
  };


  useEffect(()=>{
      if(passedTime<5000){
          let timer = setInterval(()=>{
             if(dotState>=5){
                 setDotState(1)
             }else{
                 setDotState(dotState+1)
             }
             setPassedTime(passedTime+500)
          },500) 
          return (()=>{
            clearInterval(timer)
        })
      }

     if(passedTime >= 5000){
         dispatch(push(routes.tab.main))
         setLoaded(true)
     }

    
  },[dotState,passedTime,dispatch])

  

  return  loaded?null:
(<IonPage>
      <IonContent fullscreen>
        <div className="landing-content-container all-to-center">
          <img
            className="landing-main-logo"
            src={mainLogo}
            alt="trip tail logo"
          />
          <div className="landing-dot-container">
            {[1, 2, 3, 4, 5].map((idx) => (
              <FontAwesomeIcon
                key={idx}
                className="landing-dot"
                icon={faCircle}
                color={dotStateColor(idx)}
                size="2x"
              />
            ))}
          </div>
        </div>
        <ProfileMapLabelsProvider  />
        <UserProfileProvider  />
        <TrailRecordProvider/>
        <MountainInfoProvider />
        <MapLabelProvider  />
      </IonContent>
    </IonPage>)
  
    
  
};
