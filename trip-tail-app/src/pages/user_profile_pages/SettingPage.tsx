import {
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonItem,
  IonList,
  IonPage,
  IonRow,
  IonToolbar,
  useIonViewWillEnter,
  useIonViewWillLeave,
} from "@ionic/react";
import React from "react";
import { useDispatch } from "react-redux";
import { logoutThunk } from "../../redux/auth/thunk";
import "./SettingPage.css";
import { push } from "connected-react-router";
import { routes } from "../../routes";
import { GoBackButton } from "../../components/GoBackButton";

export const SettingPage: React.FC = () => {
  useIonViewWillEnter(() => {
    const tabBar = document.getElementById("app-tab-bar");
    if (tabBar !== null) {
      tabBar.style.display = "none";
    }
  });

  useIonViewWillLeave(() => {
    console.log("leave");
  });

  const dispatch = useDispatch();

  const logout = () => {
    dispatch(push(routes.tab.main));
    dispatch(logoutThunk());
  };

  return (
    <IonPage className="setting-page">
      <IonHeader>
      <IonToolbar>
            <IonGrid>
              <IonRow>
                <IonCol size="2">
                  <GoBackButton routerLink={routes.tab.userProfile} />
                </IonCol>
                <IonCol
                  size="7"
                  className="d-flex justify-content-center "
                  style={{ marginLeft: "1.4rem", marginTop: "0.3rem" }}
                >
                  設定
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          <IonItem onClick={logout}>登出</IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};
