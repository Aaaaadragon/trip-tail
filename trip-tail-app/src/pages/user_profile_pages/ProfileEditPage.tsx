import {
  IonButton,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonInput,
  IonLabel,
  IonPage,
  IonRow,
  IonToolbar,
  useIonViewWillEnter,
} from "@ionic/react";
import imageCompression from "browser-image-compression";
import { push } from "connected-react-router";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { GoBackButton } from "../../components/GoBackButton";
import { UserIcon } from "../../components/UserIcon";
import { useProfile } from "../../hooks/useProfile";
import { updateUserProfileThunk } from "../../redux/profile/thunk";
import { routes } from "../../routes";

const imageCompressOptions = {
  maxSizeMB: 0.2,
};

type ProfileFormState = {
  nickname?: string;
  icon_photo: any;
  gender?: string;
  district_id?: number;
  hike_frequency?: number;
  is_private?: boolean;
};

export const ProfileEditPage = () => {
  useIonViewWillEnter(() => {
    const tabBar = document.getElementById("app-tab-bar");
    if (tabBar !== null) {
      tabBar.style.display = "none";
    }
  });


  const profileState = useProfile();

  const defaultFormState: ProfileFormState = {
    nickname: profileState.nickname,
    icon_photo: profileState.icon_photo ? profileState.icon_photo : "",
    gender: profileState.gender,
    district_id: profileState.district_id,
    hike_frequency: profileState.hike_frequency,
    is_private: profileState.is_private,
  };

  const dispatch = useDispatch();
  const [openReminder, setOpenReminDer] = useState(false);
  const [formState, setFormState] = useState(defaultFormState);
  const [previewImage, setPreviewImage] = useState("");
  const [remindTextClassName, setRemindTextClassName] =
    useState("animate__animated ");

  const onFileChange = async (fileChangesEvent: any) => {
    let photoFile = fileChangesEvent.target.files[0];
    try {
      const compressedPhotoFile: Blob = await imageCompression(
        photoFile,
        imageCompressOptions
      );
      setFormState({ ...formState, icon_photo: compressedPhotoFile });
      let objURL = URL.createObjectURL(compressedPhotoFile);
      setPreviewImage(objURL);
    } catch (error) {
      console.log("Image compression failed, ERROR:", error);
    }
  };

  const submitForm = (e: React.TouchEvent) => {
    e.preventDefault();
    if (formState.nickname && formState.nickname?.length > 10) {
      triggerReminder();
    }
    let profileFormData = new FormData();
    for (let [key, value] of Object.entries(formState)) {
      profileFormData.append(key, value);
    }

 
    dispatch(updateUserProfileThunk(profileFormData));
    dispatch(push(routes.tab.userProfile));
  };

  const triggerReminder = () => {
    setOpenReminDer(true);
    setRemindTextClassName(
      (remindTextClassName) => (remindTextClassName += "animate__headShake")
    );

    setTimeout(() => {
      setRemindTextClassName("animate__animated ");
      setOpenReminDer(false);
    }, 2000);
    return;
  };

  return (
    <>
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonGrid>
              <IonRow>
                <IonCol size="2">
                  <GoBackButton routerLink={routes.tab.userProfile} />
                </IonCol>
                <IonCol
                  size="7"
                  className="d-flex justify-content-center "
                  style={{
                    marginLeft: "1.4rem",
                    marginTop: "1.5rem",
                    color: "white",
                  }}
                >
                  個人資料
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
          <div
            className="w-100"
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
              height: "75vh",
            }}
          >
            <div className="d-flex justify-content-center align-items-center mt-3">
              <UserIcon
                iconSize="5rem"
                photo={
                  previewImage
                    ? previewImage
                    : formState.icon_photo
                    ? formState.icon_photo
                    : null
                }
              ></UserIcon>
            </div>
            <IonButton>
              <label htmlFor="files">上傳相片</label>
            </IonButton>
            <input
              id="files"
              style={{ visibility: "hidden", width: "0" }}
              type="file"
              onChange={(e) => onFileChange(e)}
              accept="image/*"
            ></input>

            <IonGrid>
              <IonRow>
                <IonCol
                  size="7"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <IonLabel
                    style={{
                      height: "3rem",
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    暱稱：
                  </IonLabel>
                  <IonLabel style={{ marginTop: "20px" }}>性別：</IonLabel>
                  <IonLabel style={{ marginTop: "20px" }}>
                    經常遠足地區：
                  </IonLabel>
                  <IonLabel style={{ marginTop: "20px" }}>
                    偏好路徑難度：
                  </IonLabel>
                </IonCol>

                <IonCol
                  size="5"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <IonInput
                    placeholder="暱稱/網名"
                    style={{
                      margin: "2px",
                      height: "3rem",
                      width: "30vw",
                      border: "1px solid grey",
                      borderRadius: "0.5rem",
                      textAlign: "center",
                    }}
                    type="text"
                    value={formState.nickname}
                    onIonChange={(e) => {
                      setFormState({
                        ...formState,
                        nickname: e.detail.value || "",
                      });
                    }}
                  ></IonInput>
                  <div style={{ marginTop: "20px" }}>
                    <select
                      onChange={(e) => {
                        setFormState({ ...formState, gender: e.target.value });
                      }}
                      defaultValue={formState.gender ? formState.gender : 0}
                      style={{
                        width: "5rem",
                        textAlign: "center",
                        border: "0",
                      }}
                    >
                      <option value="default">未填寫</option>
                      <option value="male">男性</option>
                      <option value="female">女性</option>
                    </select>
                  </div>
                  <div style={{ marginTop: "20px" }}>
                    <select
                      onChange={(e) => {
                        setFormState({
                          ...formState,
                          district_id: +e.target.value,
                        });
                      }}
                      defaultValue={
                        formState.district_id ? formState.district_id : 0
                      }
                      style={{
                        width: "5rem",
                        textAlign: "center",
                        border: "0",
                      }}
                    >
                      <option value={0}>未填寫</option>
                      <option value={1}>新界</option>
                      <option value={2}>九龍</option>
                      <option value={3}>香港島</option>
                      <option value={4}>離島</option>
                    </select>
                  </div>
                  <div style={{ marginTop: "20px" }}>
                    <select
                      onChange={(e) => {
                        setFormState({
                          ...formState,
                          hike_frequency: +e.target.value,
                        });
                      }}
                      defaultValue={
                        formState.hike_frequency ? formState.hike_frequency : 0
                      }
                      style={{
                        width: "7rem",
                        textAlign: "center",
                        border: "0",
                      }}
                    >
                      <option value={0}>未填寫</option>
                      <option value={2}>低</option>
                      <option value={4}>中</option>
                      <option value={5}>高</option>
                    </select>
                  </div>
                </IonCol>
              </IonRow>
            </IonGrid>
            <div style={{ height: "2rem" }} className={remindTextClassName}>
              <IonLabel style={{ color: "red" }} hidden={!openReminder}>
                暱稱不能多餘十個字
              </IonLabel>
            </div>
            <IonLabel style={{ color: "grey", marginBottom: "1rem" }}>
              部分資料將用於演算主頁的推薦山徑
            </IonLabel>
            <IonButton onTouchEnd={(e) => submitForm(e)}>更改資料</IonButton>
          </div>
        </IonContent>
      </IonPage>
    </>
  );
};
