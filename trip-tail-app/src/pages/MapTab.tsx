import {
  IonContent,
  IonLoading,
  IonPage,
  IonToast,
  useIonViewDidEnter,
} from "@ionic/react";
import { LatLngExpression } from "leaflet";
import React, { useEffect, useState } from "react";
import {
  Circle,
  MapContainer,
  Marker,
  Polyline,
  Popup,
  TileLayer,
} from "react-leaflet";
import { Geolocation, Geoposition } from "@ionic-native/geolocation";
// import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationEvents, BackgroundGeolocationResponse } from '@awesome-cordova-plugins/background-geolocation'

import "./MapTab.css";

import { MapButton } from "../components/map/MapButton";
import { useDispatch, useSelector } from "react-redux";
import { myPositionIcon } from "../icons/leaflet-icons";
import { setGetMapLabelsStatusAction, setPositionAction } from "../redux/map/action";
import { TracingMenuBar } from "../components/map/TracingMenuBar";
import { RootState } from "../redux/state";
import { usePosition } from "../hooks/usePosition";
import { useRecordingData } from "../hooks/useRecordingData";
import { CreateMapLabelPopup } from "../components/map/CreateMapLabelPopup";
import { MapLabelProvider } from "../redux/map/provider";
import { useMapLabels } from "../hooks/useMapLabels";
import { MapLabels } from "../components/map/MapLabels";
import { GeoSearch } from "../components/map/GeoSearch";
import { useMountainInfo } from "../hooks/useMounainInfo";
import { MountainLabel } from "../components/map/MountainLabel";
import { useRole } from "../hooks/useRole";
import { SearchButton } from "../components/map/SearchButton";
import { MapSideMenu } from "../components/map/MapSideMenu";
import { MapMenuButton } from "../components/map/MapMenuButton";
import { useAuthUser } from "../hooks/useAuthUser";
import SweetAlert from "react-bootstrap-sweetalert";
import { RefreshMapLabelsButton } from "../components/map/RefreshMapLabelsButton";
import { informationCircleOutline } from "ionicons/icons";
// import { BackgroundMode} from '@awesome-cordova-plugins/background-mode'

type LocationError = {
  showError: boolean;
  message?: string;
};


const MapTab: React.FC = () => {
  useIonViewDidEnter(() => {
    window.dispatchEvent(new Event("resize"));
  });
  // BackgroundMode.enable()
  const dispatch = useDispatch();
  const position = usePosition();
  const latestMapLabels = useMapLabels();

  const [currentLocation, setCurrentLocation] = useState<Geoposition>();
  const [error, setError] = useState<LocationError>({ showError: false });
  const [showErrorCount, setShowErrorCount] = useState(0);
  const [GPSData, setGPSData] = useState<LatLngExpression[]>([]);
  const loading = useSelector((state: RootState) => state.map.isLoading);
  const [isCreateLabelPopOpen, setIsCreateLabelPopOpen] = useState(false);
  const [mapLabels, setMapLabels] = useState(latestMapLabels);
  const [remindMessage, setRemindMessage] = useState<string>("");
  const [triggerSweetAlert, setTriggerSweetAlert] = useState(false)
  let thisUserId = useAuthUser()?.payload.id;
  if (!thisUserId) thisUserId = 0;
  let recordingData = useRecordingData();
  let mapSetting: any = localStorage.getItem("mapSetting");
  if (!mapSetting) {
    mapSetting = {
      personalLabel: true,
      publicLabel: true,
      mountainLabel: true,
    };
  } else {
    mapSetting = JSON.parse(mapSetting);
  }
  const mountainInfos = useMountainInfo().mountainInfos;
  const role = useRole();
  const markerPosition: LatLngExpression = currentLocation
    ? toLatLng(currentLocation)
    : position;

  useEffect(() => {
    const timer = setInterval(async () => {
      // console.log("getting gps data");
      try {


        await Geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(
          (position) => {
            setCurrentLocation(position);
            dispatch(setPositionAction(toLatLng(position)));
          }
        );
      } catch (e: any) {
        if (showErrorCount < 1) {
          const message =
            e.message.length > 0 ? e.message : "Cannot get user location";
          setError({ showError: true, message });
          dispatch(setPositionAction([22.3, 114.18]));
        }
      }
    }, 3000);

    return () => {
      clearInterval(timer);
    };
  }, [dispatch, showErrorCount]);

  useEffect(() => {
    if (recordingData.trail_name !== "") {
      const polyLineTimer = setInterval(() => {
        // console.log("update poly line");
        if(recordingData.GPSData&&recordingData.GPSData?.length>2){
          setGPSData(recordingData.GPSData!);
        }
      }, 1000);
      return () => {
        clearInterval(polyLineTimer);
      };
    }
  }, [
    dispatch,
    position,
    GPSData,
    recordingData.GPSData,
    recordingData.trail_name,
  ]);

  useEffect(() => {
    setMapLabels(latestMapLabels);

  }, [dispatch, latestMapLabels]);

  return loading ? (
    <IonLoading isOpen={loading} message={"正在獲取你的位置"} />
  ) : (
    <>
      <MapSideMenu />
      <IonPage id="map" >
        <IonContent>
          <MapContainer
            id="main-map"
            center={markerPosition}
            zoom={18}
            tap={false}
          >
            <MapLabelProvider>
              {mapLabels.map((mapLabel) => {
                if (
                  !mapSetting.personalLabel 
                ) {
                  if(mapLabel.user_id === thisUserId) return null;
                  
                }
                if (
                  !mapSetting.publicLabel &&
                  thisUserId !== mapLabel.user_id
                ) {
                  return null;
                }
                return <MapLabels key={mapLabel.id} mapLabel={mapLabel} />;
              })}

              {mountainInfos.map((mountainInfo) => {
                if (!mapSetting.mountainLabel) {
                  return null;
                }

                return (
                  <MountainLabel
                    key={mountainInfo.id}
                    mountainInfo={mountainInfo}
                  />
                );
              })}
            </MapLabelProvider>

            <MapButton purpose="SetPositionView" />
            <MapButton
              purpose="CreateMapLabel"
              color="#628A66"
              onTouchEnd={() => {
                if (role === "guest") {
                  setRemindMessage("登入後才可使用地圖標籤功能。");
                  return;
                }
                // console.log("create map label");
                setIsCreateLabelPopOpen(true);
              }}
            />
            <CreateMapLabelPopup
              open={isCreateLabelPopOpen}
              onClose={() => {
                setIsCreateLabelPopOpen(false);
              }}
              onDecided={()=>setTriggerSweetAlert(true)}
            />
            <SearchButton />
            <RefreshMapLabelsButton />
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <GeoSearch />
            <div>
              <Circle center={markerPosition} radius={20} />
              <Marker position={markerPosition} icon={myPositionIcon}>
                <Popup>呢到係你個位置🇭🇰</Popup>
              </Marker>

              <TrailPath GPSData={GPSData} />
              <IonToast
                isOpen={error.showError}
                message={error.message}
                icon={informationCircleOutline}
                onDidDismiss={() => {
                  setShowErrorCount(showErrorCount + 1);
                  setError({ showError: false, message: undefined });
                }}
                duration={3000}
              />
              <IonToast
                isOpen={remindMessage ? true : false}
                message={remindMessage}
                duration={2000}
                onDidDismiss={() => setRemindMessage("")}
              />
            </div>
            <TracingMenuBar
              onStop={() =>
                setTimeout(() => {
                  setGPSData([]);
                }, 1100)
              }
            />
          </MapContainer>
          <MapMenuButton />
        </IonContent>
      </IonPage>
      <SweetAlert
    openAnim={triggerSweetAlert}
    hideOverlay={true}
    show={triggerSweetAlert}
    success
    title="已成功在地圖上標籤"
    onConfirm={()=>setTriggerSweetAlert(false)}
>
</SweetAlert>
    </>
  );
};

export default MapTab;

function toLatLng(location: Geoposition): LatLngExpression {
  let latLng: LatLngExpression = [
    location.coords.latitude,
    location.coords.longitude,
  ];
  return latLng;
}

export const TrailPath = (props: { GPSData: LatLngExpression[] }) => {
  return (
    <Polyline pathOptions={{ color: "blue" }} positions={[...props.GPSData]} />
  );
};
