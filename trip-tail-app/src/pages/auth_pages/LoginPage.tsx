import {
  IonCol,
  IonContent,
  IonGrid,
  IonLabel,
  IonPage,
  IonRow,
  IonSegment,
  IonSegmentButton,
  IonToolbar,
} from "@ionic/react";

import { Link } from "react-router-dom";
import { routes } from "../../routes";
import mainIcon from "../../icons/trip_tail_logo.png";
import "./LoginPage.css";
import { useState } from "react";
import { LoginWithPasswordBoard } from "../../components/auth/LoginWithPasswordBoard";
import { LoginWithEmailCodeBoard } from "../../components/auth/LoginWithEmailCodeBoard";

export function LoginPage() {
  const [segmentSelected, setSegmentSelected] = useState("username");

  return (
    <>
      <IonPage>
        <IonToolbar>
          <IonGrid>
            <IonRow>
              <IonCol
                size="12"
                className="d-flex justify-content-center"
                style={{ marginTop: "1.5rem", color: "white" }}
              >
                登入頁面
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonToolbar>
        <IonContent className="ion-padding">
          <div className="login-page-container">
            <div className="w-100 text-center">
              <img className="trip-tail-icon" src={mainIcon} alt="trip tail" />
            </div>
           
            <div className="login-segment-container">
            {segmentSelected === "username" ? (
              <LoginWithPasswordBoard />
            ) : (
              <LoginWithEmailCodeBoard />
            )}

          

            <div className="ion-text-center mt-4">
              <IonLabel>
                未有帳號？<Link to={routes.authPages.register}>註冊</Link>
              </IonLabel>
            </div>
            <div className="segment-selector">
            <IonSegment
              defaultValue={'username'}
              value={segmentSelected}
              onIonChange={(e) => setSegmentSelected(e.detail.value!)}
              style={{ height: "7vh" }}
            >
              <IonSegmentButton value="username">
                <IonLabel>一般登入</IonLabel>
              </IonSegmentButton>
              <IonSegmentButton value="email">
                <IonLabel>電郵登入</IonLabel>
              </IonSegmentButton>
            </IonSegment>
            </div>
            </div>
          </div>
        </IonContent>
      </IonPage>
    </>
  );
}
