import {
  IonContent,
  IonCol,
  IonGrid,
  IonLabel,
  IonPage,
  IonRow,
  IonSegment,
  IonSegmentButton,
  IonToolbar,
} from "@ionic/react";
import { useState } from "react";
import { SignUpWithEmailBoard } from "../../components/auth/SignUpWithEmailBoard";
import { SignUpWithPasswordBoard } from "../../components/auth/SignUpWithPasswordBoard";

import "./RegisterPage.css";

export const RegisterPage: React.FC = () => {
  const [segmentSelected, setSegmentSelected] = useState("username");

  return (
    <IonPage>
      <IonToolbar>
        <IonGrid>
          <IonRow>
            <IonCol
              size="12"
              className="d-flex justify-content-center"
              style={{ marginTop: "1.5rem", color: "white" }}
            >
              註冊頁面
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonToolbar>
      <IonContent className="ion-padding">
        <IonSegment
          defaultValue={"username"}
          value={segmentSelected}
          onIonChange={(e) => setSegmentSelected(e.detail.value!)}
          style={{ height: "7vh" }}
        >
          <IonSegmentButton value="username">
            <IonLabel>一般註冊</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="email">
            <IonLabel>電郵註冊</IonLabel>
          </IonSegmentButton>
        </IonSegment>
        <div className="login-page-container">
          {segmentSelected === "username" ? (
            <SignUpWithPasswordBoard />
          ) : (
            <SignUpWithEmailBoard />
          )}
        </div>
      </IonContent>
    </IonPage>
  );
};

//   export default RegisterPage
