import {
  IonPage,
  IonContent,
  IonHeader,
  IonLabel,
  IonChip,
  IonIcon,
} from "@ionic/react";
import "./MainTab.css";
import { WeatherCard } from "../components/home/WeatherCard";
import { UserProfileProvider } from "../redux/profile/provider";
import { TrailImageSlider } from "../components/home/TrailImageSlider";
import { MountainInfoProvider } from "../redux/mountainInfo/provider";
import { imagesSharp } from "ionicons/icons";
import { TopRankBoard } from "../components/home/TopRankBoard";
import './MainTab.css'


const MainTab: React.FC = () => {
  const tabBar = document.getElementById("app-tab-bar");
  if (tabBar !== null) {
    tabBar.style.display = "flex";
  }


  return (
    <IonPage >
      <IonHeader style={{height:'6vh'}}>
      </IonHeader>
      <IonContent >
        <div style={IonInnerContentStyle}>
          <WeatherCard />
          <div className="w-100" style={{marginLeft:'10vw'}}>
          <IonChip style={{background:"#94D1AF",color:'#175733'}}>
          <IonIcon icon={imagesSharp} color="primary" />
          <IonLabel >推薦山路</IonLabel>
        </IonChip>
          </div>
         
          <div style={sliderContainerStyle}>
            <TrailImageSlider />
          </div>
          <div className="w-100" style={{marginLeft:'10vw'}}>
          <IonChip style={{background:"#94D1AF",color:'#175733'}}>
          <IonIcon icon={imagesSharp} color="primary" />
          <IonLabel >熱門統計</IonLabel>
        </IonChip>
          </div>
         
        <div className="top-rank-container" style={topRankContainerStyle}>
          <TopRankBoard></TopRankBoard>
        </div>
        </div>
      </IonContent>
      <MountainInfoProvider />
    </IonPage>
  );
};

export default MainTab;

const sliderContainerStyle: React.CSSProperties = {
  width: "90vw",
  height:'auto',
  backgroundColor:'#94D1AF',
  padding:'5vw',
  borderRadius:"1rem",
  background: 'linear-gradient(175deg, rgba(113,126,96,1) 0%, rgba(148,209,175,1) 100%, rgba(23,87,51,1) 100%)',
  boxShadow:' 0px 10px 15px 4px rgba(179,179,179,0.68)',
  marginBottom:"5vh",
};

const IonInnerContentStyle: React.CSSProperties = {
  display: "flex",
  justifyContent: "center",
  flexDirection: "column",
  alignItems: "center",
};

const topRankContainerStyle:React.CSSProperties = {
  width:"90vw",
  height:"40vh",
  backgroundColor:'#94D1AF',
  padding:'5vw',
  borderRadius:"1rem",
  background:'linear-gradient(175deg, rgba(113,126,96,1) 0%, rgba(148,209,175,1) 100%, rgba(23,87,51,1) 100%)',
  boxShadow:' 0px 10px 15px 4px rgba(179,179,179,0.68)',
  marginBottom:'5vh',
  overflow:'scroll',
  
}

