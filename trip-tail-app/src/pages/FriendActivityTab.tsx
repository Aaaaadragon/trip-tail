import { IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonPage, IonRow, IonToolbar } from '@ionic/react';
import './FriendActivityTab.css';
import { businessSharp } from 'ionicons/icons';
const FriendActivityTab: React.FC = () => {

  

  return (<>

    <IonPage id='main'>
      <IonHeader>
        <IonToolbar>
            <IonGrid>
              <IonRow>
                <IonCol
                   size="12"
                   className="d-flex justify-content-center"
                   style={{ marginTop: "0.7rem",color:'white' }}
                >
                  山友動態
                </IonCol>
              </IonRow>
            </IonGrid>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className='ion-padding'>
        <div  style={{height:'100%',width:'100%',display:'flex',justifyContent:'center',alignItems:'center',flexDirection:'column'}}>

        <IonIcon icon={businessSharp} style={{fontSize:'15rem'}}  color="primary"/>
          
          <label style={{fontSize:'2rem'}}>發展中...</label>
        </div>
        
      </IonContent>
    </IonPage>
  </>
  );
};

export default FriendActivityTab;
