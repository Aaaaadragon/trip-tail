import {
  IonChip,
  IonContent,
  IonHeader,
  IonLabel,
  IonList,
  IonPage,
  IonSegment,
  IonSegmentButton,
  IonToolbar,
} from "@ionic/react";
import { useDispatch, useSelector } from "react-redux";
import "./UserProfileTab.scss";
import { RootState } from "../redux/state";
import React, { useEffect, useState } from "react";
import { UserIcon } from "../components/UserIcon";

import { TrailRecordCard } from "../components/profile/TrailRecordCard";
import { routes } from "../routes";
import { push } from "connected-react-router";
import { TrailLabelCard } from "../components/profile/TrailLabelCard";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsis } from "@fortawesome/free-solid-svg-icons";
import {
  ProfileMapLabelsProvider,
  TrailRecordProvider,
  UserProfileProvider,
} from "../redux/profile/provider";
import { LoadingDots } from "../components/LoadingDots";
import { useTrailRecords } from "../hooks/useTrailRecords";
import { MapLabel } from "../redux/map/state";
import { useProfileMapLabels } from "../hooks/useProfileMapLabels";
import { useProfile } from "../hooks/useProfile";
import { StatisticLabel } from "../components/profile/StatisticLabel";
import { setProfileMapLabelStatusAction, setTrailRecordStatusAction } from "../redux/profile/action";

type RecordSegment = "trail" | "label";


const UserProfileTab: React.FC = () => {
  const tabBar = document.getElementById("app-tab-bar");
  if (tabBar !== null) {
    tabBar.style.display = "flex";
  }



  
  const [isRecordSegmentLoading, setIsRecordSegmentLoading] = useState(true)
  const [isLabelSegmentLoading, setIsLabelSegmentLoading] = useState(true)
  const [selectedSegment, setSelectedSegment] =
    useState<RecordSegment>("trail");

    const {fetchRecordStatus,fetchLabelStatus} = useSelector((state: RootState) => {
      let fetchRecordStatus = state.profile.fetch.trailRecordStatus.type;
      let fetchLabelStatus = state.profile.fetch.mapLabelStatus.type;
      return {fetchRecordStatus,fetchLabelStatus}
    });
  const dispatch = useDispatch();


  let user = useSelector((state: RootState) => {
    return state.auth.user;
  });

  const allTrailPosts = useSelector((state:RootState)=>state.profile.trailRecordPosts)

  const trailRecords = useTrailRecords();

  const totalTimesOfTrail = trailRecords.length;
  let totalHourOfTrail: number = 0;
  let totalKilometerOfTrail: number = 0;
  trailRecords.map((record) => {
    totalHourOfTrail += record.duration;
    totalKilometerOfTrail += record.distance;
    return null;
  });

  const mapLabels: MapLabel[] = useProfileMapLabels();
  const profile = useProfile();


  const toSettingPage = (e: React.MouseEvent) => {
    e.preventDefault();
    dispatch(push(routes.userProfilePages.setting));
    // return <Redirect to={routes.userProfilePages.setting}/>
  };

  const toProfileEditPage = (e: React.TouchEvent) => {
    e.preventDefault();
    dispatch(push(routes.userProfilePages.profile));
  };

  useEffect(() => {

    
    if(fetchRecordStatus === "idle" ||fetchRecordStatus === "fail"){
      setIsRecordSegmentLoading(true)
    }else{
      setIsRecordSegmentLoading(false)
    }
    if(fetchLabelStatus === "idle" ||fetchLabelStatus === "fail"){
      setIsLabelSegmentLoading(true)
    }else{
      setIsLabelSegmentLoading(false)
    }

  }, [dispatch,fetchRecordStatus,fetchLabelStatus]);

  useEffect(() => {

    dispatch(setTrailRecordStatusAction({type:'idle'}))
    dispatch(setProfileMapLabelStatusAction({type:'idle'}))
  }, [dispatch]);

  return (
    <>
      <UserProfileProvider />
      <TrailRecordProvider />
      <ProfileMapLabelsProvider />

      <IonPage>
        

      <IonHeader style={{height:'7vh' ,backgroundColor:'white'}}>
      </IonHeader>
        <IonContent>
        <div className="profile-tab-fixed-head">
          <div className="profile-header">
            <div className="left-icon-container">
              <IonChip color="primary" style={{ fontSize: "1.2rem" }}>
                <IonLabel>
                  {profile.nickname ? profile.nickname : user?.payload.username}
                </IonLabel>
              </IonChip>

              <UserIcon
                photo={profile.icon_photo}
                iconSize={"5em"}
                style={{ boxShadow: "5px 5px 10px 4px lightgray" }}
                onTouchEnd={(e) => toProfileEditPage(e)}
              />
            </div>
            <div className="right-info-container">
              <div className="w-100 d-flex justify-content-end mt-2">
                {/* <div style={{marginLeft:'2em', marginTop:'5px'}}>行山紀錄</div>  */}
                <button
                  className="profile-setting-btn"
                  onClick={(e) => toSettingPage(e)}
                  style={{ backgroundColor: "#d6d6d6" }}
                >
                  <FontAwesomeIcon
                    icon={faEllipsis}
                    color="black"
                  ></FontAwesomeIcon>
                </button>
              </div>
              <div className="w-100 mt-1" />

              <StatisticLabel title="次數" data={totalTimesOfTrail} unit="次" />
              <StatisticLabel
                title="時長"
                data={(totalHourOfTrail / 1000 / (60 * 60)).toFixed(2)}
                unit="小時"
                color="secondary"
              />
              <StatisticLabel
                title="路程"
                data={totalKilometerOfTrail.toFixed(2)}
                unit="公里"
                color="secondary"
              />
            </div>
          </div>
          <IonToolbar style={{height:'8vh',display:'flex',justifyContent:'center',alignItems:'center',backgroundColor:'#628A66'}}>
            <IonSegment
              defaultValue={'trail'}
              className="w-100 profile-tab-segment"
              value={selectedSegment}
              onIonChange={(e) => setSelectedSegment(e.detail.value as any)}
            >
              <IonSegmentButton value="trail">
                <div>足跡</div>
              </IonSegmentButton>
              <IonSegmentButton value="label">
                <div>標籤</div>
              </IonSegmentButton>
            </IonSegment>
          </IonToolbar>
          </div>
          <IonList className="segment-tab-container" >
            {selectedSegment === "trail" ? (
              <>
                {isRecordSegmentLoading? (
                  <LoadingDots isOpen={true} size={5} />
                ) : (
                  <>
                    {trailRecords.length === 0 ? (
                      <IonLabel
                        style={{
                          height: "60vh",
                          display: "flex",
                          alignItems: "center",
                        }}
                        className="ion-text-center"
                      >
                        未有足跡
                        <br />
                        快去地圖頁面嘗試創建你的首個足跡👣吧！
                      </IonLabel>
                    ) : (
                      trailRecords.map((trailRecord,idx) => {

                        let trailPosts = allTrailPosts[trailRecord.id]

                        return (
                          <TrailRecordCard
                            key={idx}
                            id={trailRecord.id}
                            trailRecordData={{
                              title: trailRecord.trail_name,
                              duration: trailRecord.duration,
                              distance: trailRecord.distance,
                              date: trailRecord.created_at,
                              gps_record: trailRecord.gps_record,
                            }}
                            trailPosts={trailPosts}
                          />
                        );
                      })
                    )}
                  </>
                )}
              </>
            ) : (
              <>
                {isLabelSegmentLoading? (
                  <LoadingDots isOpen={true} size={5} />
                ) : (
                  <>
                    {mapLabels.length === 0 ? (
                      <IonLabel
                        style={{
                          height: "60vh",
                          display: "flex",
                          alignItems: "center",
                        }}
                        className="ion-text-center"
                      >
                        未有標籤
                        <br />
                        快去地圖頁面嘗試創建你的首個標籤🏷️吧！
                      </IonLabel>
                    ) : (
                      mapLabels.map((mapLabel,idx) => {
                        return (
                          <TrailLabelCard
                            key={idx}
                            mapLabel={mapLabel}
                          />
                        );
                      })
                    )}
                  </>
                )}
              </>
            )}
          </IonList>
        </IonContent>
      </IonPage>
    </>
  );
};

export default UserProfileTab;
