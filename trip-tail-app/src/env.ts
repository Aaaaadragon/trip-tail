
function ensure(key:string){
    let value = process.env[key]
    if(!value){
        throw new Error(`missing ${key} in environment variable`)
    }
    return value
}

export let env = {
    API_ORIGIN:ensure('REACT_APP_API_ORIGIN'),
    AWS_S3_UPLOADS_ORIGIN:ensure('REACT_APP_AWS_S3_UPLOADS_ORIGIN'),
}